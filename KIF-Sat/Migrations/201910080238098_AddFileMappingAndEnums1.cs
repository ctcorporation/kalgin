namespace KIF_Sat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFileMappingAndEnums1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FileDescription",
                c => new
                    {
                        PC_Id = c.Guid(nullable: false, identity: true),
                        PC_FileName = c.String(maxLength: 50),
                        PC_HasHeader = c.Boolean(nullable: false),
                        PC_Delimiter = c.Boolean(nullable: false),
                        PC_FieldCount = c.Int(nullable: false),
                        PC_Quotations = c.Boolean(nullable: false),
                        PC_HeaderStartFrom = c.Int(nullable: false),
                        PC_DataStartFrom = c.Int(nullable: false),
                        PC_FirstFieldName = c.String(),
                        PC_LastFieldName = c.String(),
                        PC_FileExtension = c.String(maxLength: 5),
                    })
                .PrimaryKey(t => t.PC_Id);
            
            CreateTable(
                "dbo.MAPPING_ENUM",
                c => new
                    {
                        M_Id = c.Guid(nullable: false, identity: true),
                        M_Description = c.String(maxLength: 100),
                        M_MapType = c.String(maxLength: 20),
                        M_Operation = c.String(maxLength: 30),
                        M_FieldType = c.String(maxLength: 3),
                        M_From = c.String(maxLength: 50),
                        M_To = c.String(maxLength: 50),
                        M_PC = c.Guid(),
                    })
                .PrimaryKey(t => t.M_Id)
                .ForeignKey("dbo.FileDescription", t => t.M_PC)
                .Index(t => t.M_PC);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MAPPING_ENUM", "M_PC", "dbo.FileDescription");
            DropIndex("dbo.MAPPING_ENUM", new[] { "M_PC" });
            DropTable("dbo.MAPPING_ENUM");
            DropTable("dbo.FileDescription");
        }
    }
}
