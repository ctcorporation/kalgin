﻿namespace KIF_Sat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateEF : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
                //"dbo.vw_CustomerProfile",
                //c => new
                //    {
                //        C_IS_ACTIVE = c.String(nullable: false, maxLength: 1),
                //        C_ON_HOLD = c.String(nullable: false, maxLength: 1),
                //        C_FTP_CLIENT = c.String(nullable: false, maxLength: 1),
                //        P_ID = c.Guid(nullable: false),
                //        C_ID = c.Guid(nullable: false),
                //        P_DIRECTION = c.String(nullable: false, maxLength: 1),
                //        P_DTS = c.String(nullable: false, maxLength: 1),
                //        P_ACTIVE = c.String(nullable: false, maxLength: 1),
                //        P_NOTIFY = c.String(nullable: false, maxLength: 1),
                //        C_NAME = c.String(maxLength: 50),
                //        C_CODE = c.String(maxLength: 15),
                //        C_PATH = c.String(maxLength: 80),
                //        P_C = c.Guid(),
                //        P_REASONCODE = c.String(maxLength: 3),
                //        P_SERVER = c.String(),
                //        P_USERNAME = c.String(),
                //        P_PASSWORD = c.String(),
                //        P_DESCRIPTION = c.String(),
                //        P_PORT = c.String(maxLength: 10),
                //        P_DELIVERY = c.String(maxLength: 1),
                //        P_PATH = c.String(maxLength: 100),
                //        P_XSD = c.String(maxLength: 50),
                //        P_LIBNAME = c.String(maxLength: 50),
                //        P_RECIPIENTID = c.String(maxLength: 15),
                //        P_SENDERID = c.String(maxLength: 15),
                //        P_BILLTO = c.String(maxLength: 15),
                //        P_CHARGEABLE = c.String(maxLength: 1),
                //        P_MSGTYPE = c.String(maxLength: 20),
                //        P_MESSAGETYPE = c.String(maxLength: 3),
                //        P_FILETYPE = c.String(maxLength: 7),
                //        P_EMAILADDRESS = c.String(maxLength: 100),
                //        P_SSL = c.String(maxLength: 1),
                //        P_SENDEREMAIL = c.String(maxLength: 100),
                //        P_SUBJECT = c.String(maxLength: 100),
                //        P_MESSAGEDESCR = c.String(maxLength: 20),
                //        P_EVENTCODE = c.String(maxLength: 3),
                //        P_CUSTOMERCOMPANYNAME = c.String(maxLength: 50),
                //        C_SHORTNAME = c.String(maxLength: 10),
                //        P_METHOD = c.String(maxLength: 50),
                //        P_PARAMLIST = c.String(),
                //        P_FORWARDWITHFLAGS = c.String(maxLength: 1),
                //        P_CWSUBJECT = c.String(maxLength: 80),
                //        P_CWFILENAME = c.String(maxLength: 80),
                //        P_CWAPPCODE = c.String(maxLength: 3),
                //    })
                //.PrimaryKey(t => new { t.C_IS_ACTIVE, t.C_ON_HOLD, t.C_FTP_CLIENT, t.P_ID, t.C_ID, t.P_DIRECTION, t.P_DTS, t.P_ACTIVE, t.P_NOTIFY });
            
            CreateTable(
                "dbo.MapOperation",
                c => new
                    {
                        MD_ID = c.Guid(nullable: false, identity: true),
                        MD_MapDescription = c.String(maxLength: 100),
                        MD_Type = c.String(maxLength: 4),
                        MD_Operation = c.String(maxLength: 50),
                        MD_FromField = c.String(maxLength: 50),
                        MD_ToField = c.String(maxLength: 50),
                        MD_DataType = c.String(maxLength: 3),
                    })
                .PrimaryKey(t => t.MD_ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MapOperation");
            DropTable("dbo.vw_CustomerProfile");
        }
    }
}
