﻿namespace KIF_Sat.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class P_notify : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PROFILE", "P_NOTIFY", c => c.String( maxLength: 1, defaultValue: "N"));
        }

        public override void Down()
        {
            AlterColumn("dbo.PROFILE", "P_NOTIFY", c => c.String(maxLength: 80));
        }
    }
}
