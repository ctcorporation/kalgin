﻿namespace KIF_Sat.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class orgsPk : DbMigration
    {
        public override void Up()
        {

            AlterColumn("dbo.Organisations", "OR_ID", c => c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"));
            AddPrimaryKey("dbo.Organisations", "OR_ID");
        }

        public override void Down()
        {
            DropPrimaryKey("dbo.Organisations");
            AlterColumn("dbo.Organisations", "OR_ID", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.Organisations", "OR_ID");
        }
    }
}
