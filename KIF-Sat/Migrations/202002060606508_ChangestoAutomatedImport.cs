﻿namespace KIF_Sat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangestoAutomatedImport : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PROFILE", "P_NOTIFY", c => c.String(maxLength: 80));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PROFILE", "P_NOTIFY");
        }
    }
}
