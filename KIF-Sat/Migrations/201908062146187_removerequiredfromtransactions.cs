namespace KIF_Sat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removerequiredfromtransactions : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Transactions", "T_TRIAL", c => c.String(maxLength: 1, fixedLength: true, unicode: false));
            AlterColumn("dbo.Transactions", "T_INVOICED", c => c.String(maxLength: 1, fixedLength: true, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Transactions", "T_INVOICED", c => c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false));
            AlterColumn("dbo.Transactions", "T_TRIAL", c => c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false));
        }
    }
}
