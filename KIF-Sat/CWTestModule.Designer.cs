﻿namespace KIF_Sat
{
    partial class CWTestModule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.edRecipientID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.edSendPassword = new System.Windows.Forms.TextBox();
            this.lblSendPassword = new System.Windows.Forms.Label();
            this.edSendUsername = new System.Windows.Forms.TextBox();
            this.lblSendUsername = new System.Windows.Forms.Label();
            this.edSendeAdapterResults = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.bbSend = new System.Windows.Forms.Button();
            this.bbDir = new System.Windows.Forms.Button();
            this.edSendFile = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnTest = new System.Windows.Forms.Button();
            this.edSendCTCUrl = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.bbClose = new System.Windows.Forms.Button();
            this.fdSendFile = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(141, 11);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(403, 21);
            this.comboBox1.TabIndex = 57;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(133, 13);
            this.label6.TabIndex = 56;
            this.label6.Text = "Select Send Profile to Test";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(10, 262);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 53;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            // 
            // edRecipientID
            // 
            this.edRecipientID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edRecipientID.Location = new System.Drawing.Point(404, 62);
            this.edRecipientID.Name = "edRecipientID";
            this.edRecipientID.Size = new System.Drawing.Size(100, 20);
            this.edRecipientID.TabIndex = 52;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(337, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 51;
            this.label5.Text = "Recipient ID";
            // 
            // edSendPassword
            // 
            this.edSendPassword.Location = new System.Drawing.Point(141, 84);
            this.edSendPassword.Margin = new System.Windows.Forms.Padding(2);
            this.edSendPassword.Name = "edSendPassword";
            this.edSendPassword.Size = new System.Drawing.Size(179, 20);
            this.edSendPassword.TabIndex = 49;
            // 
            // lblSendPassword
            // 
            this.lblSendPassword.AutoSize = true;
            this.lblSendPassword.Location = new System.Drawing.Point(7, 86);
            this.lblSendPassword.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSendPassword.Name = "lblSendPassword";
            this.lblSendPassword.Size = new System.Drawing.Size(53, 13);
            this.lblSendPassword.TabIndex = 50;
            this.lblSendPassword.Text = "Password";
            // 
            // edSendUsername
            // 
            this.edSendUsername.Location = new System.Drawing.Point(141, 60);
            this.edSendUsername.Margin = new System.Windows.Forms.Padding(2);
            this.edSendUsername.Name = "edSendUsername";
            this.edSendUsername.Size = new System.Drawing.Size(179, 20);
            this.edSendUsername.TabIndex = 47;
            // 
            // lblSendUsername
            // 
            this.lblSendUsername.AutoSize = true;
            this.lblSendUsername.Location = new System.Drawing.Point(7, 61);
            this.lblSendUsername.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSendUsername.Name = "lblSendUsername";
            this.lblSendUsername.Size = new System.Drawing.Size(55, 13);
            this.lblSendUsername.TabIndex = 48;
            this.lblSendUsername.Text = "Username";
            // 
            // edSendeAdapterResults
            // 
            this.edSendeAdapterResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edSendeAdapterResults.Location = new System.Drawing.Point(141, 132);
            this.edSendeAdapterResults.Multiline = true;
            this.edSendeAdapterResults.Name = "edSendeAdapterResults";
            this.edSendeAdapterResults.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.edSendeAdapterResults.Size = new System.Drawing.Size(541, 237);
            this.edSendeAdapterResults.TabIndex = 46;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 136);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 45;
            this.label2.Text = "Results";
            // 
            // bbSend
            // 
            this.bbSend.Location = new System.Drawing.Point(584, 106);
            this.bbSend.Name = "bbSend";
            this.bbSend.Size = new System.Drawing.Size(100, 23);
            this.bbSend.TabIndex = 44;
            this.bbSend.Text = "Send File";
            this.bbSend.UseVisualStyleBackColor = true;
            this.bbSend.Click += new System.EventHandler(this.bbSend_Click);
            // 
            // bbDir
            // 
            this.bbDir.Location = new System.Drawing.Point(517, 106);
            this.bbDir.Name = "bbDir";
            this.bbDir.Size = new System.Drawing.Size(28, 23);
            this.bbDir.TabIndex = 43;
            this.bbDir.Text = "...";
            this.bbDir.UseVisualStyleBackColor = true;
            this.bbDir.Click += new System.EventHandler(this.bbDir_Click);
            // 
            // edSendFile
            // 
            this.edSendFile.Location = new System.Drawing.Point(141, 108);
            this.edSendFile.Name = "edSendFile";
            this.edSendFile.Size = new System.Drawing.Size(378, 20);
            this.edSendFile.TabIndex = 42;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 41;
            this.label3.Text = "Send File";
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(584, 36);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(100, 23);
            this.btnTest.TabIndex = 40;
            this.btnTest.Text = "Test Connection";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // edSendCTCUrl
            // 
            this.edSendCTCUrl.Location = new System.Drawing.Point(141, 36);
            this.edSendCTCUrl.Name = "edSendCTCUrl";
            this.edSendCTCUrl.Size = new System.Drawing.Size(403, 20);
            this.edSendCTCUrl.TabIndex = 39;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 13);
            this.label1.TabIndex = 38;
            this.label1.Text = "Cargowise eAdapter URL";
            // 
            // bbClose
            // 
            this.bbClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bbClose.Location = new System.Drawing.Point(582, 375);
            this.bbClose.Name = "bbClose";
            this.bbClose.Size = new System.Drawing.Size(100, 23);
            this.bbClose.TabIndex = 37;
            this.bbClose.Text = "C&lose";
            this.bbClose.UseVisualStyleBackColor = true;
            this.bbClose.Click += new System.EventHandler(this.bbClose_Click);
            // 
            // fdSendFile
            // 
            this.fdSendFile.DefaultExt = "xml";
            this.fdSendFile.FileName = "openFileDialog1";
            this.fdSendFile.Filter = "XML Files|*.xml|All Files| *.*";
            // 
            // CWTestModule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 410);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.edRecipientID);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.edSendPassword);
            this.Controls.Add(this.lblSendPassword);
            this.Controls.Add(this.edSendUsername);
            this.Controls.Add(this.lblSendUsername);
            this.Controls.Add(this.edSendeAdapterResults);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bbSend);
            this.Controls.Add(this.bbDir);
            this.Controls.Add(this.edSendFile);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.edSendCTCUrl);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bbClose);
            this.Name = "CWTestModule";
            this.Text = "CWTestModule";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox edRecipientID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox edSendPassword;
        private System.Windows.Forms.Label lblSendPassword;
        private System.Windows.Forms.TextBox edSendUsername;
        private System.Windows.Forms.Label lblSendUsername;
        private System.Windows.Forms.TextBox edSendeAdapterResults;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bbSend;
        private System.Windows.Forms.Button bbDir;
        private System.Windows.Forms.TextBox edSendFile;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.TextBox edSendCTCUrl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bbClose;
        private System.Windows.Forms.OpenFileDialog fdSendFile;
    }
}