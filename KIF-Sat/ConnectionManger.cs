﻿using NodeResources;
using System.Configuration;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;

namespace KIF_Sat
{
    public class ConnectionManager : IConnectionManager
    {
        private string _serverName { get; set; }
        private string _databaseName { get; set; }
        private string _user { get; set; }
        private string _password { get; set; }
        public string ConnString { get; set; }

        public string ServerName
        {
            get { return _serverName; }
            set { _serverName = value; }
        }
        public string DatabaseName
        {
            get { return _databaseName; }
            set { _databaseName = value; }
        }

        public string User
        {
            get { return _user; }
            set { _user = value; }
        }
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public ConnectionManager()
        {
            ConnectionStringSettings _settings = ConfigurationManager.ConnectionStrings["KIFEntity"];
            ConnString = _settings.ConnectionString.ToString();
        }

        public ConnectionManager(string server, string database, string userName, string password)
        {
            Password = password;
            DatabaseName = database;
            ServerName = server;
            User = userName;
            ConnString = BuildEntityConnectionString(BuildConnectionString(ServerName, DatabaseName, User, Password));
            ConnString = BuildConnectionString(ServerName, DatabaseName, User, Password);
        }

        public string BuildConnectionString(string server, string database, string user, string password)
        {
            Password = password;
            DatabaseName = database;
            ServerName = server;
            User = user;

            var sqlBuilder = new SqlConnectionStringBuilder
            {
                DataSource = ServerName,
                InitialCatalog = DatabaseName,
                UserID = User,
                Password = Password
            };

            return sqlBuilder.ToString();

        }

        public string BuildEntityConnectionString(string sqlString)
        {
            EntityConnectionStringBuilder esb = new EntityConnectionStringBuilder();
            //esb.Metadata = "res://*/Models.NodeModel.csdl|res://*/Models.NodeModel.ssdl|res://*/Models.NodeModel.msl";
            esb.Provider = "System.Data.SqlClient";

            esb.ProviderConnectionString = sqlString;
            return esb.ToString();

        }
    }
}
