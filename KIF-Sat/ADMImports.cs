﻿using KIF_Sat.DTO;
using KIF_Sat.Models;
using NodeResources;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using XMLLocker.CTC;

namespace KIF_Sat
{
    public class ADMImport
    {
        #region Public Properties
        public string ErrorString
        {
            get
            { return _errString; }
            set
            { _errString = value; }
        }
        public string Summary
        {
            get
            {
                return _summary;
            }

        }

        public Transaction TrLog
        {
            get
            { return _tr; }
            set
            { _tr = value; }
        }
        #endregion

        #region Private Members
        private Transaction _tr;
        private string _importFile;
        private string _errString;
        private string _connstring;
        private List<MapOperation> _map;
        string prevOrderNo = string.Empty;
        string nextOrderNo = string.Empty;
        private string _summary = string.Empty;
        string containerTypeField = string.Empty;
        string orderNoField = string.Empty;
        private string _senderID = string.Empty;
        private string _recipientID = string.Empty;
        #endregion

        #region Constructors
        public ADMImport(string fileToImport, string connString, string senderId, string recipeintID)
        {
            _importFile = fileToImport;
            _connstring = connString;
            _recipientID = recipeintID;
            _senderID = senderId;
        }
        #endregion

        #region Methods
        public bool CreateOrder()
        {
            var tb = ExcelToDB(_importFile);
            _map = BuildMap(tb);
            if (_map == null)
            {
                _summary = "Import Failed. File is of a Different Format. There is no maping found for this.";
                return false;
            }
            if (_map.Count == 0)
            {
                return false;
            }
            CreateCommonfromTable(tb, _map);
            if (_tr == null)
            {
                _tr = new Transaction();
                _tr.T_FILENAME = _importFile;

            }

            _tr.T_DATETIME = DateTime.Now;
            _tr.T_ID = Guid.NewGuid();

            return true;
        }
        private string CreateCommonfromTable(DataTable tb, List<MapOperation> map)
        {
            prevOrderNo = "";

            int iLineNo = 0;
            var lineNoField = ReturnStringFromList(map, "Order", "LineNo");
            List<NodeFileTrackingOrder> TrackingOrders = new List<NodeFileTrackingOrder>();
            List<OrderLineElement> orderLines = new List<OrderLineElement>();
            var productCodeField = ReturnStringFromList(map, "Order", "ProductCode");
            var productField = ReturnStringFromList(map, "Order", "ProductDesc");
            var weightField = ReturnStringFromList(map, "Order", "OrdQty");
            containerTypeField = ReturnStringFromList(map, "Order", "ContainerMode");
            orderNoField = ReturnStringFromList(map, "Order", "OrderNo");
            _summary += "Begin Conversion:";
            NodeFileTrackingOrder order = new NodeFileTrackingOrder();
            NodeFile nodeFile = new NodeFile();
            for (int i = 0; i < tb.Rows.Count; i++)
            {
                if (prevOrderNo != tb.Rows[i][orderNoField].ToString() && order == null)
                {
                    orderLines = new List<OrderLineElement>();
                }
                // Begin by creating the Order Lines Arrary or adding to it. 
                // The array will be reset if not needed. 
                OrderLineElement line = new OrderLineElement();
                iLineNo = orderLines.Count + 1;
                line.LineNo = iLineNo;
                line.Product = new ProductElement()
                { Code = tb.Rows[i][productCodeField].ToString(), Description = tb.Rows[i][productField].ToString() };
                if (!string.IsNullOrEmpty(weightField))
                {
                    line.Weight = decimal.Parse(tb.Rows[i][weightField].ToString());
                    line.WeightSpecified = true;
                    line.WeightUnit = OrderLineElementWeightUnit.KG;
                    line.WeightUnitSpecified = true;
                }

                orderLines.Add(line);

                CompanyElement cust = new CompanyElement();


                if ((tb.Rows[i][orderNoField].ToString() != prevOrderNo))
                {
                    // Build Order Header
                    // 
                    prevOrderNo = tb.Rows[i][orderNoField].ToString();

                    _summary += "Order " + prevOrderNo + " Line " + iLineNo + Environment.NewLine;
                    order = new NodeFileTrackingOrder();
                    order = CreateOrderHeader(tb.Rows[i]);
                    if (order == null)
                    {
                        if (orderLines.Count == 1)
                        {
                            _summary += "Organisations missing/not found. Skipping." + Environment.NewLine;
                        }
                        if (tb.Rows.Count > i + 1)
                        {


                            if (prevOrderNo != tb.Rows[i + 1][orderNoField].ToString())
                            {
                                // Looks ahead to see if it is the last Line of the Current order. 
                                // If it is the last line then reset the OrderLines to null
                                orderLines = null;
                            }
                        }
                        continue;
                    }

                }
                //Look ahead for the next Order Number to check for multiple lines from the same order number. 
                try
                {
                    nextOrderNo = tb.Rows[i + 1][orderNoField].ToString();
                }
                catch (NullReferenceException)
                {
                    nextOrderNo = string.Empty;
                }
                catch (IndexOutOfRangeException)
                {
                    nextOrderNo = string.Empty;
                }
                // end of the Order and therefore close the file Add Lines and Identity Matrix. Create Order File.
                if (nextOrderNo != prevOrderNo)
                {

                    if (order == null)
                    {
                        // This only occurs when there are multiple lines in an Order and it is missing companies.
                        // Reason for this is that when OrderLine count >1 it skips the opportunity to continue (as it doesn't need to try and create the order header again)
                        orderLines = null;
                        continue;
                    }
                    order.OrderLines = orderLines.ToArray();
                    orderLines = new List<OrderLineElement>();

                    nodeFile.IdendtityMatrix = new NodeFileIdendtityMatrix
                    {
                        SenderId = _senderID,
                        CustomerId = _recipientID,
                        DocumentType = "Order"
                    };
                    TrackingOrders.Add(order);

                    nodeFile.TrackingOrders = TrackingOrders.ToArray();

                    order = null;
                    orderLines = null;
                }
                _summary += "Order " + prevOrderNo + " Imported Ok." + Environment.NewLine;

            }
            CommonToCw cw = new CommonToCw();

            XUS.UniversalInterchange cwXML = nodeFile.IdendtityMatrix != null ? cw.CreateOrderFile(nodeFile, Globals.glOutputDir) : null;

            //// Fix Log Location
            //ToCargowise toCargowise = new ToCargowise(Globals.glLibPath);
            //toCargowise.ToFile(cwXML, nodeFile.IdendtityMatrix.SenderId + "-ORD-" + prevOrderNo + ".XML", Globals.glOutputDir);

            return _summary;


        }

        private NodeFileTrackingOrder CreateOrderHeader(DataRow dataRow)
        {

            NodeFileTrackingOrder order = new NodeFileTrackingOrder();
            order.IncoTerms = "EXW";
            order.Dates = GetDatesFromRow(dataRow).ToArray();
            List<CompanyElement> cmp = GetCompaniesFromRow(dataRow);
            if (cmp == null)
            {
                return null;
            }
            order.Companies = cmp.ToArray();
            order.ContainerMode = GetContainerModeFromRow(dataRow[containerTypeField].ToString());
            order.TransportMode = order.ContainerMode == "LSE" ? "AIR" : "SEA";
            order.OrderNo = dataRow[orderNoField].ToString();

            return order;
        }

        private string GetContainerModeFromRow(string containerType)
        {
            string containerMode = string.Empty;
            switch (containerType.ToUpper())
            {
                case "AIRFREIGHT":
                    containerMode = "LSE";

                    break;
                case "LCL SEAFREIGHT":
                    containerMode = "LCL";
                    break;
                default:
                    if (containerType.Contains("20") || (containerType.Contains("40")))
                    {
                        containerMode = "FCL";
                    }
                    break;
            }
            return containerMode;
        }

        private DataTable ExcelToDB(string filetoOpen)
        {
            var tb = ExcelToTable.ToTable(filetoOpen);

            return tb;
        }

        private List<MapOperation> BuildMap(DataTable tb)
        {
            using (IUnitOfWork uow = new UnitOfWork(new KifEntity(_connstring)))
            {
                int fCount = tb.Columns.Count;
                string firstCol = tb.Columns[0].ColumnName;
                var fd = uow.FileDescriptions.Find(x => x.PC_FieldCount == fCount
                                                            && x.PC_FirstFieldName.Trim() == firstCol).FirstOrDefault();
                // FileDescription has been found.
                if (fd != null)
                {
                    var me = uow.MappingEnums.Find(x => x.M_PC == fd.PC_Id);
                    if (me != null)
                    {
                        List<MapOperation> mapOperation = new List<MapOperation>();
                        foreach (var m in me)
                        {

                            MapOperation map = new MapOperation
                            {
                                MD_DataType = m.M_FieldType,
                                MD_Operation = m.M_Operation,
                                MD_MapDescription = m.M_Description,
                                MD_FromField = m.M_From,
                                MD_ToField = m.M_To,
                                MD_Type = m.M_MapType
                            };
                            mapOperation.Add(map);
                        }
                        return mapOperation;
                    }
                }

            }
            return null;

        }
        #endregion

        #region Helpers

        private void SendAlertMsg(string orderNo, string companyType, string companyName, string fieldName)
        {

            string errorSummary = string.Empty;
            string errMessage = "Please do not change the layout of this email as it is used to create the appropriate Organisation mapping for Cargowise." + Environment.NewLine
                + " Please fill out and forward to csr@ctcorp.com.au ";
            errMessage += "--------------------------------------------------------------" + Environment.NewLine;
            errMessage += "Company Type: " + companyType + Environment.NewLine;
            errMessage += "From Field(" + fieldName + ")" + Environment.NewLine;
            errMessage += "Order No:" + orderNo + Environment.NewLine;
            errMessage += "Company Name: " + companyName + Environment.NewLine;
            errMessage += "Short Name: " + Environment.NewLine;
            errMessage += "Address: " + Environment.NewLine;
            errMessage += "City: " + Environment.NewLine;
            errMessage += "State: " + Environment.NewLine;
            errMessage += "Post Code: " + Environment.NewLine;
            errMessage += "Country Code: " + Environment.NewLine;
            errMessage += "Port Of Discharge Code: " + Environment.NewLine;
            errMessage += "Please also confirm the Origin Codes" + Environment.NewLine;
            errMessage += "(Sea) Port of Origin: USCHI" + Environment.NewLine;
            errMessage += "(Sea) Port of Loading: USLGB" + Environment.NewLine;
            errMessage += "(Air) Port of Origin: USORD" + Environment.NewLine;
            errMessage += "(Air) Port of Loading: USORD" + Environment.NewLine;
            errMessage += "Paste Cargowise Code here:" + Environment.NewLine;
            using (MailModule mail = new MailModule(Globals.glMailServerSettings, "csr@ctcorp.com.au", Globals.glTestLocation))
            {
                mail.SendMsg("", Globals.glAlertsTo, "Errors found when importing ADM Order (" + orderNo + ")", errMessage);
            }

        }

        private static string ReturnStringFromList(List<MapOperation> map, string operation, string stringToFind)
        {
            var value = (from x in map
                         where x.MD_Operation == operation
                         && x.MD_ToField == stringToFind
                         select x.MD_FromField).FirstOrDefault();
            return value;
        }

        private string GetEnum(string enumToFind)
        {
            using (IUnitOfWork uow = new UnitOfWork(new KifEntity(_connstring)))
            {
                var val = uow.Cargowise_Enums.Find(x => x.CW_ENUM == enumToFind).FirstOrDefault();
                if (val != null)
                {
                    return val.CW_MAPVALUE;
                }
            }
            return null;
        }

        private List<CompanyElement> GetCompaniesFromRow(DataRow dataRow)
        {
            List<CompanyElement> companies = new List<CompanyElement>();

            var consigneeField = ReturnStringFromList(_map, "Order", "Consignee");
            var consignee = ReturnCompanyFromString(dataRow[consigneeField].ToString(), "Consignee", consigneeField);
            bool validCompany = true;
            if (consignee == null)
            {
                validCompany = false;
                _errString += "Consignee not found:" + Environment.NewLine;
                _summary += "Consignee not found" + Environment.NewLine;

            }
            else
            {
                companies.Add(consignee);
            }
            var supplierField = ReturnStringFromList(_map, "Order", "Supplier");
            var supplier = ReturnCompanyFromString(dataRow[supplierField].ToString(), "Supplier", supplierField);
            if (supplier == null)
            {
                validCompany = false;
                _errString += "Supplier not found:" + Environment.NewLine;

                _summary += "Supplier not found" + Environment.NewLine;
            }
            else
            {
                companies.Add(supplier);
            }
            var agentField = ReturnStringFromList(_map, "Order", "ForwardingAgent");
            var agent = ReturnCompanyFromString(dataRow[agentField].ToString(), "ForwardingAgent", agentField);
            if (agent == null)
            {
                validCompany = false;
                _errString += "Agent not found:" + Environment.NewLine;

                _summary += "Agent not found" + Environment.NewLine;
            }
            else
            {
                companies.Add(agent);
            }
            var deliverToField = ReturnStringFromList(_map, "Order", "DeliverTo");
            var deliverTo = ReturnCompanyFromString(dataRow[deliverToField].ToString(), "DeliverTo", deliverToField);
            if (deliverTo == null)
            {
                validCompany = false;
                _errString += "Deliver To not found:" + Environment.NewLine;

                _summary += "Deliver To Company not found" + Environment.NewLine;
            }
            else
            {
                companies.Add(deliverTo);
            }
            if (!validCompany)
            {
                return null;
            }
            return companies;
        }

        private CompanyElement ReturnCompanyFromString(string company, string companyType, string fieldname)
        {
            var mapValue = company.RemoveChars('\n');
            using (IUnitOfWork uow = new UnitOfWork(new KifEntity(Globals.ConnString.ConnString)))
            {
                var comp = uow.Organisations.Find(x => x.OR_MapValue.Trim().ToUpper() == mapValue.Trim().ToUpper() && x.OR_Type == companyType).FirstOrDefault();
                if (comp == null)
                {
                    SendAlertMsg(prevOrderNo, companyType, company, fieldname);
                    return null;
                }
                else
                {
                    CompanyElement foundCompany = new CompanyElement();
                    CompanyElementCompanyType compType;
                    if (Enum.TryParse(companyType, true, out compType))
                    {
                        foundCompany.CompanyType = compType;
                    }
                    foundCompany.Address1 = !string.IsNullOrEmpty(comp.OR_Address1) ? comp.OR_Address1.Trim() : string.Empty;
                    foundCompany.Address2 = !string.IsNullOrEmpty(comp.OR_Address2) ? comp.OR_Address2.Trim() : string.Empty;
                    foundCompany.CountryCode = !string.IsNullOrEmpty(comp.OR_Address3) ? comp.OR_Address3.Trim() : string.Empty;
                    foundCompany.PostCode = !string.IsNullOrEmpty(comp.OR_PostCode) ? comp.OR_PostCode.Trim() : string.Empty;
                    foundCompany.State = !string.IsNullOrEmpty(comp.OR_State) ? comp.OR_State.Trim() : string.Empty;
                    foundCompany.CompanyOrgCode = !string.IsNullOrEmpty(comp.OR_CODE) ? comp.OR_CODE.Trim() : string.Empty;
                    foundCompany.CompanyCode = !string.IsNullOrEmpty(comp.OR_ShortCode) ? comp.OR_ShortCode.Trim() : string.Empty;
                    return foundCompany;
                }

            }
        }

        private List<DateElement> GetDatesFromRow(DataRow dataRow)
        {
            string dateFormat = "yyyy-MM-ddTHH:mm:ss";
            List<DateElement> Dates = new List<DateElement>();
            var orderDate = ReturnStringFromList(_map, "Order", "OrderDate");
            var requiredDate = ReturnStringFromList(_map, "Order", "DeliverBy");
            var etaDate = ReturnStringFromList(_map, "Order", "ETA");
            int i = 0;
            string convertedDate = string.Empty;
            if (int.TryParse(dataRow[orderDate].ToString(), out i))
            {
                convertedDate = DateTime.FromOADate(i).ToString(dateFormat);
            }
            else
            {
                convertedDate = string.Empty;
            }

            var date = new DateElement
            {
                DateType = DateElementDateType.Ordered,
                ActualDate = convertedDate
            };
            if (!string.IsNullOrEmpty(date.ActualDate))
            {
                Dates.Add(date);
            }
            if (int.TryParse(dataRow[requiredDate].ToString(), out i))
            {
                convertedDate = DateTime.FromOADate(i).ToString(dateFormat);
            }
            else
            {
                convertedDate = string.Empty;
            }
            date = new DateElement
            {
                DateType = DateElementDateType.DeliverBy,
                EstimateDate = convertedDate
            };

            if (!string.IsNullOrEmpty(date.EstimateDate))
            {
                Dates.Add(date);
            }
            if (int.TryParse(dataRow[etaDate].ToString(), out i))
            {
                convertedDate = DateTime.FromOADate(i).ToString(dateFormat);
            }
            else
            {
                convertedDate = string.Empty;
            }
            date = new DateElement
            {
                DateType = DateElementDateType.Arrival,
                EstimateDate = convertedDate
            };
            if (!string.IsNullOrEmpty(date.EstimateDate))
            {
                Dates.Add(date);
            }
            return Dates;


        }
        #endregion
    }
}
