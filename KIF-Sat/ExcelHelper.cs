﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Text.RegularExpressions;

namespace KIF_Sat
{
    public static class ExcelHelper
    {
        public static int? GetColumnIndexFromName(string columnName)
        {
            string name = columnName;
            int no = 0;
            int pow = 1;
            for (int i = name.Length - 1; i >= 0; i--)
            {
                no += (name[i] - 'A' + 1) * pow;
                pow *= 26;
            }
            return no;
        }

        public static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            if (cell.CellValue == null)
            {
                return "";
            }
            string value = cell.CellValue.InnerXml;

            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            else
            {
                return value;

            }
        }

        public static string GetColumnName(string cellReference)
        {
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellReference);
            return match.Value;
        }
    }
}
