namespace KIF_Sat.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Customer")]
    public partial class Customer
    {
        [Key]
        public Guid C_ID { get; set; }

        [StringLength(50)]
        public string C_NAME { get; set; }

        [Required]
        [StringLength(1)]
        public string C_IS_ACTIVE { get; set; }

        [Required]
        [StringLength(1)]
        public string C_ON_HOLD { get; set; }

        [StringLength(80)]
        public string C_PATH { get; set; }

        [Required]
        [StringLength(1)]
        public string C_FTP_CLIENT { get; set; }

        [StringLength(15)]
        public string C_CODE { get; set; }

        [Required]
        [StringLength(1)]
        public string C_TRIAL { get; set; }

        public DateTime? C_TRIALSTART { get; set; }

        public DateTime? C_TRIALEND { get; set; }

        [StringLength(10)]
        public string C_SHORTNAME { get; set; }
    }
}
