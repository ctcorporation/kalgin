namespace KIF_Sat.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CargowiseContext")]
    public partial class CargowiseContext
    {
        [Key]
        public Guid CC_ID { get; set; }

        [StringLength(50)]
        public string CC_Context { get; set; }

        public string CC_Description { get; set; }
    }
}
