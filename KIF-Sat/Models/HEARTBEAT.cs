﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KIF_Sat.Models
{
    [Table("HEARTBEAT")]
    public partial class HeartBeat
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid HB_ID { get; set; }

        public Guid? HB_C { get; set; }

        [StringLength(50)]
        public string HB_NAME { get; set; }

        [StringLength(200)]
        public string HB_PATH { get; set; }

        [Required]
        [StringLength(1)]
        public string HB_ISMONITORED { get; set; }

        public int? HB_PID { get; set; }

        [StringLength(50)]
        public string HB_LASTOPERATION { get; set; }

        public DateTime? HB_LASTCHECKIN { get; set; }

        public DateTime? HB_OPENED { get; set; }

        [StringLength(100)]
        public string HB_LASTOPERATIONPARAMS { get; set; }
    }
}
