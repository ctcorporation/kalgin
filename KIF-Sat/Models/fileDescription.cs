﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KIF_Sat.Models
{
    [Table("FileDescription")]
    public class FileDescription
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid PC_Id { get; set; }

        [MaxLength(50)]
        public string PC_FileName { get; set; }

        public bool PC_HasHeader { get; set; }

        public bool PC_Delimiter { get; set; }

        public int PC_FieldCount { get; set; }

        public bool PC_Quotations { get; set; }

        public int PC_HeaderStartFrom { get; set; }

        public int PC_DataStartFrom { get; set; }

        public string PC_FirstFieldName { get; set; }

        public string PC_LastFieldName { get; set; }

        [MaxLength(5)]
        public string PC_FileExtension { get; set; }

        public ICollection<MappingEnum> MappingEnumFileDescription { get; set; }
    }
}