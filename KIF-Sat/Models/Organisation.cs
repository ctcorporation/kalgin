namespace KIF_Sat.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class Organisation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid OR_ID { get; set; }

        [StringLength(20)]
        public string OR_CODE { get; set; }

        [StringLength(100)]
        public string OR_Name { get; set; }

        [StringLength(100)]
        public string OR_Address1 { get; set; }

        [StringLength(100)]
        public string OR_Address2 { get; set; }

        [StringLength(100)]
        public string OR_Address3 { get; set; }

        [StringLength(10)]
        public string OR_PostCode { get; set; }

        [StringLength(50)]
        public string OR_Suburb { get; set; }

        [StringLength(10)]
        public string OR_State { get; set; }

        [StringLength(200)]
        public string OR_MapValue { get; set; }

        [StringLength(50)]
        public string OR_Type { get; set; }

        [StringLength(5)]
        public string OR_PortOfLoading { get; set; }

        [StringLength(5)]
        public string OR_PortOfDischarge { get; set; }

        [StringLength(50)]
        public string OR_ShortCode { get; set; }

        [StringLength(5)]
        public string OR_AirPortOfLoading { get; set; }

        [StringLength(5)]
        public string OR_AirPortOfOrigin { get; set; }

        [StringLength(5)]
        public string OR_SeaPortOfOrigin { get; set; }
    }
}
