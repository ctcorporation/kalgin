namespace KIF_Sat.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Processing_Error
    {
        [Key]
        public Guid E_PK { get; set; }

        [StringLength(15)]
        public string E_SENDERID { get; set; }

        [StringLength(15)]
        public string E_RECIPIENTID { get; set; }

        public DateTime? E_PROCDATE { get; set; }

        public string E_FILENAME { get; set; }

        public string E_ERRORDESC { get; set; }

        [StringLength(10)]
        public string E_ERRORCODE { get; set; }

        public Guid? E_P { get; set; }

        [Required]
        [StringLength(1)]
        public string E_IGNORE { get; set; }
    }
}
