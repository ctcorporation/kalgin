﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace KIF_Sat.Models
{
    [Table("MapOperation")]
    public class MapOperation : DbContext
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid MD_ID { get; set; }

        [MaxLength(100)]
        public string MD_MapDescription { get; set; }
        [MaxLength(4)]
        public string MD_Type { get; set; }
        [MaxLength(50)]
        public string MD_Operation { get; set; }
        [MaxLength(50)]
        public string MD_FromField { get; set; }

        [MaxLength(50)]
        public string MD_ToField { get; set; }
        [MaxLength(3)]
        public string MD_DataType { get; set; }

    }

}
