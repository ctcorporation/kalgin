﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KIF_Sat.Models
{
    [Table("vw_CustomerProfile")]
    public class CustomerProfile
    {

        [StringLength(50)]
        public string C_NAME { get; set; }

        [StringLength(15)]
        public string C_CODE { get; set; }

        [Key]
        [Column(Order = 0)]
        [StringLength(1)]
        public string C_IS_ACTIVE { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(1)]
        public string C_ON_HOLD { get; set; }

        [StringLength(80)]
        public string C_PATH { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(1)]
        public string C_FTP_CLIENT { get; set; }

        [Key]
        [Column(Order = 3)]
        public Guid P_ID { get; set; }

        public Guid? P_C { get; set; }

        [StringLength(3)]
        public string P_REASONCODE { get; set; }

        public string P_SERVER { get; set; }

        public string P_USERNAME { get; set; }

        public string P_PASSWORD { get; set; }

        [Key]
        [Column(Order = 4)]
        public Guid C_ID { get; set; }

        public string P_DESCRIPTION { get; set; }

        [StringLength(10)]
        public string P_PORT { get; set; }

        [StringLength(1)]
        public string P_DELIVERY { get; set; }

        [StringLength(100)]
        public string P_PATH { get; set; }

        [Key]
        [Column(Order = 5)]
        [StringLength(1)]
        public string P_DIRECTION { get; set; }

        [StringLength(50)]
        public string P_XSD { get; set; }

        [StringLength(50)]
        public string P_LIBNAME { get; set; }

        [StringLength(15)]
        public string P_RECIPIENTID { get; set; }

        [StringLength(15)]
        public string P_SENDERID { get; set; }

        [Key]
        [Column(Order = 6)]
        [StringLength(1)]
        public string P_DTS { get; set; }

        [StringLength(15)]
        public string P_BILLTO { get; set; }

        [StringLength(1)]
        public string P_CHARGEABLE { get; set; }

        [StringLength(20)]
        public string P_MSGTYPE { get; set; }

        [StringLength(3)]
        public string P_MESSAGETYPE { get; set; }

        [Key]
        [Column(Order = 7)]
        [StringLength(1)]
        public string P_ACTIVE { get; set; }

        [StringLength(7)]
        public string P_FILETYPE { get; set; }

        [StringLength(100)]
        public string P_EMAILADDRESS { get; set; }

        [StringLength(1)]
        public string P_SSL { get; set; }

        [StringLength(100)]
        public string P_SENDEREMAIL { get; set; }

        [StringLength(100)]
        public string P_SUBJECT { get; set; }

        [StringLength(20)]
        public string P_MESSAGEDESCR { get; set; }

        [StringLength(3)]
        public string P_EVENTCODE { get; set; }

        [StringLength(50)]
        public string P_CUSTOMERCOMPANYNAME { get; set; }

        [StringLength(10)]
        public string C_SHORTNAME { get; set; }

        [StringLength(50)]
        public string P_METHOD { get; set; }

        public string P_PARAMLIST { get; set; }

        [Key]
        [Column(Order = 8)]
        [StringLength(1)]
        public string P_NOTIFY { get; set; }

        [StringLength(1)]
        public string P_FORWARDWITHFLAGS { get; set; }

        [StringLength(80)]
        public string P_CWSUBJECT { get; set; }

        [StringLength(80)]
        public string P_CWFILENAME { get; set; }

        [StringLength(3)]
        public string P_CWAPPCODE { get; set; }
    }
}



