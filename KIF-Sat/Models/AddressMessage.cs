﻿namespace KIF_Sat.Models
{
    public class AddressMessage
    {
        public string CompanyType { get; set; }
        public string OrderNo { get; set; }
        public string CompanyName { get; set; }
        public string ShortName { get; set; }
        public string MapValue { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }
        public string CountryCode { get; set; }
        public string Discharge { get; set; }
        public string CWCode { get; set; }
        public string POOS { get; set; }
        public string POLS { get; set; }
        public string POOA { get; set; }
        public string POLA { get; set; }



    }
}
