﻿using System.Data.Entity;

namespace KIF_Sat.Models
{
    public partial class CNodeEntity : DbContext
    {
        public CNodeEntity(string connString)
            : base(connString)
        {
        }

        public virtual DbSet<HeartBeat> HeartBeat { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<HeartBeat>()
                .Property(e => e.HB_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<HeartBeat>()
                .Property(e => e.HB_PATH)
                .IsUnicode(false);

            modelBuilder.Entity<HeartBeat>()
                .Property(e => e.HB_ISMONITORED)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<HeartBeat>()
                .Property(e => e.HB_LASTOPERATION)
                .IsUnicode(false);
        }
    }
}
