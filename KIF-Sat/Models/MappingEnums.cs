﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KIF_Sat.Models
{
    [Table("MAPPING_ENUM")]
    public class MappingEnum
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid M_Id { get; set; }

        [MaxLength(100)]
        public string M_Description { get; set; }

        [MaxLength(20)]
        public string M_MapType { get; set; }

        [MaxLength(30)]
        public string M_Operation { get; set; }

        [MaxLength(3)]
        public string M_FieldType { get; set; }

        [MaxLength(50)]
        public string M_From { get; set; }

        [MaxLength(50)]
        public string M_To { get; set; }

        [ForeignKey("FileDescriptionMappingEnum")]
        public Guid? M_PC { get; set; }

        public virtual FileDescription FileDescriptionMappingEnum { get; set; }


    }
}
