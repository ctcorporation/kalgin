namespace KIF_Sat.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DTS")]
    public partial class DTS
    {
        [Key]
        public Guid D_ID { get; set; }

        public Guid? D_C { get; set; }

        public int? D_INDEX { get; set; }

        [Required]
        [StringLength(1)]
        public string D_FINALPROCESSING { get; set; }

        public Guid? D_P { get; set; }

        [StringLength(3)]
        public string D_FILETYPE { get; set; }

        [StringLength(1)]
        public string D_DTSTYPE { get; set; }

        [StringLength(1)]
        public string D_DTS { get; set; }

        public string D_SEARCHPATTERN { get; set; }

        public string D_NEWVALUE { get; set; }

        public string D_QUALIFIER { get; set; }

        public string D_TARGET { get; set; }

        public string D_CURRENTVALUE { get; set; }
    }
}
