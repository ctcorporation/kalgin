namespace KIF_Sat.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("PROFILE")]
    public partial class Profile
    {
        [Key]
        public Guid P_ID { get; set; }

        public Guid? P_C { get; set; }

        [StringLength(3)]
        public string P_REASONCODE { get; set; }

        public string P_SERVER { get; set; }

        public string P_USERNAME { get; set; }

        public string P_PASSWORD { get; set; }

        [StringLength(1)]
        public string P_DELIVERY { get; set; }

        [StringLength(10)]
        public string P_PORT { get; set; }

        public string P_DESCRIPTION { get; set; }

        [StringLength(100)]
        public string P_PATH { get; set; }

        [Required]
        [StringLength(1)]
        public string P_DIRECTION { get; set; }

        [StringLength(50)]
        public string P_LIBNAME { get; set; }

        [StringLength(3)]
        public string P_MESSAGETYPE { get; set; }

        [StringLength(15)]
        public string P_RECIPIENTID { get; set; }

        [StringLength(20)]
        public string P_MSGTYPE { get; set; }

        [StringLength(1)]
        public string P_CHARGEABLE { get; set; }

        [StringLength(15)]
        public string P_BILLTO { get; set; }

        [StringLength(15)]
        public string P_SENDERID { get; set; }

        [Required]
        [StringLength(1)]
        public string P_DTS { get; set; }

        [StringLength(100)]
        public string P_EMAILADDRESS { get; set; }

        [Required]
        [StringLength(1)]
        public string P_ACTIVE { get; set; }

        [StringLength(1)]
        public string P_SSL { get; set; }

        [StringLength(100)]
        public string P_SENDEREMAIL { get; set; }

        [StringLength(100)]
        public string P_SUBJECT { get; set; }

        [StringLength(5)]
        public string P_FILETYPE { get; set; }

        [StringLength(20)]
        public string P_MESSAGEDESCR { get; set; }

        [StringLength(3)]
        public string P_EVENTCODE { get; set; }

        [StringLength(50)]
        public string P_CUSTOMERCOMPANYNAME { get; set; }

        [StringLength(1)]
        public string P_GROUPCHARGES { get; set; }

        [StringLength(50)]
        public string P_XSD { get; set; }

        public string P_PARAMLIST { get; set; }

        [StringLength(50)]
        public string P_METHOD { get; set; }

        public Guid? P_PCTCNODE { get; set; }

        [MaxLength(1)]
        [Required]
        public string P_NOTIFY { get; set; }
    }
}
