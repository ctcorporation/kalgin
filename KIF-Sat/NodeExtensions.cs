﻿
using System.Linq;
using System.Text;

namespace KIF_Sat
{
    class NodeExtensions
    {

    }
    public static class StringEx
    {
        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }

            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }

        public static string RemoveChars(this string s, params char[] removeChars)
        {
            //Contract.Requires<ArgumentNullException>(s != null);
            //Contract.Requires<ArgumentNullException>(removeChars != null);
            var sb = new StringBuilder(s.Length);
            foreach (char c in s)
            
            {
                if (!removeChars.Contains(c))
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

    }
}
