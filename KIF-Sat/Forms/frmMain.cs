﻿using KIF_Sat.Classes;
using KIF_Sat.DTO;
using KIF_Sat.Forms;
using KIF_Sat.Models;
using NodeResources;
using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Timers;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using static KIF_Sat.SatelliteErrors;

namespace KIF_Sat
{

    public partial class frmMain : Form
    {
        // public SqlConnection sqlConn, sqlCTCConn;
        public int totfilesRx;
        public int totfilesTx;
        public int cwRx;
        public int cwTx;
        public int filesRx;
        public int filesTx;
        public IConnectionManager connMgr;
        public System.Timers.Timer tmrMain;
        public frmMain()
        {
            InitializeComponent();
            tmrMain = new System.Timers.Timer();

            AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnProcessExit);
        }

        private void OnProcessExit(object sender, EventArgs e)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            Heartbeat.RegisterHeartBeat(Globals.glCustCode, "Stopping", m.GetParameters(), Path.GetDirectoryName(Application.ExecutablePath));
        }

        private void tmrMain_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (btnTimer.Text == "&Stop")
            {
                tmrMain.Stop();
                CheckProfiles();
                GetFiles();
                tmrMain.Start();
            }
            else
            {
                tmrMain.Stop();
            }
        }

        private void CheckProfiles()
        {
            using (IUnitOfWork uow = new UnitOfWork(new KifEntity(Globals.ConnString.ConnString)))
            {
                var profList = (uow.Profiles.Find(x => x.P_ACTIVE == "Y")).ToList();
                if (profList.Count > 0)
                {
                    foreach (var prof in profList)
                    {
                        if (!string.IsNullOrEmpty(prof.P_METHOD))
                        {
                            Type custType = this.GetType();
                            MethodInfo custMethodToRun = custType.GetMethod(prof.P_METHOD);
                            var varResultInbound = custMethodToRun.Invoke(this, new object[] { prof });
                            if (varResultInbound != null)
                            {

                            }
                        }
                    }
                }
            }
        }

        public void UpdateAddresses(Profile prof)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            Heartbeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters(), Path.GetDirectoryName(Application.ExecutablePath));
            UpdateAddresses ua = new UpdateAddresses(prof);
            ua.SystemMail = Globals.glMailServerSettings;
            var addresses = ua.ScanMailBox();
            if (addresses.Count > 0)
            {

                foreach (var address in addresses)
                {
                    using (IUnitOfWork uow = new UnitOfWork(new KifEntity(Globals.connString())))
                    {
                        NodeResource.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Updating Organisations from email (" + address.CompanyName + ")", System.Drawing.Color.Green);
                        var ad = (uow.Organisations.Find(x => x.OR_MapValue.Trim().ToUpper() == address.MapValue.Trim().ToUpper() && x.OR_Type == address.CompanyType.Trim())).FirstOrDefault();
                        bool newAdd = false;
                        if (ad == null)
                        {
                            ad = new Organisation();
                            newAdd = true;
                        }
                        ad.OR_Name = string.IsNullOrEmpty(address.CompanyName) ? string.Empty : address.CompanyName.Trim().Truncate(100);
                        ad.OR_ShortCode = string.IsNullOrEmpty(address.ShortName) ? string.Empty : address.ShortName.Trim().Truncate(50);
                        ad.OR_Address1 = string.IsNullOrEmpty(address.Address) ? string.Empty : address.Address.Trim().Truncate(100);
                        ad.OR_CODE = string.IsNullOrEmpty(address.CWCode) ? string.Empty : address.CWCode.Trim().Truncate(20);
                        ad.OR_State = string.IsNullOrEmpty(address.State) ? string.Empty : address.State.Trim().Truncate(10);
                        ad.OR_Suburb = string.IsNullOrEmpty(address.City) ? string.Empty : address.City.Trim().Truncate(50);
                        ad.OR_PostCode = string.IsNullOrEmpty(address.PostCode) ? string.Empty : address.PostCode.Trim().Truncate(10);
                        ad.OR_Type = address.CompanyType.Truncate(50).Trim();
                        ad.OR_AirPortOfLoading = string.IsNullOrEmpty(address.POLA) ? string.Empty : address.POLA.Trim().Truncate(5);
                        ad.OR_SeaPortOfOrigin = string.IsNullOrEmpty(address.POOS) ? string.Empty : address.POOS.Trim().Truncate(5);
                        ad.OR_AirPortOfOrigin = string.IsNullOrEmpty(address.POOA) ? string.Empty : address.POOA.Trim().Truncate(5);
                        ad.OR_PortOfDischarge = string.IsNullOrEmpty(address.Discharge) ? string.Empty : address.Discharge.Trim().Truncate(5);
                        ad.OR_PortOfLoading = string.IsNullOrEmpty(address.POLS) ? string.Empty : address.POLS.Trim().Truncate(5);
                        ad.OR_MapValue = address.CompanyName.Trim().Truncate(200);
                        if (newAdd)
                        {
                            uow.Organisations.Add(ad);
                        }
                        uow.Complete();
                    }
                }
            }
            NodeResource.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Update Operation Complete. Resuming Timed Mode", System.Drawing.Color.Black);

        }

        private void btnTimer_Click(object sender, EventArgs e)
        {
            if (((Button)sender).Text == "&Start")
            {
                ((Button)sender).Text = "&Stop";
                ((Button)sender).BackColor = System.Drawing.Color.LightCoral;
                tslMain.Text = "Timed Processes Started";
               
                CheckProfiles();
                GetFiles();
                //  PrintJobs("");
                tmrMain.Start();

            }
            else
            {
                ((Button)sender).Text = "&Start";
                ((Button)sender).BackColor = System.Drawing.Color.LightGreen;
                tslMain.Text = "Timed Processes Stopped";
                tmrMain.Stop();
            }
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void clearLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbLog.Text = "";

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAbout AboutBox = new frmAbout();
            AboutBox.ShowDialog();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

            LoadSettings();
            productionToolStripMenuItem.Checked = true;
            Heartbeat.RegisterHeartBeat(Globals.glCustCode, "Starting", null, Path.GetDirectoryName(Application.ExecutablePath));
            this.tslSpacer.Padding = new Padding(this.Size.Width - 175, 0, 0, 0);
        }

        private void AddTransaction(Transaction trans)
        {
            using (KifEntity db = new KifEntity(connMgr.ConnString))
            {
                var transactions = (from t in db.Transactions
                                    where t.T_FILENAME == trans.T_FILENAME
                                    && t.T_REF1 == trans.T_REF1
                                    && t.T_ARCHIVE == trans.T_ARCHIVE
                                    select t).FirstOrDefault();
                if (transactions == null)
                {
                    db.Transactions.Add(trans);
                    db.SaveChanges();
                }

            }
        }
        private void LoadSettings()
        {
            var appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var path = Path.Combine(appDataPath, System.Diagnostics.Process.GetCurrentProcess().ProcessName);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);


            }
            tmrMain.Interval = (1000 * 5) * 60;

            tmrMain.Elapsed += new ElapsedEventHandler(tmrMain_Elapsed);
            Globals.glAppConfig = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".XML";
            Globals.glAppConfig = Path.Combine(path, Globals.glAppConfig);
            frmSettings Settings = new frmSettings();
            if (!File.Exists(Globals.glAppConfig))
            {
                XDocument xmlConfig = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "Yes"),
                    new XElement("Satellite",
                    new XElement("Customer"),
                    new XElement("Database"),
                    new XElement("CTCNode"),
                    new XElement("Communications")));
                xmlConfig.Save(Globals.glAppConfig);
                Settings.ShowDialog();
            }
            else
            {
                try
                {
                    bool loadConfig = false;
                    XmlDocument xmlConfig = new XmlDocument();
                    xmlConfig.Load(Globals.glAppConfig);
                    // Configuration Section DataBase
                    XmlNodeList nodeList = xmlConfig.SelectNodes("/Satellite/Database");
                    XmlNode nodeCat = nodeList[0].SelectSingleNode("Catalog");
                    if (nodeCat != null)
                    {
                        Globals.glDbInstance = nodeCat.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xmlServer = nodeList[0].SelectSingleNode("Servername");
                    if (xmlServer != null)
                    {
                        Globals.glDbServer = xmlServer.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xmlUser = nodeList[0].SelectSingleNode("Username");
                    if (xmlUser != null)
                    {
                        Globals.glDbUserName = xmlUser.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xmlPassword = nodeList[0].SelectSingleNode("Password");
                    if (xmlUser != null)
                    {
                        Globals.glDbPassword = xmlPassword.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    //sqlConn = new SqlConnection();
                    //sqlConn.ConnectionString = Globals.connString();

                    nodeList = null;
                    //Configuration Section Customer
                    nodeList = xmlConfig.SelectNodes("/Satellite/Customer");

                    XmlNode xCustCode = nodeList[0].SelectSingleNode("Code");
                    if (xCustCode != null)
                    {
                        Globals.glCustCode = xCustCode.InnerText;
                        Globals.Customer cust = new Globals.Customer();
                        cust = Globals.GetCustomer(Globals.glCustCode);
                        if (cust != null)
                        {
                            Globals.glCustCode = cust.Code;
                            Globals.gl_CustId = cust.Id;
                        }
                        else
                        {
                            loadConfig = true;
                        }
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xPath = nodeList[0].SelectSingleNode("ProfilePath");
                    if (xPath != null)
                    {
                        Globals.glProfilePath = xPath.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xOutPath = nodeList[0].SelectSingleNode("OutputPath");
                    if (xOutPath != null)
                    {
                        Globals.glOutputDir = xOutPath.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xInPath = nodeList[0].SelectSingleNode("PickupPath");
                    if (xInPath != null)
                    {
                        Globals.glPickupPath = xInPath.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xLib = nodeList[0].SelectSingleNode("LibraryPath");
                    if (xLib != null)
                    {
                        Globals.glLibPath = xLib.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xArc = nodeList[0].SelectSingleNode("ArchiveLocation");
                    if (xArc != null)
                    {
                        Globals.glArcLocation = xArc.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xFail = nodeList[0].SelectSingleNode("FailPath");
                    if (xFail != null)
                    {
                        Globals.glFailPath = xFail.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xTesting = nodeList[0].SelectSingleNode("TestingLocation");
                    if (xTesting != null)
                    {
                        Globals.glTestLocation = xTesting.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    if (loadConfig)
                    {
                        throw new System.Exception("Mandatory Settings missing");
                    }

                    // Configuration Section Communications
                    XDocument doc = XDocument.Load(Globals.glAppConfig);
                    nodeList = xmlConfig.SelectNodes("/Satellite/Communications");
                    var node = doc.Root.Element("Communications");
                    if (node == null)
                    {
                        MessageBox.Show("Error Loading Some of the System settings.", "System Settings missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        loadConfig = true;
                    }
                    Globals.glAlertsTo = node.Element("AlertsTo").Value;
                    Globals.glMailServerSettings = new MailServerSettings
                    {
                        Server = node.Element("SMTPServer").Value,
                        UserName = node.Element("SMTPUsername").Value,
                        Password = node.Element("SMTPPassword").Value,
                        //IsSecure = bool.Parse(node.Element("SMTPSsl").Value),
                        Port = int.Parse(node.Element("SMTPServer").Attribute("Port").Value),
                        Email = node.Element("EmailAddress").Value
                    };
                    XmlNode xmlAlertsTo = nodeList[0].SelectSingleNode("AlertsTo");
                    if (xmlAlertsTo != null)
                    {
                        Globals.glAlertsTo = xmlAlertsTo.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xSmtp = nodeList[0].SelectSingleNode("SMTPServer");
                    if (xSmtp != null)
                    {
                        SMTPServer.Server = xSmtp.InnerText;
                        SMTPServer.Port = xSmtp.Attributes[0].InnerXml;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xEmail = nodeList[0].SelectSingleNode("EmailAddress");
                    if (xEmail != null)
                    { SMTPServer.Email = xEmail.InnerText; }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xUserName = nodeList[0].SelectSingleNode("SMTPUsername");
                    if (xUserName != null)
                    {
                        SMTPServer.Username = xUserName.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    XmlNode xPassword = nodeList[0].SelectSingleNode("SMTPPassword");
                    if (xPassword != null)
                    {
                        SMTPServer.Password = xPassword.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }
                    //CTC Node Database Connection
                    nodeList = null;
                    nodeList = xmlConfig.SelectNodes("/Satellite/CTCNode");
                    nodeCat = null;
                    nodeCat = nodeList[0].SelectSingleNode("Catalog");
                    if (nodeCat != null)
                    {
                        Globals.glCTCDbInstance = nodeCat.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    xmlServer = null;
                    xmlServer = nodeList[0].SelectSingleNode("Servername");
                    if (xmlServer != null)
                    {
                        Globals.glCTCDbServer = xmlServer.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    xmlUser = null;
                    xmlUser = nodeList[0].SelectSingleNode("Username");
                    if (xmlUser != null)
                    {
                        Globals.glCTCDbUserName = xmlUser.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    xmlPassword = null;
                    xmlPassword = nodeList[0].SelectSingleNode("Password");
                    if (xmlUser != null)
                    {
                        Globals.glCTCDbPassword = xmlPassword.InnerText;
                    }
                    else
                    {
                        loadConfig = true;
                    }

                    // sqlCTCConn = new SqlConnection();
                    // sqlCTCConn.ConnectionString = Globals.CTCconnString();
                    Globals.ConnString = new ConnectionManager(Globals.glDbServer, Globals.glDbInstance, Globals.glDbUserName, Globals.glDbPassword);
                    Globals.CTCconnString = new ConnectionManager(Globals.glCTCDbServer, Globals.glCTCDbInstance, Globals.glCTCDbUserName, Globals.glCTCDbPassword);
                }

                catch (Exception)
                {
                    MessageBox.Show("There was an error loading the Config files. Please check the settings", "Loading Settings", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    frmSettings FormSettings = new frmSettings();
                    FormSettings.ShowDialog();
                    DialogResult dr = FormSettings.DialogResult;
                    if (dr == DialogResult.OK)
                    {
                        LoadSettings();
                    }
                }
                var Cust = Globals.GetCustomer(Globals.glCustCode);
                this.Text = "CTC Satellite - " + Cust.CustomerName;
                connMgr = new ConnectionManager(Globals.glDbServer, Globals.glDbInstance, Globals.glDbUserName, Globals.glDbPassword);
                NodeResource.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "System Ready");
            }
        }



        private void profilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmProfiles Profiles = new frmProfiles();
            Profiles.ShowDialog();

        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSettings FormSettings = new frmSettings();
            FormSettings.ShowDialog();
            DialogResult dr = FormSettings.DialogResult;
            if (dr == DialogResult.OK)
            {
                LoadSettings();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GetFiles();

        }

        private void GetFiles()
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            Heartbeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters(), Path.GetDirectoryName(Application.ExecutablePath));
            string filesPath = string.Empty;
            if (tslMode.Text == "Production")
            {
                filesPath = Globals.glPickupPath;
            }
            else
            {
                filesPath = Globals.glTestLocation;
            }
            DirectoryInfo diTodo = new DirectoryInfo(filesPath);
            int iCount = 0;
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var list = diTodo.GetFiles();

            NodeResource.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Now Processing " + list.Length + " Files");
            foreach (var fileName in diTodo.GetFiles())
            {
                iCount++;
                ProcessResult thisResult = new ProcessResult();

                thisResult = ProcessCustomFiles(fileName.FullName);
                //return;
                if (thisResult.Processed)
                {
                    try
                    {
                        if (File.Exists(fileName.FullName))
                        {
                            File.Delete(fileName.FullName);
                        }
                    }
                    catch (Exception ex)
                    {
                        NodeResource.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Unable to Delete File. Error :" + ex.Message);
                    }

                }

            }
            stopwatch.Stop();
            double t = stopwatch.Elapsed.TotalSeconds;
            if (iCount > 0)
            {
                NodeResource.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Processing Complete. " + iCount + " files processed in " + t + " seconds");
            }
            NodeResource.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Waiting for Files");
        }



        private ProcessResult ProcessCustomFiles(String xmlFile)
        {
            MethodBase m = MethodBase.GetCurrentMethod();

            Heartbeat.RegisterHeartBeat(Globals.glCustCode, m.Name, m.GetParameters(), Path.GetDirectoryName(Application.ExecutablePath));

            ProcessResult thisResult = new ProcessResult();
            thisResult.Processed = true;

            NodeResource.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " File " + Path.GetFileName(xmlFile) + " Is not a Cargowise File. Checking for Against Customer.");
            FileInfo processingFile = new FileInfo(xmlFile);
            filesRx++;
            NodeResource.AddLabel(lblFilesRx, filesRx.ToString());
            totfilesRx++;
            NodeResource.AddLabel(lblTotFilesRx, totfilesRx.ToString());
            TransReference trRef = new TransReference();
            String archiveFile;
            string recipientID = "";
            string senderID = "";
            switch (processingFile.Extension.ToUpper())
            {
                case ".XML":
                    NodeResource.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " New XML file found " + Path.GetFileName(processingFile.FullName) + ". Processing" + "");
                    Boolean processed = false;
                    using (IUnitOfWork uow = new UnitOfWork(new KifEntity(Globals.ConnString.ConnString)))
                    {
                        var profileList = uow.CustomerProfiles.Find(x => x.P_XSD != null && x.P_ACTIVE == "Y").ToList();
                        if (profileList.Count > 0)
                        {
                            while (!processed)
                            {
                                //   FileStream fsFile = new FileStream(xmlFile, FileMode.Open, FileAccess.ReadWrite);
                                //   StreamReader sr = new StreamReader(fsFile);
                                foreach (var profile in profileList)
                                {
                                    string schemaFile = Path.Combine(Globals.glProfilePath, "Lib", profile.P_XSD.Trim());
                                    if (IdentifyXML(schemaFile, xmlFile))
                                    {
                                        recipientID = profile.P_RECIPIENTID;
                                        senderID = profile.P_SENDERID;
                                        archiveFile = Globals.ArchiveFile(profile.C_PATH + "\\Archive", xmlFile);
                                        processed = true;
                                        thisResult.Processed = true;
                                    }
                                }
                                if (!processed)
                                {
                                    ProcessingErrors procerror = new ProcessingErrors();
                                    CTCErrorCode error = new CTCErrorCode();
                                    error.Code = NodeError.e111;
                                    error.Description = "Unable to process " + Path.GetFileName(xmlFile) + "No matching routines found. Moving to Failed.";
                                    error.Severity = "Warning";
                                    procerror.ErrorCode = error;
                                    procerror.SenderId = senderID;
                                    procerror.RecipientId = recipientID;
                                    procerror.FileName = xmlFile;
                                    NodeResource.AddProcessingError(procerror);
                                    thisResult.FolderLoc = Globals.glFailPath;
                                    thisResult.Processed = false;
                                    string f = NodeResource.MoveFile(xmlFile, Globals.glFailPath);
                                    if (f.StartsWith("Warning"))
                                    {
                                        NodeResource.AddText(rtbLog, string.Format("{0:g} ", DateTime.Now) + f, System.Drawing.Color.Red);
                                    }
                                    else
                                    { xmlFile = f; }

                                    NodeResource.AddProcessingError(procerror);
                                    thisResult.Processed = false;
                                    thisResult.FolderLoc = Globals.glFailPath;
                                    NodeResource.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + " Unable to process " + Path.GetFileName(xmlFile) + "No matching routines found. Moving to Failed." + "", System.Drawing.Color.Red);
                                    processed = true;
                                    //  return thisResult;
                                }


                            }
                        }
                    }
                    break;
                case ".XLSX":
                    string archiveName = Globals.ArchiveFile(Globals.glArcLocation, xmlFile);
                    ADMImport admImport = new ADMImport(xmlFile, Globals.ConnString.ConnString, "ADMAUS_AU", "KIFSYDSYD");
                    if (admImport.CreateOrder())
                    {
                        NodeResource.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "Import Results : " + Environment.NewLine + admImport.Summary, System.Drawing.Color.Blue);
                        if (admImport.ErrorString != null)
                        {
                            NodeResource.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "Errors were found in Data file. Error Summary Sent.", System.Drawing.Color.Red);

                        }
                        try
                        {

                            File.Delete(xmlFile);
                        }
                        catch (Exception ex)
                        {
                            string strEx = ex.GetType().Name;
                            NodeResource.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "Exception:" + strEx + ". Unable to Delete " + Path.GetFileName(xmlFile) + ":" + ex.Message, System.Drawing.Color.Red);

                        }
                    }
                    else
                    {
                        NodeResource.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "Import Results : " + Environment.NewLine + admImport.Summary, System.Drawing.Color.Red);
                        // Errors were found and the process failed. 
                        NodeResource.AddRTBText(rtbLog, string.Format("{0:g} ", DateTime.Now) + "Errors were found in Data file. Error Summary Sent.", System.Drawing.Color.Red);
                    }
                    break;
            }
            return thisResult;
        }

        private bool IdentifyXML(string schemaFile, string xmlFile)
        {
            try
            {
                XmlTextReader sXsd = new XmlTextReader(schemaFile);
                XmlSchema schema = new XmlSchema();
                string _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
                schema = XmlSchema.Read(sXsd, CustomValidateHandler.HandlerErrors);
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.ValidationType = ValidationType.Schema;
                settings.Schemas.Add(schema);
                settings.ValidationEventHandler += (o, ea) =>
                {
                    throw new XmlSchemaValidationException(
                        string.Format("Schema Not Found: {0}",
                                      ea.Message),
                        ea.Exception);
                };
                using (var stream = new FileStream(xmlFile, FileMode.Open, FileAccess.Read, FileShare.Read))
                using (var cusXML = XmlReader.Create(stream, settings))
                {
                    while (cusXML.Read())
                    {

                    }
                    stream.Close();
                    return true;
                }
            }
            catch (XmlSchemaValidationException)
            {
                return false;
            }
            catch (XmlException)
            {

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }



        private void GetProcErrors()
        {
            using (IUnitOfWork uow = new UnitOfWork(new KifEntity(Globals.ConnString.ConnString)))
            {
                var procErrorList = uow.ProcessingErrors.GetAll().OrderByDescending(x => x.E_PROCDATE).ToList();
                if (procErrorList.Count > 0)
                {
                    dgProcessingErrors.AutoGenerateColumns = false;
                    dgProcessingErrors.DataSource = procErrorList;
                }
            }

        }

        private string FixNumber(string bl)
        {
            string result;
            if (bl.Contains("/"))
            {
                result = bl.Replace("/", "-");
            }
            else
            {
                result = bl;
            }

            return result;
        }

        private void ProcessCargowiseFiles()
        {

        }
        private void ProcessCustomXML(string XMLFile)
        {


        }

        private void customMappingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEnums Enums = new frmEnums();
            Enums.Show();

        }

        private void bbClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void organisationLookupsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmOrgLookup OrgLookup = new frmOrgLookup();
            OrgLookup.Show();
        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            this.tslSpacer.Padding = new Padding(this.Size.Width - 175, 0, 0, 0);
        }

        private void productionToolStripMenu_Click(object sender, EventArgs e)
        {
            if (productionToolStripMenuItem.Checked)
            {
                testingToolStripMenuItem.Checked = false;
                tslMode.Text = "Production";
            }
            else
            {
                testingToolStripMenuItem.Checked = true;
                tslMode.Text = "Testing";
            }
        }

        private void testingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (testingToolStripMenuItem.Checked)
            {
                productionToolStripMenuItem.Checked = false;
                tslMode.Text = "Testing";
            }
            else
            {
                productionToolStripMenuItem.Checked = true;
                tslMode.Text = "Production";
            }
        }

        private void reSendOrderFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmResend resend = new FrmResend();
            resend.Show();

        }

        private void fileMappingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmFileMapping fileMap = new frmFileMapping();
            fileMap.Show();
        }

    }
}

