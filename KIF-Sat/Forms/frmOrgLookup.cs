﻿using KIF_Sat.DTO;
using KIF_Sat.Models;
using KIF_Sat.Resource;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace KIF_Sat
{

    public partial class frmOrgLookup : Form
    {
        private Guid Oid;
        private Organisation comp;
        private BindingSource bsOrgList = new BindingSource();
        public frmOrgLookup()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (IUnitOfWork uow = new UnitOfWork(new KifEntity(Globals.ConnString.ConnString)))
            {
                var compToAdd = uow.Organisations.Get(Oid);

                if (compToAdd == null)
                {
                    compToAdd = new Organisation();
                    compToAdd.OR_ID = Guid.NewGuid();
                }
                compToAdd.OR_Address1 = edAddress1.Text;
                compToAdd.OR_Address2 = edAddress2.Text;
                compToAdd.OR_Address3 = edCountryCode.Text;
                compToAdd.OR_PostCode = edPostCode.Text;
                compToAdd.OR_Name = edCompanyName.Text;
                compToAdd.OR_MapValue = edMapValue.Text;
                compToAdd.OR_CODE = edCargowiseCode.Text;
                compToAdd.OR_AirPortOfLoading = edAirPOL.Text;
                compToAdd.OR_AirPortOfOrigin = edAirPOO.Text;
                compToAdd.OR_PortOfLoading = edSeaPol.Text;
                compToAdd.OR_SeaPortOfOrigin = edSeaPOO.Text;
                compToAdd.OR_PortOfDischarge = edPod.Text;
                switch (cmbOrgType.Text)
                {
                    case "Consignee":
                        compToAdd.OR_Type = "Consignee";
                        break;
                    case "Deliver To":
                        compToAdd.OR_Type = "DeliverTo";
                        break;
                    case "Forwarding Agent":
                        compToAdd.OR_Type = "ForwardingAgent";
                        break;
                    case "Supplier":
                        compToAdd.OR_Type = "Supplier";
                        break;
                }
                compToAdd.OR_ShortCode = edShortName.Text;
                if (Oid == Guid.Empty)
                {

                    uow.Organisations.Add(comp);

                }
                uow.Complete();
            }
            LoadData();
        }

        private void frmOrgLookup_Load(object sender, EventArgs e)
        {
            LoadData();
            Oid = Guid.Empty;
        }

        private void LoadData()
        {
            using (IUnitOfWork uow = new UnitOfWork(new KifEntity(Globals.ConnString.ConnString)))
            {
                var compList = uow.Organisations.GetAll().OrderBy(x => x.OR_MapValue).ToList();
                if (compList.Count > 0)
                {
                    bsOrgList.DataSource = new SortableBindingList<Organisation>(compList);
                    dgAddress.DataSource = bsOrgList;
                }
            }

        }

        private void dgAddress_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > 0)
            {
                Guid id = Guid.Empty;
                if (Guid.TryParse(dgAddress[0, e.RowIndex].Value.ToString(), out id))
                {
                    using (IUnitOfWork uow = new UnitOfWork(new KifEntity(Globals.ConnString.ConnString)))
                    {
                        comp = uow.Organisations.Get(id);
                        edAddress1.Text = comp.OR_Address1;
                        edAddress2.Text = comp.OR_Address2;
                        edAirPOL.Text = comp.OR_AirPortOfLoading;
                        edSeaPol.Text = comp.OR_PortOfLoading;
                        edAirPOO.Text = comp.OR_AirPortOfOrigin;
                        edSeaPOO.Text = comp.OR_SeaPortOfOrigin;
                        edCargowiseCode.Text = comp.OR_CODE;
                        edCity.Text = comp.OR_Suburb;
                        edCompanyName.Text = comp.OR_Name;
                        edCountryCode.Text = comp.OR_Address3;
                        edMapValue.Text = comp.OR_MapValue;
                        switch (comp.OR_Type)
                        {
                            case "Consignee":
                                cmbOrgType.Text = "Consignee";
                                break;
                            case "DeliverTo":
                                cmbOrgType.Text = "Deliver To";
                                break;
                            case "ForwardingAgent":
                                cmbOrgType.Text = "Forwarding Agent";
                                break;
                            case "Supplier":
                                cmbOrgType.Text = "Supplier";
                                break;
                        }

                        edPod.Text = comp.OR_PortOfDischarge;
                        edPostCode.Text = comp.OR_PostCode;
                        edShortName.Text = comp.OR_ShortCode;
                        edState.Text = comp.OR_State;
                        Oid = comp.OR_ID;

                    }
                }
            }
        }

        private void cmnAddress_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int rowIndex = dgAddress.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            if (rowIndex != null)
            {
                Guid id = Guid.Empty;
                if (Guid.TryParse(dgAddress[3, rowIndex].Value.ToString(), out id))
                {
                    using (IUnitOfWork uow = new UnitOfWork(new KifEntity(Globals.ConnString.ConnString)))
                    {
                        var orgToDelete = uow.Organisations.Get(id);
                        if (orgToDelete != null)
                        {
                            uow.Organisations.Remove(orgToDelete);
                            uow.Complete();
                        }
                    }
                }
                LoadData();
            }
        }

        private void dgAddress_CellContextMenuStripNeeded(object sender, DataGridViewCellContextMenuStripNeededEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            if (e.RowIndex == -1 || e.ColumnIndex == -1)
            {
                return;
            }
        }

        private void dgAddress_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex != -1 && e.RowIndex != -1 && e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                DataGridViewCell c = (sender as DataGridView)[e.ColumnIndex, e.RowIndex];
                if (!c.Selected)
                {
                    c.DataGridView.ClearSelection();
                    c.DataGridView.CurrentCell = c;
                    c.Selected = true;
                }
            }
        }

        private void bbNew_Click(object sender, EventArgs e)
        {
            ResetControls(this, ctrl => ctrl.Text = "");
        }

        private void ResetControls(Control control, Action<Control> action)
        {
            foreach (Control child in control.Controls)
            {

                ResetControls(child, action);
                if (child is TextBox)
                {
                    action(child);
                }
            }
            edAirPOL.Text = "USORD";
            edAirPOO.Text = "USORD";
            edSeaPol.Text = "USLGB";
            edSeaPOO.Text = "USCHI";
            cmbOrgType.SelectedIndex = 0;
            edMapValue.Focus();
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(edFind.Text))
            {
                try
                {
                    string searchValue = edFind.Text.ToUpper();
                    DataGridViewRow row = dgAddress.Rows
                        .Cast<DataGridViewRow>()
                        .Where(r => r.Cells[2].Value.ToString().ToUpper().Trim().Contains(searchValue))
                        //|| r.Cells[2].Value.ToString().Trim().Contains(searchValue)
                        //|| r.Cells[3].Value.ToString().Trim().Contains(searchValue))
                        .FirstOrDefault();
                    if (row != null)
                    {
                        dgAddress.ClearSelection();
                        dgAddress.Rows[row.Index].Selected = true;
                        dgAddress.FirstDisplayedScrollingRowIndex = row.Index;
                    }
                    else
                    {
                        MessageBox.Show("Item not found in Mappings");
                    }
                }
                catch (NullReferenceException)
                {

                }

            }
        }
    }




}
