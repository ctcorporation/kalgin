﻿namespace KIF_Sat
{
    partial class frmProfiles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnClose = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.dgProfile = new System.Windows.Forms.DataGridView();
            this.P_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Active = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.P_DESCRIPTION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.P_SENDERID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.P_RECIPIENTID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.P_REASONCODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Direction = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tcMessageProfile = new System.Windows.Forms.TabControl();
            this.tbMessageHeader = new System.Windows.Forms.TabPage();
            this.cbGroupCharges = new System.Windows.Forms.CheckBox();
            this.cbProfileActive = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.edEventCode = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.cbDTS = new System.Windows.Forms.CheckBox();
            this.edReasonCode = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.edSenderID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.edRecipientID = new System.Windows.Forms.TextBox();
            this.gbDirection = new System.Windows.Forms.GroupBox();
            this.rbReceive = new System.Windows.Forms.RadioButton();
            this.rbSend = new System.Windows.Forms.RadioButton();
            this.edDescription = new System.Windows.Forms.TextBox();
            this.cbChargeable = new System.Windows.Forms.CheckBox();
            this.grpBillto = new System.Windows.Forms.GroupBox();
            this.rbVendor = new System.Windows.Forms.RadioButton();
            this.rbCustomer = new System.Windows.Forms.RadioButton();
            this.cmbMsgType = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbRecieveProperties = new System.Windows.Forms.TabPage();
            this.gbReceiveOptions = new System.Windows.Forms.GroupBox();
            this.edReceiveCustomerName = new System.Windows.Forms.TextBox();
            this.lblReceiveCustomerName = new System.Windows.Forms.Label();
            this.btnReceiveFolder = new System.Windows.Forms.Button();
            this.txtTestResults = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.bbTestRx = new System.Windows.Forms.Button();
            this.edReceiveSubject = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.edReceiveSender = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cbSSL = new System.Windows.Forms.CheckBox();
            this.cmbFileType = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.edReceiveEmailAddress = new System.Windows.Forms.TextBox();
            this.lblReceiveEmailAddress = new System.Windows.Forms.Label();
            this.gbReceiveMethod = new System.Windows.Forms.GroupBox();
            this.rbReceivePickup = new System.Windows.Forms.RadioButton();
            this.rbEmail = new System.Windows.Forms.RadioButton();
            this.rbReceiveFTP = new System.Windows.Forms.RadioButton();
            this.rbPickup = new System.Windows.Forms.RadioButton();
            this.edFTPReceivePath = new System.Windows.Forms.TextBox();
            this.lblFTPReceivePath = new System.Windows.Forms.Label();
            this.edFTPReceivePort = new System.Windows.Forms.TextBox();
            this.lblFTPReceivePort = new System.Windows.Forms.Label();
            this.edReceivePassword = new System.Windows.Forms.TextBox();
            this.lblReceivePassword = new System.Windows.Forms.Label();
            this.edReceiveUsername = new System.Windows.Forms.TextBox();
            this.lblReceiveUsername = new System.Windows.Forms.Label();
            this.edServerAddress = new System.Windows.Forms.TextBox();
            this.lblServerAddress = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbSendProperties = new System.Windows.Forms.TabPage();
            this.gbSendOptions = new System.Windows.Forms.GroupBox();
            this.btnPickupFolder = new System.Windows.Forms.Button();
            this.edSendSubject = new System.Windows.Forms.TextBox();
            this.lblSendSubject = new System.Windows.Forms.Label();
            this.edSendEmailAddress = new System.Windows.Forms.TextBox();
            this.lblSendEmailAddress = new System.Windows.Forms.Label();
            this.edSendFtpPort = new System.Windows.Forms.TextBox();
            this.lblSendFTPPort = new System.Windows.Forms.Label();
            this.btnTestSendeAdapter = new System.Windows.Forms.Button();
            this.edSendeAdapterResults = new System.Windows.Forms.TextBox();
            this.lblTestResults = new System.Windows.Forms.Label();
            this.edSendFolder = new System.Windows.Forms.TextBox();
            this.gbSendMethod = new System.Windows.Forms.GroupBox();
            this.rbSoapSend = new System.Windows.Forms.RadioButton();
            this.rbFTPSendDirect = new System.Windows.Forms.RadioButton();
            this.rbEmailSend = new System.Windows.Forms.RadioButton();
            this.rbFTPSend = new System.Windows.Forms.RadioButton();
            this.rbCTCSend = new System.Windows.Forms.RadioButton();
            this.rbPickupSend = new System.Windows.Forms.RadioButton();
            this.lblSendPath = new System.Windows.Forms.Label();
            this.edSendPassword = new System.Windows.Forms.TextBox();
            this.lblSendPassword = new System.Windows.Forms.Label();
            this.edSendUsername = new System.Windows.Forms.TextBox();
            this.lblSendUsername = new System.Windows.Forms.Label();
            this.edSendCTCUrl = new System.Windows.Forms.TextBox();
            this.lblSendServer = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tbConversion = new System.Windows.Forms.TabPage();
            this.edCustomMethod = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.btnAddParam = new System.Windows.Forms.Button();
            this.edParameter = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.lbParameters = new System.Windows.Forms.ListBox();
            this.label34 = new System.Windows.Forms.Label();
            this.tvwXSD = new System.Windows.Forms.TreeView();
            this.btnLoadXSD = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.ddlCWContext = new System.Windows.Forms.ComboBox();
            this.btnXsdPath = new System.Windows.Forms.Button();
            this.edLibraryName = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.edXSDPath = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.tbDTS = new System.Windows.Forms.TabPage();
            this.gbDTS = new System.Windows.Forms.GroupBox();
            this.edDTSCurrentValue = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.edDTSValue = new System.Windows.Forms.TextBox();
            this.edDTSTarget = new System.Windows.Forms.TextBox();
            this.edDTSSearch = new System.Windows.Forms.TextBox();
            this.edDTSQual = new System.Windows.Forms.TextBox();
            this.bbAddDTS = new System.Windows.Forms.Button();
            this.dgDts = new System.Windows.Forms.DataGridView();
            this.DTSIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTSType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTSSearch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTSModifies = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTSFileType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTSQual = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTSTarget = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTSNewValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrentValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbModifies = new System.Windows.Forms.GroupBox();
            this.rbDTSStructure = new System.Windows.Forms.RadioButton();
            this.rbDTSRecord = new System.Windows.Forms.RadioButton();
            this.ddlDTSFileType = new System.Windows.Forms.ComboBox();
            this.ddlDTSType = new System.Windows.Forms.ComboBox();
            this.edDTSIndex = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.btnNewProfile = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmProfiles = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.duplicateProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.markInactiveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fdProfile = new System.Windows.Forms.FolderBrowserDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dgProfile)).BeginInit();
            this.tcMessageProfile.SuspendLayout();
            this.tbMessageHeader.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbDirection.SuspendLayout();
            this.grpBillto.SuspendLayout();
            this.tbRecieveProperties.SuspendLayout();
            this.gbReceiveOptions.SuspendLayout();
            this.gbReceiveMethod.SuspendLayout();
            this.tbSendProperties.SuspendLayout();
            this.gbSendOptions.SuspendLayout();
            this.gbSendMethod.SuspendLayout();
            this.tbConversion.SuspendLayout();
            this.tbDTS.SuspendLayout();
            this.gbDTS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDts)).BeginInit();
            this.gbModifies.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.cmProfiles.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(1163, 464);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(11, 9);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(214, 13);
            this.label4.TabIndex = 37;
            this.label4.Text = "Customer Send and Receive Profiles";
            // 
            // dgProfile
            // 
            this.dgProfile.AllowUserToAddRows = false;
            this.dgProfile.AllowUserToResizeColumns = false;
            this.dgProfile.AllowUserToResizeRows = false;
            this.dgProfile.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgProfile.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.P_ID,
            this.Active,
            this.P_DESCRIPTION,
            this.P_SENDERID,
            this.P_RECIPIENTID,
            this.P_REASONCODE,
            this.Direction});
            this.dgProfile.DataMember = "CUSTOMERPROFILE";
            this.dgProfile.Location = new System.Drawing.Point(664, 9);
            this.dgProfile.Margin = new System.Windows.Forms.Padding(2);
            this.dgProfile.MultiSelect = false;
            this.dgProfile.Name = "dgProfile";
            this.dgProfile.ReadOnly = true;
            this.dgProfile.RowHeadersVisible = false;
            this.dgProfile.RowTemplate.Height = 28;
            this.dgProfile.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgProfile.Size = new System.Drawing.Size(577, 391);
            this.dgProfile.TabIndex = 38;
            this.dgProfile.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler(this.dgProfile_CellContextMenuStripNeeded);
            this.dgProfile.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgProfile_CellDoubleClick);
            this.dgProfile.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgProfile_CellFormatting);
            this.dgProfile.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgProfile_CellMouseDown);
            this.dgProfile.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgProfile_UserDeletedRow);
            // 
            // P_ID
            // 
            this.P_ID.DataPropertyName = "P_ID";
            this.P_ID.HeaderText = "P_ID";
            this.P_ID.Name = "P_ID";
            this.P_ID.ReadOnly = true;
            this.P_ID.Visible = false;
            // 
            // Active
            // 
            this.Active.DataPropertyName = "P_ACTIVE";
            this.Active.FalseValue = "N";
            this.Active.HeaderText = "Active";
            this.Active.Name = "Active";
            this.Active.ReadOnly = true;
            this.Active.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Active.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Active.TrueValue = "Y";
            this.Active.Width = 40;
            // 
            // P_DESCRIPTION
            // 
            this.P_DESCRIPTION.DataPropertyName = "P_DESCRIPTION";
            this.P_DESCRIPTION.HeaderText = "Description";
            this.P_DESCRIPTION.Name = "P_DESCRIPTION";
            this.P_DESCRIPTION.ReadOnly = true;
            this.P_DESCRIPTION.Width = 250;
            // 
            // P_SENDERID
            // 
            this.P_SENDERID.DataPropertyName = "P_SENDERID";
            this.P_SENDERID.HeaderText = "Sender ID";
            this.P_SENDERID.Name = "P_SENDERID";
            this.P_SENDERID.ReadOnly = true;
            // 
            // P_RECIPIENTID
            // 
            this.P_RECIPIENTID.DataPropertyName = "P_RECIPIENTID";
            this.P_RECIPIENTID.HeaderText = "Recipient ID";
            this.P_RECIPIENTID.Name = "P_RECIPIENTID";
            this.P_RECIPIENTID.ReadOnly = true;
            this.P_RECIPIENTID.Width = 200;
            // 
            // P_REASONCODE
            // 
            this.P_REASONCODE.DataPropertyName = "P_REASONCODE";
            this.P_REASONCODE.HeaderText = "Code";
            this.P_REASONCODE.Name = "P_REASONCODE";
            this.P_REASONCODE.ReadOnly = true;
            // 
            // Direction
            // 
            this.Direction.DataPropertyName = "P_DIRECTION";
            this.Direction.HeaderText = "Direction";
            this.Direction.Name = "Direction";
            this.Direction.ReadOnly = true;
            this.Direction.Visible = false;
            // 
            // tcMessageProfile
            // 
            this.tcMessageProfile.Controls.Add(this.tbMessageHeader);
            this.tcMessageProfile.Controls.Add(this.tbRecieveProperties);
            this.tcMessageProfile.Controls.Add(this.tbSendProperties);
            this.tcMessageProfile.Controls.Add(this.tbConversion);
            this.tcMessageProfile.Controls.Add(this.tbDTS);
            this.tcMessageProfile.Location = new System.Drawing.Point(14, 25);
            this.tcMessageProfile.Name = "tcMessageProfile";
            this.tcMessageProfile.SelectedIndex = 0;
            this.tcMessageProfile.Size = new System.Drawing.Size(645, 354);
            this.tcMessageProfile.TabIndex = 39;
            // 
            // tbMessageHeader
            // 
            this.tbMessageHeader.Controls.Add(this.cbGroupCharges);
            this.tbMessageHeader.Controls.Add(this.cbProfileActive);
            this.tbMessageHeader.Controls.Add(this.groupBox1);
            this.tbMessageHeader.Controls.Add(this.gbDirection);
            this.tbMessageHeader.Controls.Add(this.edDescription);
            this.tbMessageHeader.Controls.Add(this.cbChargeable);
            this.tbMessageHeader.Controls.Add(this.grpBillto);
            this.tbMessageHeader.Controls.Add(this.cmbMsgType);
            this.tbMessageHeader.Controls.Add(this.label9);
            this.tbMessageHeader.Controls.Add(this.label6);
            this.tbMessageHeader.Location = new System.Drawing.Point(4, 22);
            this.tbMessageHeader.Name = "tbMessageHeader";
            this.tbMessageHeader.Padding = new System.Windows.Forms.Padding(3);
            this.tbMessageHeader.Size = new System.Drawing.Size(637, 328);
            this.tbMessageHeader.TabIndex = 0;
            this.tbMessageHeader.Text = "Profile Properties";
            this.tbMessageHeader.UseVisualStyleBackColor = true;
            // 
            // cbGroupCharges
            // 
            this.cbGroupCharges.AutoSize = true;
            this.cbGroupCharges.Location = new System.Drawing.Point(443, 149);
            this.cbGroupCharges.Name = "cbGroupCharges";
            this.cbGroupCharges.Size = new System.Drawing.Size(97, 17);
            this.cbGroupCharges.TabIndex = 31;
            this.cbGroupCharges.Text = "Group Charges";
            this.cbGroupCharges.UseVisualStyleBackColor = true;
            // 
            // cbProfileActive
            // 
            this.cbProfileActive.AutoSize = true;
            this.cbProfileActive.Location = new System.Drawing.Point(369, 149);
            this.cbProfileActive.Name = "cbProfileActive";
            this.cbProfileActive.Size = new System.Drawing.Size(67, 17);
            this.cbProfileActive.TabIndex = 30;
            this.cbProfileActive.Text = "Is Active";
            this.cbProfileActive.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.edEventCode);
            this.groupBox1.Controls.Add(this.label32);
            this.groupBox1.Controls.Add(this.cbDTS);
            this.groupBox1.Controls.Add(this.edReasonCode);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.edSenderID);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.edRecipientID);
            this.groupBox1.Location = new System.Drawing.Point(18, 54);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(421, 85);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cargowise XML Details";
            // 
            // edEventCode
            // 
            this.edEventCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edEventCode.Location = new System.Drawing.Point(247, 52);
            this.edEventCode.Margin = new System.Windows.Forms.Padding(2);
            this.edEventCode.Name = "edEventCode";
            this.edEventCode.Size = new System.Drawing.Size(40, 20);
            this.edEventCode.TabIndex = 34;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(180, 55);
            this.label32.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(63, 13);
            this.label32.TabIndex = 35;
            this.label32.Text = "Event Code";
            // 
            // cbDTS
            // 
            this.cbDTS.AutoSize = true;
            this.cbDTS.Location = new System.Drawing.Point(321, 25);
            this.cbDTS.Name = "cbDTS";
            this.cbDTS.Size = new System.Drawing.Size(94, 17);
            this.cbDTS.TabIndex = 33;
            this.cbDTS.Text = "DTS Required";
            this.cbDTS.UseVisualStyleBackColor = true;
            // 
            // edReasonCode
            // 
            this.edReasonCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edReasonCode.Location = new System.Drawing.Point(132, 51);
            this.edReasonCode.Margin = new System.Windows.Forms.Padding(2);
            this.edReasonCode.Name = "edReasonCode";
            this.edReasonCode.Size = new System.Drawing.Size(40, 20);
            this.edReasonCode.TabIndex = 31;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 55);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 13);
            this.label7.TabIndex = 32;
            this.label7.Text = "Message Purpose Code";
            // 
            // edSenderID
            // 
            this.edSenderID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edSenderID.Location = new System.Drawing.Point(73, 23);
            this.edSenderID.Margin = new System.Windows.Forms.Padding(2);
            this.edSenderID.Name = "edSenderID";
            this.edSenderID.Size = new System.Drawing.Size(79, 20);
            this.edSenderID.TabIndex = 27;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(156, 26);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 28;
            this.label5.Text = "Recipient ID ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 26);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 30;
            this.label10.Text = "Sender ID";
            // 
            // edRecipientID
            // 
            this.edRecipientID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edRecipientID.Location = new System.Drawing.Point(229, 23);
            this.edRecipientID.Margin = new System.Windows.Forms.Padding(2);
            this.edRecipientID.Name = "edRecipientID";
            this.edRecipientID.Size = new System.Drawing.Size(79, 20);
            this.edRecipientID.TabIndex = 29;
            // 
            // gbDirection
            // 
            this.gbDirection.Controls.Add(this.rbReceive);
            this.gbDirection.Controls.Add(this.rbSend);
            this.gbDirection.Location = new System.Drawing.Point(547, 54);
            this.gbDirection.Name = "gbDirection";
            this.gbDirection.Size = new System.Drawing.Size(80, 85);
            this.gbDirection.TabIndex = 27;
            this.gbDirection.TabStop = false;
            this.gbDirection.Text = "Direction";
            // 
            // rbReceive
            // 
            this.rbReceive.AutoSize = true;
            this.rbReceive.Location = new System.Drawing.Point(6, 19);
            this.rbReceive.Name = "rbReceive";
            this.rbReceive.Size = new System.Drawing.Size(65, 17);
            this.rbReceive.TabIndex = 4;
            this.rbReceive.TabStop = true;
            this.rbReceive.Text = "Receive";
            this.rbReceive.UseVisualStyleBackColor = true;
            // 
            // rbSend
            // 
            this.rbSend.AutoSize = true;
            this.rbSend.Location = new System.Drawing.Point(6, 42);
            this.rbSend.Name = "rbSend";
            this.rbSend.Size = new System.Drawing.Size(50, 17);
            this.rbSend.TabIndex = 5;
            this.rbSend.TabStop = true;
            this.rbSend.Text = "Send";
            this.rbSend.UseVisualStyleBackColor = true;
            this.rbSend.Click += new System.EventHandler(this.DeliveryChecked);
            // 
            // edDescription
            // 
            this.edDescription.Location = new System.Drawing.Point(18, 26);
            this.edDescription.Margin = new System.Windows.Forms.Padding(2);
            this.edDescription.Name = "edDescription";
            this.edDescription.Size = new System.Drawing.Size(421, 20);
            this.edDescription.TabIndex = 0;
            // 
            // cbChargeable
            // 
            this.cbChargeable.AutoSize = true;
            this.cbChargeable.Location = new System.Drawing.Point(237, 149);
            this.cbChargeable.Name = "cbChargeable";
            this.cbChargeable.Size = new System.Drawing.Size(126, 17);
            this.cbChargeable.TabIndex = 7;
            this.cbChargeable.Text = "Chargeable Message";
            this.cbChargeable.UseVisualStyleBackColor = true;
            // 
            // grpBillto
            // 
            this.grpBillto.Controls.Add(this.rbVendor);
            this.grpBillto.Controls.Add(this.rbCustomer);
            this.grpBillto.Location = new System.Drawing.Point(445, 54);
            this.grpBillto.Name = "grpBillto";
            this.grpBillto.Size = new System.Drawing.Size(96, 85);
            this.grpBillto.TabIndex = 8;
            this.grpBillto.TabStop = false;
            this.grpBillto.Text = "Bill To:";
            // 
            // rbVendor
            // 
            this.rbVendor.AutoSize = true;
            this.rbVendor.Location = new System.Drawing.Point(16, 41);
            this.rbVendor.Name = "rbVendor";
            this.rbVendor.Size = new System.Drawing.Size(59, 17);
            this.rbVendor.TabIndex = 1;
            this.rbVendor.Text = "Vendor";
            this.rbVendor.UseVisualStyleBackColor = true;
            this.rbVendor.CheckedChanged += new System.EventHandler(this.rbVendor_CheckedChanged);
            // 
            // rbCustomer
            // 
            this.rbCustomer.AutoSize = true;
            this.rbCustomer.Checked = true;
            this.rbCustomer.Location = new System.Drawing.Point(16, 19);
            this.rbCustomer.Name = "rbCustomer";
            this.rbCustomer.Size = new System.Drawing.Size(69, 17);
            this.rbCustomer.TabIndex = 0;
            this.rbCustomer.TabStop = true;
            this.rbCustomer.Text = "Customer";
            this.rbCustomer.UseVisualStyleBackColor = true;
            this.rbCustomer.CheckedChanged += new System.EventHandler(this.rbCustomer_CheckedChanged);
            // 
            // cmbMsgType
            // 
            this.cmbMsgType.FormattingEnabled = true;
            this.cmbMsgType.Items.AddRange(new object[] {
            "(none selected)",
            "Original",
            "Response",
            "Forward",
            "eAdapter",
            "Conversion"});
            this.cmbMsgType.Location = new System.Drawing.Point(107, 145);
            this.cmbMsgType.Name = "cmbMsgType";
            this.cmbMsgType.Size = new System.Drawing.Size(121, 21);
            this.cmbMsgType.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(24, 148);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Message Type";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 11);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Description";
            // 
            // tbRecieveProperties
            // 
            this.tbRecieveProperties.Controls.Add(this.gbReceiveOptions);
            this.tbRecieveProperties.Location = new System.Drawing.Point(4, 22);
            this.tbRecieveProperties.Name = "tbRecieveProperties";
            this.tbRecieveProperties.Padding = new System.Windows.Forms.Padding(3);
            this.tbRecieveProperties.Size = new System.Drawing.Size(637, 328);
            this.tbRecieveProperties.TabIndex = 1;
            this.tbRecieveProperties.Text = "Receive Properties";
            this.tbRecieveProperties.UseVisualStyleBackColor = true;
            // 
            // gbReceiveOptions
            // 
            this.gbReceiveOptions.Controls.Add(this.edReceiveCustomerName);
            this.gbReceiveOptions.Controls.Add(this.lblReceiveCustomerName);
            this.gbReceiveOptions.Controls.Add(this.btnReceiveFolder);
            this.gbReceiveOptions.Controls.Add(this.txtTestResults);
            this.gbReceiveOptions.Controls.Add(this.label33);
            this.gbReceiveOptions.Controls.Add(this.bbTestRx);
            this.gbReceiveOptions.Controls.Add(this.edReceiveSubject);
            this.gbReceiveOptions.Controls.Add(this.label15);
            this.gbReceiveOptions.Controls.Add(this.edReceiveSender);
            this.gbReceiveOptions.Controls.Add(this.label14);
            this.gbReceiveOptions.Controls.Add(this.cbSSL);
            this.gbReceiveOptions.Controls.Add(this.cmbFileType);
            this.gbReceiveOptions.Controls.Add(this.label12);
            this.gbReceiveOptions.Controls.Add(this.edReceiveEmailAddress);
            this.gbReceiveOptions.Controls.Add(this.lblReceiveEmailAddress);
            this.gbReceiveOptions.Controls.Add(this.gbReceiveMethod);
            this.gbReceiveOptions.Controls.Add(this.edFTPReceivePath);
            this.gbReceiveOptions.Controls.Add(this.lblFTPReceivePath);
            this.gbReceiveOptions.Controls.Add(this.edFTPReceivePort);
            this.gbReceiveOptions.Controls.Add(this.lblFTPReceivePort);
            this.gbReceiveOptions.Controls.Add(this.edReceivePassword);
            this.gbReceiveOptions.Controls.Add(this.lblReceivePassword);
            this.gbReceiveOptions.Controls.Add(this.edReceiveUsername);
            this.gbReceiveOptions.Controls.Add(this.lblReceiveUsername);
            this.gbReceiveOptions.Controls.Add(this.edServerAddress);
            this.gbReceiveOptions.Controls.Add(this.lblServerAddress);
            this.gbReceiveOptions.Controls.Add(this.label8);
            this.gbReceiveOptions.Location = new System.Drawing.Point(9, 5);
            this.gbReceiveOptions.Margin = new System.Windows.Forms.Padding(2);
            this.gbReceiveOptions.Name = "gbReceiveOptions";
            this.gbReceiveOptions.Padding = new System.Windows.Forms.Padding(2);
            this.gbReceiveOptions.Size = new System.Drawing.Size(610, 350);
            this.gbReceiveOptions.TabIndex = 19;
            this.gbReceiveOptions.TabStop = false;
            this.gbReceiveOptions.Text = "Receive Options";
            // 
            // edReceiveCustomerName
            // 
            this.edReceiveCustomerName.Location = new System.Drawing.Point(108, 143);
            this.edReceiveCustomerName.Margin = new System.Windows.Forms.Padding(2);
            this.edReceiveCustomerName.Name = "edReceiveCustomerName";
            this.edReceiveCustomerName.Size = new System.Drawing.Size(161, 20);
            this.edReceiveCustomerName.TabIndex = 10;
            // 
            // lblReceiveCustomerName
            // 
            this.lblReceiveCustomerName.AutoSize = true;
            this.lblReceiveCustomerName.Location = new System.Drawing.Point(14, 146);
            this.lblReceiveCustomerName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblReceiveCustomerName.Name = "lblReceiveCustomerName";
            this.lblReceiveCustomerName.Size = new System.Drawing.Size(82, 13);
            this.lblReceiveCustomerName.TabIndex = 41;
            this.lblReceiveCustomerName.Text = "Customer Name";
            // 
            // btnReceiveFolder
            // 
            this.btnReceiveFolder.Location = new System.Drawing.Point(457, 90);
            this.btnReceiveFolder.Name = "btnReceiveFolder";
            this.btnReceiveFolder.Size = new System.Drawing.Size(25, 20);
            this.btnReceiveFolder.TabIndex = 39;
            this.btnReceiveFolder.Text = "...";
            this.btnReceiveFolder.UseVisualStyleBackColor = true;
            this.btnReceiveFolder.Click += new System.EventHandler(this.btnReceiveFolder_Click);
            // 
            // txtTestResults
            // 
            this.txtTestResults.Location = new System.Drawing.Point(109, 244);
            this.txtTestResults.Multiline = true;
            this.txtTestResults.Name = "txtTestResults";
            this.txtTestResults.Size = new System.Drawing.Size(408, 52);
            this.txtTestResults.TabIndex = 14;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(17, 244);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(80, 13);
            this.label33.TabIndex = 37;
            this.label33.Text = "Receive results";
            // 
            // bbTestRx
            // 
            this.bbTestRx.Location = new System.Drawing.Point(448, 116);
            this.bbTestRx.Name = "bbTestRx";
            this.bbTestRx.Size = new System.Drawing.Size(97, 23);
            this.bbTestRx.TabIndex = 9;
            this.bbTestRx.Text = "&Test Receive";
            this.bbTestRx.UseVisualStyleBackColor = true;
            this.bbTestRx.Click += new System.EventHandler(this.bbTestRx_Click);
            // 
            // edReceiveSubject
            // 
            this.edReceiveSubject.Location = new System.Drawing.Point(108, 217);
            this.edReceiveSubject.Margin = new System.Windows.Forms.Padding(2);
            this.edReceiveSubject.Name = "edReceiveSubject";
            this.edReceiveSubject.Size = new System.Drawing.Size(410, 20);
            this.edReceiveSubject.TabIndex = 13;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(14, 220);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(91, 13);
            this.label15.TabIndex = 35;
            this.label15.Text = "Expected Subject";
            // 
            // edReceiveSender
            // 
            this.edReceiveSender.Location = new System.Drawing.Point(108, 193);
            this.edReceiveSender.Margin = new System.Windows.Forms.Padding(2);
            this.edReceiveSender.Name = "edReceiveSender";
            this.edReceiveSender.Size = new System.Drawing.Size(410, 20);
            this.edReceiveSender.TabIndex = 12;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(14, 196);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(69, 13);
            this.label14.TabIndex = 33;
            this.label14.Text = "Sender Email";
            // 
            // cbSSL
            // 
            this.cbSSL.AutoSize = true;
            this.cbSSL.Location = new System.Drawing.Point(497, 92);
            this.cbSSL.Name = "cbSSL";
            this.cbSSL.Size = new System.Drawing.Size(46, 17);
            this.cbSSL.TabIndex = 31;
            this.cbSSL.Text = "SSL";
            this.cbSSL.UseVisualStyleBackColor = true;
            // 
            // cmbFileType
            // 
            this.cmbFileType.FormattingEnabled = true;
            this.cmbFileType.Items.AddRange(new object[] {
            "*.*",
            "TXT",
            "XML",
            "XLS",
            "CSV",
            "DAT",
            "PDF"});
            this.cmbFileType.Location = new System.Drawing.Point(507, 31);
            this.cmbFileType.Name = "cmbFileType";
            this.cmbFileType.Size = new System.Drawing.Size(64, 21);
            this.cmbFileType.TabIndex = 30;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(403, 34);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(98, 13);
            this.label12.TabIndex = 29;
            this.label12.Text = "File Type Expected";
            // 
            // edReceiveEmailAddress
            // 
            this.edReceiveEmailAddress.Location = new System.Drawing.Point(108, 169);
            this.edReceiveEmailAddress.Margin = new System.Windows.Forms.Padding(2);
            this.edReceiveEmailAddress.Name = "edReceiveEmailAddress";
            this.edReceiveEmailAddress.Size = new System.Drawing.Size(410, 20);
            this.edReceiveEmailAddress.TabIndex = 11;
            // 
            // lblReceiveEmailAddress
            // 
            this.lblReceiveEmailAddress.AutoSize = true;
            this.lblReceiveEmailAddress.Location = new System.Drawing.Point(14, 172);
            this.lblReceiveEmailAddress.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblReceiveEmailAddress.Name = "lblReceiveEmailAddress";
            this.lblReceiveEmailAddress.Size = new System.Drawing.Size(80, 13);
            this.lblReceiveEmailAddress.TabIndex = 28;
            this.lblReceiveEmailAddress.Text = "Recipient Email";
            // 
            // gbReceiveMethod
            // 
            this.gbReceiveMethod.Controls.Add(this.rbReceivePickup);
            this.gbReceiveMethod.Controls.Add(this.rbEmail);
            this.gbReceiveMethod.Controls.Add(this.rbReceiveFTP);
            this.gbReceiveMethod.Controls.Add(this.rbPickup);
            this.gbReceiveMethod.Location = new System.Drawing.Point(15, 18);
            this.gbReceiveMethod.Margin = new System.Windows.Forms.Padding(2);
            this.gbReceiveMethod.Name = "gbReceiveMethod";
            this.gbReceiveMethod.Padding = new System.Windows.Forms.Padding(2);
            this.gbReceiveMethod.Size = new System.Drawing.Size(383, 38);
            this.gbReceiveMethod.TabIndex = 3;
            this.gbReceiveMethod.TabStop = false;
            this.gbReceiveMethod.Text = "Delivery Method (Receive)";
            // 
            // rbReceivePickup
            // 
            this.rbReceivePickup.AutoSize = true;
            this.rbReceivePickup.Location = new System.Drawing.Point(259, 16);
            this.rbReceivePickup.Name = "rbReceivePickup";
            this.rbReceivePickup.Size = new System.Drawing.Size(113, 17);
            this.rbReceivePickup.TabIndex = 4;
            this.rbReceivePickup.TabStop = true;
            this.rbReceivePickup.Text = "Pickup from Folder";
            this.rbReceivePickup.UseVisualStyleBackColor = true;
            this.rbReceivePickup.CheckedChanged += new System.EventHandler(this.rbPickup_CheckedChanged);
            // 
            // rbEmail
            // 
            this.rbEmail.AutoSize = true;
            this.rbEmail.Location = new System.Drawing.Point(203, 17);
            this.rbEmail.Margin = new System.Windows.Forms.Padding(2);
            this.rbEmail.Name = "rbEmail";
            this.rbEmail.Size = new System.Drawing.Size(50, 17);
            this.rbEmail.TabIndex = 3;
            this.rbEmail.TabStop = true;
            this.rbEmail.Tag = "email";
            this.rbEmail.Text = "E&mail";
            this.rbEmail.UseVisualStyleBackColor = true;
            this.rbEmail.CheckedChanged += new System.EventHandler(this.rbPickup_CheckedChanged);
            // 
            // rbReceiveFTP
            // 
            this.rbReceiveFTP.AutoSize = true;
            this.rbReceiveFTP.Location = new System.Drawing.Point(137, 17);
            this.rbReceiveFTP.Margin = new System.Windows.Forms.Padding(2);
            this.rbReceiveFTP.Name = "rbReceiveFTP";
            this.rbReceiveFTP.Size = new System.Drawing.Size(62, 17);
            this.rbReceiveFTP.TabIndex = 2;
            this.rbReceiveFTP.TabStop = true;
            this.rbReceiveFTP.Tag = "ftp";
            this.rbReceiveFTP.Text = "via FTP";
            this.rbReceiveFTP.UseVisualStyleBackColor = true;
            this.rbReceiveFTP.CheckedChanged += new System.EventHandler(this.rbPickup_CheckedChanged);
            // 
            // rbPickup
            // 
            this.rbPickup.AutoSize = true;
            this.rbPickup.Location = new System.Drawing.Point(5, 17);
            this.rbPickup.Margin = new System.Windows.Forms.Padding(2);
            this.rbPickup.Name = "rbPickup";
            this.rbPickup.Size = new System.Drawing.Size(134, 17);
            this.rbPickup.TabIndex = 0;
            this.rbPickup.TabStop = true;
            this.rbPickup.Tag = "pickup";
            this.rbPickup.Text = "Pickup (via CTC Node)";
            this.rbPickup.UseVisualStyleBackColor = true;
            this.rbPickup.CheckedChanged += new System.EventHandler(this.rbPickup_CheckedChanged);
            // 
            // edFTPReceivePath
            // 
            this.edFTPReceivePath.Location = new System.Drawing.Point(197, 90);
            this.edFTPReceivePath.Name = "edFTPReceivePath";
            this.edFTPReceivePath.Size = new System.Drawing.Size(263, 20);
            this.edFTPReceivePath.TabIndex = 6;
            // 
            // lblFTPReceivePath
            // 
            this.lblFTPReceivePath.AutoSize = true;
            this.lblFTPReceivePath.Location = new System.Drawing.Point(162, 94);
            this.lblFTPReceivePath.Name = "lblFTPReceivePath";
            this.lblFTPReceivePath.Size = new System.Drawing.Size(29, 13);
            this.lblFTPReceivePath.TabIndex = 26;
            this.lblFTPReceivePath.Text = "Path";
            // 
            // edFTPReceivePort
            // 
            this.edFTPReceivePort.Location = new System.Drawing.Point(108, 91);
            this.edFTPReceivePort.Margin = new System.Windows.Forms.Padding(2);
            this.edFTPReceivePort.Name = "edFTPReceivePort";
            this.edFTPReceivePort.Size = new System.Drawing.Size(46, 20);
            this.edFTPReceivePort.TabIndex = 5;
            // 
            // lblFTPReceivePort
            // 
            this.lblFTPReceivePort.AutoSize = true;
            this.lblFTPReceivePort.Location = new System.Drawing.Point(14, 94);
            this.lblFTPReceivePort.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFTPReceivePort.Name = "lblFTPReceivePort";
            this.lblFTPReceivePort.Size = new System.Drawing.Size(63, 13);
            this.lblFTPReceivePort.TabIndex = 25;
            this.lblFTPReceivePort.Text = "Default Port";
            // 
            // edReceivePassword
            // 
            this.edReceivePassword.Location = new System.Drawing.Point(340, 117);
            this.edReceivePassword.Margin = new System.Windows.Forms.Padding(2);
            this.edReceivePassword.Name = "edReceivePassword";
            this.edReceivePassword.Size = new System.Drawing.Size(103, 20);
            this.edReceivePassword.TabIndex = 8;
            // 
            // lblReceivePassword
            // 
            this.lblReceivePassword.AutoSize = true;
            this.lblReceivePassword.Location = new System.Drawing.Point(273, 120);
            this.lblReceivePassword.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblReceivePassword.Name = "lblReceivePassword";
            this.lblReceivePassword.Size = new System.Drawing.Size(53, 13);
            this.lblReceivePassword.TabIndex = 23;
            this.lblReceivePassword.Text = "Password";
            // 
            // edReceiveUsername
            // 
            this.edReceiveUsername.Location = new System.Drawing.Point(108, 117);
            this.edReceiveUsername.Margin = new System.Windows.Forms.Padding(2);
            this.edReceiveUsername.Name = "edReceiveUsername";
            this.edReceiveUsername.Size = new System.Drawing.Size(161, 20);
            this.edReceiveUsername.TabIndex = 7;
            // 
            // lblReceiveUsername
            // 
            this.lblReceiveUsername.AutoSize = true;
            this.lblReceiveUsername.Location = new System.Drawing.Point(14, 120);
            this.lblReceiveUsername.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblReceiveUsername.Name = "lblReceiveUsername";
            this.lblReceiveUsername.Size = new System.Drawing.Size(58, 13);
            this.lblReceiveUsername.TabIndex = 21;
            this.lblReceiveUsername.Text = "User name";
            // 
            // edServerAddress
            // 
            this.edServerAddress.Location = new System.Drawing.Point(108, 65);
            this.edServerAddress.Margin = new System.Windows.Forms.Padding(2);
            this.edServerAddress.Name = "edServerAddress";
            this.edServerAddress.Size = new System.Drawing.Size(410, 20);
            this.edServerAddress.TabIndex = 4;
            // 
            // lblServerAddress
            // 
            this.lblServerAddress.AutoSize = true;
            this.lblServerAddress.Location = new System.Drawing.Point(14, 68);
            this.lblServerAddress.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblServerAddress.Name = "lblServerAddress";
            this.lblServerAddress.Size = new System.Drawing.Size(79, 13);
            this.lblServerAddress.TabIndex = 19;
            this.lblServerAddress.Text = "Server Address";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(1, -11);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(154, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Send (on Forward) Details";
            // 
            // tbSendProperties
            // 
            this.tbSendProperties.Controls.Add(this.gbSendOptions);
            this.tbSendProperties.Location = new System.Drawing.Point(4, 22);
            this.tbSendProperties.Name = "tbSendProperties";
            this.tbSendProperties.Size = new System.Drawing.Size(637, 328);
            this.tbSendProperties.TabIndex = 2;
            this.tbSendProperties.Text = "Send Properties";
            this.tbSendProperties.UseVisualStyleBackColor = true;
            // 
            // gbSendOptions
            // 
            this.gbSendOptions.Controls.Add(this.btnPickupFolder);
            this.gbSendOptions.Controls.Add(this.edSendSubject);
            this.gbSendOptions.Controls.Add(this.lblSendSubject);
            this.gbSendOptions.Controls.Add(this.edSendEmailAddress);
            this.gbSendOptions.Controls.Add(this.lblSendEmailAddress);
            this.gbSendOptions.Controls.Add(this.edSendFtpPort);
            this.gbSendOptions.Controls.Add(this.lblSendFTPPort);
            this.gbSendOptions.Controls.Add(this.btnTestSendeAdapter);
            this.gbSendOptions.Controls.Add(this.edSendeAdapterResults);
            this.gbSendOptions.Controls.Add(this.lblTestResults);
            this.gbSendOptions.Controls.Add(this.edSendFolder);
            this.gbSendOptions.Controls.Add(this.gbSendMethod);
            this.gbSendOptions.Controls.Add(this.lblSendPath);
            this.gbSendOptions.Controls.Add(this.edSendPassword);
            this.gbSendOptions.Controls.Add(this.lblSendPassword);
            this.gbSendOptions.Controls.Add(this.edSendUsername);
            this.gbSendOptions.Controls.Add(this.lblSendUsername);
            this.gbSendOptions.Controls.Add(this.edSendCTCUrl);
            this.gbSendOptions.Controls.Add(this.lblSendServer);
            this.gbSendOptions.Controls.Add(this.label16);
            this.gbSendOptions.Location = new System.Drawing.Point(9, 4);
            this.gbSendOptions.Margin = new System.Windows.Forms.Padding(2);
            this.gbSendOptions.Name = "gbSendOptions";
            this.gbSendOptions.Padding = new System.Windows.Forms.Padding(2);
            this.gbSendOptions.Size = new System.Drawing.Size(608, 341);
            this.gbSendOptions.TabIndex = 21;
            this.gbSendOptions.TabStop = false;
            this.gbSendOptions.Text = "Send Options";
            // 
            // btnPickupFolder
            // 
            this.btnPickupFolder.Location = new System.Drawing.Point(476, 64);
            this.btnPickupFolder.Name = "btnPickupFolder";
            this.btnPickupFolder.Size = new System.Drawing.Size(28, 20);
            this.btnPickupFolder.TabIndex = 35;
            this.btnPickupFolder.Text = "...";
            this.btnPickupFolder.UseVisualStyleBackColor = true;
            this.btnPickupFolder.Click += new System.EventHandler(this.btnPickupFolder_Click);
            // 
            // edSendSubject
            // 
            this.edSendSubject.Location = new System.Drawing.Point(94, 133);
            this.edSendSubject.Margin = new System.Windows.Forms.Padding(2);
            this.edSendSubject.Name = "edSendSubject";
            this.edSendSubject.Size = new System.Drawing.Size(410, 20);
            this.edSendSubject.TabIndex = 33;
            // 
            // lblSendSubject
            // 
            this.lblSendSubject.AutoSize = true;
            this.lblSendSubject.Location = new System.Drawing.Point(13, 139);
            this.lblSendSubject.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSendSubject.Name = "lblSendSubject";
            this.lblSendSubject.Size = new System.Drawing.Size(43, 13);
            this.lblSendSubject.TabIndex = 34;
            this.lblSendSubject.Text = "Subject";
            // 
            // edSendEmailAddress
            // 
            this.edSendEmailAddress.Location = new System.Drawing.Point(94, 156);
            this.edSendEmailAddress.Margin = new System.Windows.Forms.Padding(2);
            this.edSendEmailAddress.Name = "edSendEmailAddress";
            this.edSendEmailAddress.Size = new System.Drawing.Size(410, 20);
            this.edSendEmailAddress.TabIndex = 31;
            // 
            // lblSendEmailAddress
            // 
            this.lblSendEmailAddress.AutoSize = true;
            this.lblSendEmailAddress.Location = new System.Drawing.Point(13, 162);
            this.lblSendEmailAddress.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSendEmailAddress.Name = "lblSendEmailAddress";
            this.lblSendEmailAddress.Size = new System.Drawing.Size(73, 13);
            this.lblSendEmailAddress.TabIndex = 32;
            this.lblSendEmailAddress.Text = "Email Address";
            // 
            // edSendFtpPort
            // 
            this.edSendFtpPort.Location = new System.Drawing.Point(94, 87);
            this.edSendFtpPort.Margin = new System.Windows.Forms.Padding(2);
            this.edSendFtpPort.Name = "edSendFtpPort";
            this.edSendFtpPort.Size = new System.Drawing.Size(46, 20);
            this.edSendFtpPort.TabIndex = 29;
            // 
            // lblSendFTPPort
            // 
            this.lblSendFTPPort.AutoSize = true;
            this.lblSendFTPPort.Location = new System.Drawing.Point(13, 90);
            this.lblSendFTPPort.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSendFTPPort.Name = "lblSendFTPPort";
            this.lblSendFTPPort.Size = new System.Drawing.Size(63, 13);
            this.lblSendFTPPort.TabIndex = 30;
            this.lblSendFTPPort.Text = "Default Port";
            // 
            // btnTestSendeAdapter
            // 
            this.btnTestSendeAdapter.Location = new System.Drawing.Point(142, 181);
            this.btnTestSendeAdapter.Name = "btnTestSendeAdapter";
            this.btnTestSendeAdapter.Size = new System.Drawing.Size(125, 23);
            this.btnTestSendeAdapter.TabIndex = 28;
            this.btnTestSendeAdapter.Text = "Test eAdapter Service";
            this.btnTestSendeAdapter.UseVisualStyleBackColor = true;
            // 
            // edSendeAdapterResults
            // 
            this.edSendeAdapterResults.Location = new System.Drawing.Point(21, 210);
            this.edSendeAdapterResults.Multiline = true;
            this.edSendeAdapterResults.Name = "edSendeAdapterResults";
            this.edSendeAdapterResults.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.edSendeAdapterResults.Size = new System.Drawing.Size(575, 93);
            this.edSendeAdapterResults.TabIndex = 27;
            // 
            // lblTestResults
            // 
            this.lblTestResults.AutoSize = true;
            this.lblTestResults.Location = new System.Drawing.Point(14, 186);
            this.lblTestResults.Name = "lblTestResults";
            this.lblTestResults.Size = new System.Drawing.Size(112, 13);
            this.lblTestResults.TabIndex = 26;
            this.lblTestResults.Text = "eAdapter Test Results";
            // 
            // edSendFolder
            // 
            this.edSendFolder.Location = new System.Drawing.Point(228, 87);
            this.edSendFolder.Margin = new System.Windows.Forms.Padding(2);
            this.edSendFolder.Name = "edSendFolder";
            this.edSendFolder.Size = new System.Drawing.Size(276, 20);
            this.edSendFolder.TabIndex = 24;
            // 
            // gbSendMethod
            // 
            this.gbSendMethod.Controls.Add(this.rbSoapSend);
            this.gbSendMethod.Controls.Add(this.rbFTPSendDirect);
            this.gbSendMethod.Controls.Add(this.rbEmailSend);
            this.gbSendMethod.Controls.Add(this.rbFTPSend);
            this.gbSendMethod.Controls.Add(this.rbCTCSend);
            this.gbSendMethod.Controls.Add(this.rbPickupSend);
            this.gbSendMethod.Location = new System.Drawing.Point(12, 17);
            this.gbSendMethod.Margin = new System.Windows.Forms.Padding(2);
            this.gbSendMethod.Name = "gbSendMethod";
            this.gbSendMethod.Padding = new System.Windows.Forms.Padding(2);
            this.gbSendMethod.Size = new System.Drawing.Size(522, 41);
            this.gbSendMethod.TabIndex = 20;
            this.gbSendMethod.TabStop = false;
            this.gbSendMethod.Text = "Delivery Method (Send)";
            // 
            // rbSoapSend
            // 
            this.rbSoapSend.AutoSize = true;
            this.rbSoapSend.Location = new System.Drawing.Point(408, 18);
            this.rbSoapSend.Name = "rbSoapSend";
            this.rbSoapSend.Size = new System.Drawing.Size(84, 17);
            this.rbSoapSend.TabIndex = 5;
            this.rbSoapSend.TabStop = true;
            this.rbSoapSend.Text = "&Soap/REST";
            this.rbSoapSend.UseVisualStyleBackColor = true;
            this.rbSoapSend.CheckedChanged += new System.EventHandler(this.rbPickupSend_CheckedChanged);
            // 
            // rbFTPSendDirect
            // 
            this.rbFTPSendDirect.AutoSize = true;
            this.rbFTPSendDirect.Location = new System.Drawing.Point(156, 17);
            this.rbFTPSendDirect.Margin = new System.Windows.Forms.Padding(2);
            this.rbFTPSendDirect.Name = "rbFTPSendDirect";
            this.rbFTPSendDirect.Size = new System.Drawing.Size(82, 17);
            this.rbFTPSendDirect.TabIndex = 4;
            this.rbFTPSendDirect.TabStop = true;
            this.rbFTPSendDirect.Tag = "ftp";
            this.rbFTPSendDirect.Text = "FTP (&Direct)";
            this.rbFTPSendDirect.UseVisualStyleBackColor = true;
            this.rbFTPSendDirect.CheckedChanged += new System.EventHandler(this.rbPickupSend_CheckedChanged);
            // 
            // rbEmailSend
            // 
            this.rbEmailSend.AutoSize = true;
            this.rbEmailSend.Location = new System.Drawing.Point(344, 17);
            this.rbEmailSend.Margin = new System.Windows.Forms.Padding(2);
            this.rbEmailSend.Name = "rbEmailSend";
            this.rbEmailSend.Size = new System.Drawing.Size(50, 17);
            this.rbEmailSend.TabIndex = 3;
            this.rbEmailSend.TabStop = true;
            this.rbEmailSend.Tag = "email";
            this.rbEmailSend.Text = "E&mail";
            this.rbEmailSend.UseVisualStyleBackColor = true;
            this.rbEmailSend.CheckedChanged += new System.EventHandler(this.rbPickupSend_CheckedChanged);
            // 
            // rbFTPSend
            // 
            this.rbFTPSend.AutoSize = true;
            this.rbFTPSend.Location = new System.Drawing.Point(67, 17);
            this.rbFTPSend.Margin = new System.Windows.Forms.Padding(2);
            this.rbFTPSend.Name = "rbFTPSend";
            this.rbFTPSend.Size = new System.Drawing.Size(85, 17);
            this.rbFTPSend.TabIndex = 2;
            this.rbFTPSend.TabStop = true;
            this.rbFTPSend.Tag = "ftp";
            this.rbFTPSend.Text = "FTP (&Polling)";
            this.rbFTPSend.UseVisualStyleBackColor = true;
            this.rbFTPSend.CheckedChanged += new System.EventHandler(this.rbPickupSend_CheckedChanged);
            // 
            // rbCTCSend
            // 
            this.rbCTCSend.AutoSize = true;
            this.rbCTCSend.Location = new System.Drawing.Point(254, 17);
            this.rbCTCSend.Margin = new System.Windows.Forms.Padding(2);
            this.rbCTCSend.Name = "rbCTCSend";
            this.rbCTCSend.Size = new System.Drawing.Size(86, 17);
            this.rbCTCSend.TabIndex = 1;
            this.rbCTCSend.TabStop = true;
            this.rbCTCSend.Tag = "eadapter";
            this.rbCTCSend.Text = "&CTC Adapter";
            this.rbCTCSend.UseVisualStyleBackColor = true;
            this.rbCTCSend.CheckedChanged += new System.EventHandler(this.rbPickupSend_CheckedChanged);
            // 
            // rbPickupSend
            // 
            this.rbPickupSend.AutoSize = true;
            this.rbPickupSend.Location = new System.Drawing.Point(5, 17);
            this.rbPickupSend.Margin = new System.Windows.Forms.Padding(2);
            this.rbPickupSend.Name = "rbPickupSend";
            this.rbPickupSend.Size = new System.Drawing.Size(58, 17);
            this.rbPickupSend.TabIndex = 0;
            this.rbPickupSend.TabStop = true;
            this.rbPickupSend.Tag = "pickup";
            this.rbPickupSend.Text = "Pick&up";
            this.rbPickupSend.UseVisualStyleBackColor = true;
            this.rbPickupSend.CheckedChanged += new System.EventHandler(this.rbPickupSend_CheckedChanged);
            // 
            // lblSendPath
            // 
            this.lblSendPath.AutoSize = true;
            this.lblSendPath.Location = new System.Drawing.Point(158, 90);
            this.lblSendPath.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSendPath.Name = "lblSendPath";
            this.lblSendPath.Size = new System.Drawing.Size(63, 13);
            this.lblSendPath.TabIndex = 25;
            this.lblSendPath.Text = "Folder/Path";
            // 
            // edSendPassword
            // 
            this.edSendPassword.Location = new System.Drawing.Point(334, 111);
            this.edSendPassword.Margin = new System.Windows.Forms.Padding(2);
            this.edSendPassword.Name = "edSendPassword";
            this.edSendPassword.Size = new System.Drawing.Size(170, 20);
            this.edSendPassword.TabIndex = 22;
            // 
            // lblSendPassword
            // 
            this.lblSendPassword.AutoSize = true;
            this.lblSendPassword.Location = new System.Drawing.Point(277, 113);
            this.lblSendPassword.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSendPassword.Name = "lblSendPassword";
            this.lblSendPassword.Size = new System.Drawing.Size(53, 13);
            this.lblSendPassword.TabIndex = 23;
            this.lblSendPassword.Text = "Password";
            // 
            // edSendUsername
            // 
            this.edSendUsername.Location = new System.Drawing.Point(94, 110);
            this.edSendUsername.Margin = new System.Windows.Forms.Padding(2);
            this.edSendUsername.Name = "edSendUsername";
            this.edSendUsername.Size = new System.Drawing.Size(179, 20);
            this.edSendUsername.TabIndex = 20;
            // 
            // lblSendUsername
            // 
            this.lblSendUsername.AutoSize = true;
            this.lblSendUsername.Location = new System.Drawing.Point(12, 114);
            this.lblSendUsername.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSendUsername.Name = "lblSendUsername";
            this.lblSendUsername.Size = new System.Drawing.Size(55, 13);
            this.lblSendUsername.TabIndex = 21;
            this.lblSendUsername.Text = "Username";
            // 
            // edSendCTCUrl
            // 
            this.edSendCTCUrl.Location = new System.Drawing.Point(95, 64);
            this.edSendCTCUrl.Margin = new System.Windows.Forms.Padding(2);
            this.edSendCTCUrl.Name = "edSendCTCUrl";
            this.edSendCTCUrl.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.edSendCTCUrl.Size = new System.Drawing.Size(383, 20);
            this.edSendCTCUrl.TabIndex = 4;
            // 
            // lblSendServer
            // 
            this.lblSendServer.AutoSize = true;
            this.lblSendServer.Location = new System.Drawing.Point(12, 66);
            this.lblSendServer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSendServer.Name = "lblSendServer";
            this.lblSendServer.Size = new System.Drawing.Size(79, 13);
            this.lblSendServer.TabIndex = 19;
            this.lblSendServer.Text = "Server Address";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(1, -11);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(154, 13);
            this.label16.TabIndex = 18;
            this.label16.Text = "Send (on Forward) Details";
            // 
            // tbConversion
            // 
            this.tbConversion.Controls.Add(this.edCustomMethod);
            this.tbConversion.Controls.Add(this.label36);
            this.tbConversion.Controls.Add(this.btnAddParam);
            this.tbConversion.Controls.Add(this.edParameter);
            this.tbConversion.Controls.Add(this.label35);
            this.tbConversion.Controls.Add(this.lbParameters);
            this.tbConversion.Controls.Add(this.label34);
            this.tbConversion.Controls.Add(this.tvwXSD);
            this.tbConversion.Controls.Add(this.btnLoadXSD);
            this.tbConversion.Controls.Add(this.label11);
            this.tbConversion.Controls.Add(this.ddlCWContext);
            this.tbConversion.Controls.Add(this.btnXsdPath);
            this.tbConversion.Controls.Add(this.edLibraryName);
            this.tbConversion.Controls.Add(this.label17);
            this.tbConversion.Controls.Add(this.edXSDPath);
            this.tbConversion.Controls.Add(this.label18);
            this.tbConversion.Controls.Add(this.label19);
            this.tbConversion.Location = new System.Drawing.Point(4, 22);
            this.tbConversion.Name = "tbConversion";
            this.tbConversion.Size = new System.Drawing.Size(637, 328);
            this.tbConversion.TabIndex = 3;
            this.tbConversion.Text = "Conversion Library";
            this.tbConversion.UseVisualStyleBackColor = true;
            // 
            // edCustomMethod
            // 
            this.edCustomMethod.Location = new System.Drawing.Point(21, 221);
            this.edCustomMethod.Name = "edCustomMethod";
            this.edCustomMethod.Size = new System.Drawing.Size(336, 20);
            this.edCustomMethod.TabIndex = 49;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(24, 205);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(112, 13);
            this.label36.TabIndex = 48;
            this.label36.Text = "Custom Method Name";
            // 
            // btnAddParam
            // 
            this.btnAddParam.Location = new System.Drawing.Point(360, 286);
            this.btnAddParam.Name = "btnAddParam";
            this.btnAddParam.Size = new System.Drawing.Size(75, 23);
            this.btnAddParam.TabIndex = 47;
            this.btnAddParam.Text = "Add Param";
            this.btnAddParam.UseVisualStyleBackColor = true;
            this.btnAddParam.Click += new System.EventHandler(this.btnAddParam_Click);
            // 
            // edParameter
            // 
            this.edParameter.Location = new System.Drawing.Point(360, 260);
            this.edParameter.Name = "edParameter";
            this.edParameter.Size = new System.Drawing.Size(268, 20);
            this.edParameter.TabIndex = 46;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(362, 244);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(77, 13);
            this.label35.TabIndex = 45;
            this.label35.Text = "Add Parameter";
            // 
            // lbParameters
            // 
            this.lbParameters.FormattingEnabled = true;
            this.lbParameters.Location = new System.Drawing.Point(363, 185);
            this.lbParameters.Name = "lbParameters";
            this.lbParameters.Size = new System.Drawing.Size(265, 56);
            this.lbParameters.TabIndex = 31;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(366, 169);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(98, 13);
            this.label34.TabIndex = 44;
            this.label34.Text = "Special Parameters";
            // 
            // tvwXSD
            // 
            this.tvwXSD.Location = new System.Drawing.Point(16, 50);
            this.tvwXSD.Name = "tvwXSD";
            this.tvwXSD.Size = new System.Drawing.Size(603, 111);
            this.tvwXSD.TabIndex = 43;
            // 
            // btnLoadXSD
            // 
            this.btnLoadXSD.Location = new System.Drawing.Point(401, 21);
            this.btnLoadXSD.Name = "btnLoadXSD";
            this.btnLoadXSD.Size = new System.Drawing.Size(75, 23);
            this.btnLoadXSD.TabIndex = 42;
            this.btnLoadXSD.Text = "&Load XSD";
            this.btnLoadXSD.UseVisualStyleBackColor = true;
            this.btnLoadXSD.Click += new System.EventHandler(this.btnLoadXSD_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(151, 164);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(121, 13);
            this.label11.TabIndex = 41;
            this.label11.Text = "Cargowise Data Context";
            // 
            // ddlCWContext
            // 
            this.ddlCWContext.FormattingEnabled = true;
            this.ddlCWContext.Location = new System.Drawing.Point(154, 181);
            this.ddlCWContext.Name = "ddlCWContext";
            this.ddlCWContext.Size = new System.Drawing.Size(203, 21);
            this.ddlCWContext.TabIndex = 40;
            // 
            // btnXsdPath
            // 
            this.btnXsdPath.Location = new System.Drawing.Point(365, 24);
            this.btnXsdPath.Margin = new System.Windows.Forms.Padding(2);
            this.btnXsdPath.Name = "btnXsdPath";
            this.btnXsdPath.Size = new System.Drawing.Size(25, 20);
            this.btnXsdPath.TabIndex = 39;
            this.btnXsdPath.Text = "...";
            this.btnXsdPath.UseVisualStyleBackColor = true;
            this.btnXsdPath.Click += new System.EventHandler(this.btnXsdPath_Click);
            // 
            // edLibraryName
            // 
            this.edLibraryName.Location = new System.Drawing.Point(18, 182);
            this.edLibraryName.Name = "edLibraryName";
            this.edLibraryName.Size = new System.Drawing.Size(109, 20);
            this.edLibraryName.TabIndex = 38;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(24, 164);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(125, 13);
            this.label17.TabIndex = 37;
            this.label17.Text = "Conversion Library Name";
            // 
            // edXSDPath
            // 
            this.edXSDPath.Location = new System.Drawing.Point(69, 24);
            this.edXSDPath.Name = "edXSDPath";
            this.edXSDPath.Size = new System.Drawing.Size(297, 20);
            this.edXSDPath.TabIndex = 35;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(11, 24);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(54, 13);
            this.label18.TabIndex = 36;
            this.label18.Text = "XSD Path";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(15, 8);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(211, 13);
            this.label19.TabIndex = 34;
            this.label19.Text = "Custom XSD and Conversion Library";
            // 
            // tbDTS
            // 
            this.tbDTS.Controls.Add(this.gbDTS);
            this.tbDTS.Location = new System.Drawing.Point(4, 22);
            this.tbDTS.Name = "tbDTS";
            this.tbDTS.Size = new System.Drawing.Size(637, 328);
            this.tbDTS.TabIndex = 4;
            this.tbDTS.Text = "DTS";
            this.tbDTS.UseVisualStyleBackColor = true;
            // 
            // gbDTS
            // 
            this.gbDTS.Controls.Add(this.edDTSCurrentValue);
            this.gbDTS.Controls.Add(this.label23);
            this.gbDTS.Controls.Add(this.edDTSValue);
            this.gbDTS.Controls.Add(this.edDTSTarget);
            this.gbDTS.Controls.Add(this.edDTSSearch);
            this.gbDTS.Controls.Add(this.edDTSQual);
            this.gbDTS.Controls.Add(this.bbAddDTS);
            this.gbDTS.Controls.Add(this.dgDts);
            this.gbDTS.Controls.Add(this.gbModifies);
            this.gbDTS.Controls.Add(this.ddlDTSFileType);
            this.gbDTS.Controls.Add(this.ddlDTSType);
            this.gbDTS.Controls.Add(this.edDTSIndex);
            this.gbDTS.Controls.Add(this.label27);
            this.gbDTS.Controls.Add(this.label26);
            this.gbDTS.Controls.Add(this.label25);
            this.gbDTS.Controls.Add(this.label24);
            this.gbDTS.Controls.Add(this.label22);
            this.gbDTS.Controls.Add(this.label21);
            this.gbDTS.Controls.Add(this.label20);
            this.gbDTS.Location = new System.Drawing.Point(9, 5);
            this.gbDTS.Name = "gbDTS";
            this.gbDTS.Size = new System.Drawing.Size(622, 307);
            this.gbDTS.TabIndex = 0;
            this.gbDTS.TabStop = false;
            this.gbDTS.Text = "Data Transformation Profiles";
            // 
            // edDTSCurrentValue
            // 
            this.edDTSCurrentValue.Location = new System.Drawing.Point(14, 237);
            this.edDTSCurrentValue.Name = "edDTSCurrentValue";
            this.edDTSCurrentValue.Size = new System.Drawing.Size(258, 20);
            this.edDTSCurrentValue.TabIndex = 19;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(13, 262);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(59, 13);
            this.label23.TabIndex = 18;
            this.label23.Text = "New Value";
            // 
            // edDTSValue
            // 
            this.edDTSValue.Location = new System.Drawing.Point(13, 278);
            this.edDTSValue.Name = "edDTSValue";
            this.edDTSValue.Size = new System.Drawing.Size(259, 20);
            this.edDTSValue.TabIndex = 17;
            // 
            // edDTSTarget
            // 
            this.edDTSTarget.Location = new System.Drawing.Point(13, 196);
            this.edDTSTarget.Name = "edDTSTarget";
            this.edDTSTarget.Size = new System.Drawing.Size(260, 20);
            this.edDTSTarget.TabIndex = 16;
            // 
            // edDTSSearch
            // 
            this.edDTSSearch.Location = new System.Drawing.Point(13, 157);
            this.edDTSSearch.Name = "edDTSSearch";
            this.edDTSSearch.Size = new System.Drawing.Size(260, 20);
            this.edDTSSearch.TabIndex = 15;
            // 
            // edDTSQual
            // 
            this.edDTSQual.Location = new System.Drawing.Point(13, 101);
            this.edDTSQual.Multiline = true;
            this.edDTSQual.Name = "edDTSQual";
            this.edDTSQual.Size = new System.Drawing.Size(327, 36);
            this.edDTSQual.TabIndex = 14;
            // 
            // bbAddDTS
            // 
            this.bbAddDTS.Location = new System.Drawing.Point(376, 262);
            this.bbAddDTS.Name = "bbAddDTS";
            this.bbAddDTS.Size = new System.Drawing.Size(75, 23);
            this.bbAddDTS.TabIndex = 13;
            this.bbAddDTS.Text = "Add DTS";
            this.bbAddDTS.UseVisualStyleBackColor = true;
            this.bbAddDTS.Click += new System.EventHandler(this.bbAddDTS_Click);
            // 
            // dgDts
            // 
            this.dgDts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DTSIndex,
            this.DTSType,
            this.DTSSearch,
            this.DTSModifies,
            this.DTSFileType,
            this.DTSQual,
            this.DTSTarget,
            this.DTSNewValue,
            this.CurrentValue});
            this.dgDts.Location = new System.Drawing.Point(376, 19);
            this.dgDts.Name = "dgDts";
            this.dgDts.ReadOnly = true;
            this.dgDts.RowHeadersVisible = false;
            this.dgDts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgDts.Size = new System.Drawing.Size(240, 232);
            this.dgDts.TabIndex = 12;
            // 
            // DTSIndex
            // 
            this.DTSIndex.DataPropertyName = "D_INDEX";
            this.DTSIndex.HeaderText = "Index";
            this.DTSIndex.Name = "DTSIndex";
            this.DTSIndex.ReadOnly = true;
            this.DTSIndex.Width = 40;
            // 
            // DTSType
            // 
            this.DTSType.DataPropertyName = "D_DTS";
            this.DTSType.HeaderText = "Type";
            this.DTSType.Name = "DTSType";
            this.DTSType.ReadOnly = true;
            this.DTSType.Width = 40;
            // 
            // DTSSearch
            // 
            this.DTSSearch.DataPropertyName = "D_SEARCHPATTERN";
            this.DTSSearch.HeaderText = "Search";
            this.DTSSearch.Name = "DTSSearch";
            this.DTSSearch.ReadOnly = true;
            // 
            // DTSModifies
            // 
            this.DTSModifies.DataPropertyName = "D_DTSTYPE";
            this.DTSModifies.HeaderText = "Modifies";
            this.DTSModifies.Name = "DTSModifies";
            this.DTSModifies.ReadOnly = true;
            this.DTSModifies.Width = 50;
            // 
            // DTSFileType
            // 
            this.DTSFileType.DataPropertyName = "D_FILETYPE";
            this.DTSFileType.HeaderText = "File Type";
            this.DTSFileType.Name = "DTSFileType";
            this.DTSFileType.ReadOnly = true;
            this.DTSFileType.Visible = false;
            // 
            // DTSQual
            // 
            this.DTSQual.DataPropertyName = "D_QUALIFIER";
            this.DTSQual.HeaderText = "Qualifier";
            this.DTSQual.Name = "DTSQual";
            this.DTSQual.ReadOnly = true;
            this.DTSQual.Visible = false;
            // 
            // DTSTarget
            // 
            this.DTSTarget.DataPropertyName = "D_TARGET";
            this.DTSTarget.HeaderText = "Target";
            this.DTSTarget.Name = "DTSTarget";
            this.DTSTarget.ReadOnly = true;
            this.DTSTarget.Visible = false;
            // 
            // DTSNewValue
            // 
            this.DTSNewValue.DataPropertyName = "D_NEWVALUE";
            this.DTSNewValue.HeaderText = "New Value";
            this.DTSNewValue.Name = "DTSNewValue";
            this.DTSNewValue.ReadOnly = true;
            this.DTSNewValue.Visible = false;
            // 
            // CurrentValue
            // 
            this.CurrentValue.DataPropertyName = "D_CURRENTVALUE";
            this.CurrentValue.HeaderText = "Current Value";
            this.CurrentValue.Name = "CurrentValue";
            this.CurrentValue.ReadOnly = true;
            this.CurrentValue.Visible = false;
            // 
            // gbModifies
            // 
            this.gbModifies.Controls.Add(this.rbDTSStructure);
            this.gbModifies.Controls.Add(this.rbDTSRecord);
            this.gbModifies.Location = new System.Drawing.Point(163, 46);
            this.gbModifies.Name = "gbModifies";
            this.gbModifies.Size = new System.Drawing.Size(177, 34);
            this.gbModifies.TabIndex = 11;
            this.gbModifies.TabStop = false;
            this.gbModifies.Text = "Modifies:";
            // 
            // rbDTSStructure
            // 
            this.rbDTSStructure.AutoSize = true;
            this.rbDTSStructure.Location = new System.Drawing.Point(103, 14);
            this.rbDTSStructure.Name = "rbDTSStructure";
            this.rbDTSStructure.Size = new System.Drawing.Size(68, 17);
            this.rbDTSStructure.TabIndex = 1;
            this.rbDTSStructure.TabStop = true;
            this.rbDTSStructure.Text = "Structure";
            this.rbDTSStructure.UseVisualStyleBackColor = true;
            // 
            // rbDTSRecord
            // 
            this.rbDTSRecord.AutoSize = true;
            this.rbDTSRecord.Location = new System.Drawing.Point(8, 14);
            this.rbDTSRecord.Name = "rbDTSRecord";
            this.rbDTSRecord.Size = new System.Drawing.Size(60, 17);
            this.rbDTSRecord.TabIndex = 0;
            this.rbDTSRecord.TabStop = true;
            this.rbDTSRecord.Text = "Record";
            this.rbDTSRecord.UseVisualStyleBackColor = true;
            // 
            // ddlDTSFileType
            // 
            this.ddlDTSFileType.FormattingEnabled = true;
            this.ddlDTSFileType.Items.AddRange(new object[] {
            "XML",
            "TXT",
            "CSV",
            "XLS"});
            this.ddlDTSFileType.Location = new System.Drawing.Point(71, 52);
            this.ddlDTSFileType.Name = "ddlDTSFileType";
            this.ddlDTSFileType.Size = new System.Drawing.Size(59, 21);
            this.ddlDTSFileType.TabIndex = 10;
            // 
            // ddlDTSType
            // 
            this.ddlDTSType.FormattingEnabled = true;
            this.ddlDTSType.Location = new System.Drawing.Point(163, 19);
            this.ddlDTSType.Name = "ddlDTSType";
            this.ddlDTSType.Size = new System.Drawing.Size(75, 21);
            this.ddlDTSType.TabIndex = 9;
            // 
            // edDTSIndex
            // 
            this.edDTSIndex.Location = new System.Drawing.Point(54, 19);
            this.edDTSIndex.Name = "edDTSIndex";
            this.edDTSIndex.Size = new System.Drawing.Size(31, 20);
            this.edDTSIndex.TabIndex = 8;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(12, 219);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(166, 13);
            this.label27.TabIndex = 7;
            this.label27.Text = "Current Value to Replace/Update";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(13, 180);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(38, 13);
            this.label26.TabIndex = 6;
            this.label26.Text = "Target";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(12, 142);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(118, 13);
            this.label25.TabIndex = 5;
            this.label25.Text = "Seach for Parent (XML)";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(13, 82);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(70, 13);
            this.label24.TabIndex = 4;
            this.label24.Text = "DTS Qualifier";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(101, 22);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(56, 13);
            this.label22.TabIndex = 2;
            this.label22.Text = "DTS Type";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(15, 22);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(33, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "Index";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(15, 55);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(50, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "File Type";
            // 
            // btnNewProfile
            // 
            this.btnNewProfile.Location = new System.Drawing.Point(480, 384);
            this.btnNewProfile.Margin = new System.Windows.Forms.Padding(2);
            this.btnNewProfile.Name = "btnNewProfile";
            this.btnNewProfile.Size = new System.Drawing.Size(77, 24);
            this.btnNewProfile.TabIndex = 43;
            this.btnNewProfile.Text = "&New";
            this.btnNewProfile.UseVisualStyleBackColor = true;
            this.btnNewProfile.Click += new System.EventHandler(this.btnNewProfile_Click);
            // 
            // label29
            // 
            this.label29.BackColor = System.Drawing.Color.LightSkyBlue;
            this.label29.Location = new System.Drawing.Point(108, 16);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(96, 20);
            this.label29.TabIndex = 42;
            this.label29.Text = "Send Profile";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.LawnGreen;
            this.label28.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label28.Location = new System.Drawing.Point(6, 16);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(96, 20);
            this.label28.TabIndex = 41;
            this.label28.Text = "Receive Profile";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(571, 384);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(77, 24);
            this.btnAdd.TabIndex = 40;
            this.btnAdd.Text = "Add  >>";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Location = new System.Drawing.Point(1027, 405);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(214, 45);
            this.groupBox2.TabIndex = 44;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Legend";
            // 
            // cmProfiles
            // 
            this.cmProfiles.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteProfileToolStripMenuItem,
            this.duplicateProfileToolStripMenuItem,
            this.toolStripMenuItem1,
            this.markInactiveToolStripMenuItem});
            this.cmProfiles.Name = "cmProfiles";
            this.cmProfiles.Size = new System.Drawing.Size(162, 76);
            // 
            // deleteProfileToolStripMenuItem
            // 
            this.deleteProfileToolStripMenuItem.Name = "deleteProfileToolStripMenuItem";
            this.deleteProfileToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.deleteProfileToolStripMenuItem.Text = "Delete Profile";
            this.deleteProfileToolStripMenuItem.Click += new System.EventHandler(this.deleteProfileToolStripMenuItem_Click);
            // 
            // duplicateProfileToolStripMenuItem
            // 
            this.duplicateProfileToolStripMenuItem.Name = "duplicateProfileToolStripMenuItem";
            this.duplicateProfileToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.duplicateProfileToolStripMenuItem.Text = "Duplicate Profile";
            this.duplicateProfileToolStripMenuItem.Click += new System.EventHandler(this.duplicateProfileToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(158, 6);
            // 
            // markInactiveToolStripMenuItem
            // 
            this.markInactiveToolStripMenuItem.Name = "markInactiveToolStripMenuItem";
            this.markInactiveToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.markInactiveToolStripMenuItem.Text = "Mark Inactive";
            this.markInactiveToolStripMenuItem.Click += new System.EventHandler(this.markInactiveToolStripMenuItem_Click);
            // 
            // frmProfiles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1250, 499);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnNewProfile);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.tcMessageProfile);
            this.Controls.Add(this.dgProfile);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnClose);
            this.Name = "frmProfiles";
            this.Text = "Customer Profiles";
            this.Load += new System.EventHandler(this.frmProfiles_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgProfile)).EndInit();
            this.tcMessageProfile.ResumeLayout(false);
            this.tbMessageHeader.ResumeLayout(false);
            this.tbMessageHeader.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbDirection.ResumeLayout(false);
            this.gbDirection.PerformLayout();
            this.grpBillto.ResumeLayout(false);
            this.grpBillto.PerformLayout();
            this.tbRecieveProperties.ResumeLayout(false);
            this.gbReceiveOptions.ResumeLayout(false);
            this.gbReceiveOptions.PerformLayout();
            this.gbReceiveMethod.ResumeLayout(false);
            this.gbReceiveMethod.PerformLayout();
            this.tbSendProperties.ResumeLayout(false);
            this.gbSendOptions.ResumeLayout(false);
            this.gbSendOptions.PerformLayout();
            this.gbSendMethod.ResumeLayout(false);
            this.gbSendMethod.PerformLayout();
            this.tbConversion.ResumeLayout(false);
            this.tbConversion.PerformLayout();
            this.tbDTS.ResumeLayout(false);
            this.gbDTS.ResumeLayout(false);
            this.gbDTS.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDts)).EndInit();
            this.gbModifies.ResumeLayout(false);
            this.gbModifies.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.cmProfiles.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dgProfile;
        private System.Windows.Forms.DataGridViewTextBoxColumn P_ID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn P_DESCRIPTION;
        private System.Windows.Forms.DataGridViewTextBoxColumn P_SENDERID;
        private System.Windows.Forms.DataGridViewTextBoxColumn P_RECIPIENTID;
        private System.Windows.Forms.DataGridViewTextBoxColumn P_REASONCODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn Direction;
        private System.Windows.Forms.TabControl tcMessageProfile;
        private System.Windows.Forms.TabPage tbMessageHeader;
        private System.Windows.Forms.CheckBox cbGroupCharges;
        private System.Windows.Forms.CheckBox cbProfileActive;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox edEventCode;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.CheckBox cbDTS;
        private System.Windows.Forms.TextBox edReasonCode;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox edSenderID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox edRecipientID;
        private System.Windows.Forms.GroupBox gbDirection;
        private System.Windows.Forms.RadioButton rbReceive;
        private System.Windows.Forms.RadioButton rbSend;
        private System.Windows.Forms.TextBox edDescription;
        private System.Windows.Forms.CheckBox cbChargeable;
        private System.Windows.Forms.GroupBox grpBillto;
        private System.Windows.Forms.RadioButton rbVendor;
        private System.Windows.Forms.RadioButton rbCustomer;
        private System.Windows.Forms.ComboBox cmbMsgType;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabPage tbRecieveProperties;
        private System.Windows.Forms.GroupBox gbReceiveOptions;
        private System.Windows.Forms.TextBox edReceiveCustomerName;
        private System.Windows.Forms.Label lblReceiveCustomerName;
        private System.Windows.Forms.Button btnReceiveFolder;
        private System.Windows.Forms.TextBox txtTestResults;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button bbTestRx;
        private System.Windows.Forms.TextBox edReceiveSubject;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox edReceiveSender;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox cbSSL;
        private System.Windows.Forms.ComboBox cmbFileType;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox edReceiveEmailAddress;
        private System.Windows.Forms.Label lblReceiveEmailAddress;
        private System.Windows.Forms.GroupBox gbReceiveMethod;
        private System.Windows.Forms.RadioButton rbReceivePickup;
        private System.Windows.Forms.RadioButton rbEmail;
        private System.Windows.Forms.RadioButton rbReceiveFTP;
        private System.Windows.Forms.RadioButton rbPickup;
        private System.Windows.Forms.TextBox edFTPReceivePath;
        private System.Windows.Forms.Label lblFTPReceivePath;
        private System.Windows.Forms.TextBox edFTPReceivePort;
        private System.Windows.Forms.Label lblFTPReceivePort;
        private System.Windows.Forms.TextBox edReceivePassword;
        private System.Windows.Forms.Label lblReceivePassword;
        private System.Windows.Forms.TextBox edReceiveUsername;
        private System.Windows.Forms.Label lblReceiveUsername;
        private System.Windows.Forms.TextBox edServerAddress;
        private System.Windows.Forms.Label lblServerAddress;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TabPage tbSendProperties;
        private System.Windows.Forms.GroupBox gbSendOptions;
        private System.Windows.Forms.Button btnPickupFolder;
        private System.Windows.Forms.TextBox edSendSubject;
        private System.Windows.Forms.Label lblSendSubject;
        private System.Windows.Forms.TextBox edSendEmailAddress;
        private System.Windows.Forms.Label lblSendEmailAddress;
        private System.Windows.Forms.TextBox edSendFtpPort;
        private System.Windows.Forms.Label lblSendFTPPort;
        private System.Windows.Forms.Button btnTestSendeAdapter;
        private System.Windows.Forms.TextBox edSendeAdapterResults;
        private System.Windows.Forms.Label lblTestResults;
        private System.Windows.Forms.TextBox edSendFolder;
        private System.Windows.Forms.GroupBox gbSendMethod;
        private System.Windows.Forms.RadioButton rbSoapSend;
        private System.Windows.Forms.RadioButton rbFTPSendDirect;
        private System.Windows.Forms.RadioButton rbEmailSend;
        private System.Windows.Forms.RadioButton rbFTPSend;
        private System.Windows.Forms.RadioButton rbCTCSend;
        private System.Windows.Forms.RadioButton rbPickupSend;
        private System.Windows.Forms.Label lblSendPath;
        private System.Windows.Forms.TextBox edSendPassword;
        private System.Windows.Forms.Label lblSendPassword;
        private System.Windows.Forms.TextBox edSendUsername;
        private System.Windows.Forms.Label lblSendUsername;
        private System.Windows.Forms.TextBox edSendCTCUrl;
        private System.Windows.Forms.Label lblSendServer;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TabPage tbConversion;
        private System.Windows.Forms.TextBox edCustomMethod;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Button btnAddParam;
        private System.Windows.Forms.TextBox edParameter;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ListBox lbParameters;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TreeView tvwXSD;
        private System.Windows.Forms.Button btnLoadXSD;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox ddlCWContext;
        private System.Windows.Forms.Button btnXsdPath;
        private System.Windows.Forms.TextBox edLibraryName;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox edXSDPath;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TabPage tbDTS;
        private System.Windows.Forms.GroupBox gbDTS;
        private System.Windows.Forms.TextBox edDTSCurrentValue;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox edDTSValue;
        private System.Windows.Forms.TextBox edDTSTarget;
        private System.Windows.Forms.TextBox edDTSSearch;
        private System.Windows.Forms.TextBox edDTSQual;
        private System.Windows.Forms.Button bbAddDTS;
        private System.Windows.Forms.DataGridView dgDts;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTSIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTSType;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTSSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTSModifies;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTSFileType;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTSQual;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTSTarget;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTSNewValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrentValue;
        private System.Windows.Forms.GroupBox gbModifies;
        private System.Windows.Forms.RadioButton rbDTSStructure;
        private System.Windows.Forms.RadioButton rbDTSRecord;
        private System.Windows.Forms.ComboBox ddlDTSFileType;
        private System.Windows.Forms.ComboBox ddlDTSType;
        private System.Windows.Forms.TextBox edDTSIndex;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btnNewProfile;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ContextMenuStrip cmProfiles;
        private System.Windows.Forms.ToolStripMenuItem deleteProfileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem duplicateProfileToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem markInactiveToolStripMenuItem;
        private System.Windows.Forms.FolderBrowserDialog fdProfile;
    }
}