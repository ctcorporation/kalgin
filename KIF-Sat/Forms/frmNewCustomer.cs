﻿using KIF_Sat.DTO;
using KIF_Sat.Models;
using System;
using System.Linq;
using System.Windows.Forms;

namespace KIF_Sat
{
    public partial class frmNewCustomer : Form
    {
        #region members

        #endregion


        #region Properties
        public Customer Customer { get; set; }
        #endregion;

        #region Constructors

        public frmNewCustomer()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods
        private void frmNewCustomer_Load(object sender, EventArgs e)
        {
            GetCustomer(Globals.glCustCode);
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            using (IUnitOfWork uow = new UnitOfWork(new Models.KifEntity(Globals.ConnString.ConnString)))
            {
                var customer = (uow.Customers.Find(x => x.C_ID == Globals.gl_CustId)).FirstOrDefault();
                bool newRecord = false;
                if (customer == null)
                {
                    customer = new Models.Customer();
                    customer.C_ID = Guid.NewGuid();
                    newRecord = true;
                }

                customer.C_NAME = edCustomerName.Text;
                customer.C_CODE = edCustCode.Text;
                customer.C_PATH = edRootPath.Text;
                customer.C_SHORTNAME = edShortName.Text;
                customer.C_IS_ACTIVE = cbActive.Checked ? "Y" : "F";
                customer.C_ON_HOLD = cbHold.Checked ? "Y" : "N";
                customer.C_FTP_CLIENT = cbFtpClient.Checked ? "Y" : "N";
                customer.C_TRIAL = cbTrial.Checked ? "Y" : "N";
                if (cbTrial.Checked)
                {
                    DateTime pDate = new DateTime();
                    customer.C_TRIALSTART = DateTime.TryParse(dtFrom.Text, out pDate) ? pDate : DateTime.MinValue;
                    customer.C_TRIALEND = DateTime.TryParse(dtTo.Text, out pDate) ? pDate : DateTime.MinValue;
                }
                if (newRecord)
                {
                    uow.Customers.Add(customer);
                }
                uow.Complete();
                Globals.gl_CustId = (Guid)customer.C_ID;

            };

            btnSave.Text = "&Save";

        }
        #endregion

        #region Helpers
        private Customer GetCustomer(string custCode)
        {

            using (IUnitOfWork uow = new UnitOfWork(new KifEntity(Globals.ConnString.ConnString)))
            {
                var cust = (uow.Customers.Find(x => x.C_CODE == custCode)).FirstOrDefault();
                if (cust != null)
                {
                    cbActive.Checked = cust.C_IS_ACTIVE == "Y" ? true : false;
                    cbHold.Checked = cust.C_IS_ACTIVE == "Y" ? true : false;
                    cbFtpClient.Checked = cust.C_FTP_CLIENT == "Y" ? true : false;
                    Globals.gl_CustId = cust.C_ID;
                    btnSave.Text = "&Update";
                    return cust;
                }
                else
                {
                    btnSave.Text = "&Save";
                    edRootPath.Text = Globals.glProfilePath;
                    edCustCode.Text = Globals.glCustCode;
                    return null;
                }

            }

        }
        #endregion
    }
}

