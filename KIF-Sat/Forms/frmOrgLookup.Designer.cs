﻿namespace KIF_Sat
{
    partial class frmOrgLookup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnClose = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bbNew = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.edAirPOO = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.edAirPOL = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.edSeaPOO = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.edSeaPol = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.edPod = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.edCompanyName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.edCountryCode = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.edState = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.edCity = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.edPostCode = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.edAddress2 = new System.Windows.Forms.TextBox();
            this.edAddress1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.edShortName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.edCargowiseCode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.edMapValue = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgAddress = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MapValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShortName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompanyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AirPOO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SeaPOO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AirPol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Address1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Address2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Suburb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PostCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrgType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbOrgType = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.edFind = new System.Windows.Forms.TextBox();
            this.btnFind = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAddress)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(538, 653);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 30;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.cmbOrgType);
            this.groupBox1.Controls.Add(this.bbNew);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.edPod);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.edCompanyName);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.edCountryCode);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.edState);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.edCity);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.edPostCode);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.edAddress2);
            this.groupBox1.Controls.Add(this.edAddress1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.edShortName);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.edCargowiseCode);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.edMapValue);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 268);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(607, 379);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Organisation Lookup details";
            // 
            // bbNew
            // 
            this.bbNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bbNew.Location = new System.Drawing.Point(525, 318);
            this.bbNew.Name = "bbNew";
            this.bbNew.Size = new System.Drawing.Size(75, 23);
            this.bbNew.TabIndex = 27;
            this.bbNew.Text = "&New";
            this.bbNew.UseVisualStyleBackColor = true;
            this.bbNew.Click += new System.EventHandler(this.bbNew_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.edAirPOO);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.edAirPOL);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Location = new System.Drawing.Point(374, 74);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(118, 97);
            this.groupBox3.TabIndex = 26;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Air Freight";
            // 
            // edAirPOO
            // 
            this.edAirPOO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edAirPOO.Location = new System.Drawing.Point(6, 30);
            this.edAirPOO.Name = "edAirPOO";
            this.edAirPOO.Size = new System.Drawing.Size(100, 20);
            this.edAirPOO.TabIndex = 6;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 14);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(68, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "Port of Origin";
            // 
            // edAirPOL
            // 
            this.edAirPOL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edAirPOL.Location = new System.Drawing.Point(6, 70);
            this.edAirPOL.Name = "edAirPOL";
            this.edAirPOL.Size = new System.Drawing.Size(100, 20);
            this.edAirPOL.TabIndex = 7;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 54);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 13);
            this.label15.TabIndex = 25;
            this.label15.Text = "Port of Loading";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.edSeaPOO);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.edSeaPol);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Location = new System.Drawing.Point(249, 74);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(119, 97);
            this.groupBox2.TabIndex = 25;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Sea Freight";
            // 
            // edSeaPOO
            // 
            this.edSeaPOO.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edSeaPOO.Location = new System.Drawing.Point(6, 29);
            this.edSeaPOO.Name = "edSeaPOO";
            this.edSeaPOO.Size = new System.Drawing.Size(100, 20);
            this.edSeaPOO.TabIndex = 4;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 13);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 13);
            this.label13.TabIndex = 23;
            this.label13.Text = "Port of Origin";
            // 
            // edSeaPol
            // 
            this.edSeaPol.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edSeaPol.Location = new System.Drawing.Point(6, 69);
            this.edSeaPol.Name = "edSeaPol";
            this.edSeaPol.Size = new System.Drawing.Size(100, 20);
            this.edSeaPol.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 53);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(79, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "Port of Loading";
            // 
            // edPod
            // 
            this.edPod.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edPod.Location = new System.Drawing.Point(17, 166);
            this.edPod.Name = "edPod";
            this.edPod.Size = new System.Drawing.Size(100, 20);
            this.edPod.TabIndex = 8;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(18, 150);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(89, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "Port of Discharge";
            // 
            // edCompanyName
            // 
            this.edCompanyName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edCompanyName.Location = new System.Drawing.Point(17, 126);
            this.edCompanyName.Name = "edCompanyName";
            this.edCompanyName.Size = new System.Drawing.Size(226, 20);
            this.edCompanyName.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 110);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Company Name";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(525, 347);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 28;
            this.button1.Text = "&Add";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // edCountryCode
            // 
            this.edCountryCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edCountryCode.Location = new System.Drawing.Point(219, 352);
            this.edCountryCode.Name = "edCountryCode";
            this.edCountryCode.Size = new System.Drawing.Size(35, 20);
            this.edCountryCode.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(216, 336);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Country Code";
            // 
            // edState
            // 
            this.edState.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edState.Location = new System.Drawing.Point(21, 352);
            this.edState.Name = "edState";
            this.edState.Size = new System.Drawing.Size(189, 20);
            this.edState.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(20, 336);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "State";
            // 
            // edCity
            // 
            this.edCity.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edCity.Location = new System.Drawing.Point(103, 313);
            this.edCity.Name = "edCity";
            this.edCity.Size = new System.Drawing.Size(235, 20);
            this.edCity.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(102, 297);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "City";
            // 
            // edPostCode
            // 
            this.edPostCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edPostCode.Location = new System.Drawing.Point(21, 313);
            this.edPostCode.Name = "edPostCode";
            this.edPostCode.Size = new System.Drawing.Size(67, 20);
            this.edPostCode.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 297);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Post Code";
            // 
            // edAddress2
            // 
            this.edAddress2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edAddress2.Location = new System.Drawing.Point(19, 276);
            this.edAddress2.Name = "edAddress2";
            this.edAddress2.Size = new System.Drawing.Size(319, 20);
            this.edAddress2.TabIndex = 12;
            // 
            // edAddress1
            // 
            this.edAddress1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edAddress1.Location = new System.Drawing.Point(19, 250);
            this.edAddress1.Name = "edAddress1";
            this.edAddress1.Size = new System.Drawing.Size(319, 20);
            this.edAddress1.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 227);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Address ";
            // 
            // edShortName
            // 
            this.edShortName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edShortName.Location = new System.Drawing.Point(143, 204);
            this.edShortName.Name = "edShortName";
            this.edShortName.Size = new System.Drawing.Size(195, 20);
            this.edShortName.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(139, 188);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Cargowise Short Name";
            // 
            // edCargowiseCode
            // 
            this.edCargowiseCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edCargowiseCode.Location = new System.Drawing.Point(17, 204);
            this.edCargowiseCode.Name = "edCargowiseCode";
            this.edCargowiseCode.Size = new System.Drawing.Size(100, 20);
            this.edCargowiseCode.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 188);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Cargowise Code";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(158, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Org Type (Spreadsheet Column)";
            // 
            // edMapValue
            // 
            this.edMapValue.Location = new System.Drawing.Point(17, 48);
            this.edMapValue.Name = "edMapValue";
            this.edMapValue.Size = new System.Drawing.Size(531, 20);
            this.edMapValue.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(194, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Organisation Name (from Spread Sheet)";
            // 
            // dgAddress
            // 
            this.dgAddress.AllowUserToAddRows = false;
            this.dgAddress.AllowUserToDeleteRows = false;
            this.dgAddress.AllowUserToResizeRows = false;
            this.dgAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgAddress.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAddress.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.MapValue,
            this.ShortName,
            this.CompanyName,
            this.AirPOO,
            this.SeaPOO,
            this.POL,
            this.AirPol,
            this.POD,
            this.Address1,
            this.Address2,
            this.Suburb,
            this.PostCode,
            this.OrgType,
            this.Code});
            this.dgAddress.ContextMenuStrip = this.contextMenuStrip1;
            this.dgAddress.Location = new System.Drawing.Point(15, 47);
            this.dgAddress.Name = "dgAddress";
            this.dgAddress.ReadOnly = true;
            this.dgAddress.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgAddress.Size = new System.Drawing.Size(604, 215);
            this.dgAddress.TabIndex = 3;
            this.dgAddress.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler(this.dgAddress_CellContextMenuStripNeeded);
            this.dgAddress.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgAddress_CellDoubleClick);
            this.dgAddress.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgAddress_CellMouseDown);
            // 
            // ID
            // 
            this.ID.DataPropertyName = "OR_ID";
            this.ID.HeaderText = "OR_ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            // 
            // MapValue
            // 
            this.MapValue.DataPropertyName = "OR_MAPVALUE";
            this.MapValue.HeaderText = "Mapped Value";
            this.MapValue.Name = "MapValue";
            this.MapValue.ReadOnly = true;
            // 
            // ShortName
            // 
            this.ShortName.DataPropertyName = "OR_SHORTCODE";
            this.ShortName.HeaderText = "Short Name";
            this.ShortName.Name = "ShortName";
            this.ShortName.ReadOnly = true;
            // 
            // CompanyName
            // 
            this.CompanyName.DataPropertyName = "OR_NAME";
            this.CompanyName.HeaderText = "Company Name";
            this.CompanyName.Name = "CompanyName";
            this.CompanyName.ReadOnly = true;
            // 
            // AirPOO
            // 
            this.AirPOO.DataPropertyName = "OR_AirPortOfOrigin";
            this.AirPOO.HeaderText = "POO (Air)";
            this.AirPOO.Name = "AirPOO";
            this.AirPOO.ReadOnly = true;
            // 
            // SeaPOO
            // 
            this.SeaPOO.DataPropertyName = "OR_SeaPortOfOrigin";
            this.SeaPOO.HeaderText = "POO (Sea)";
            this.SeaPOO.Name = "SeaPOO";
            this.SeaPOO.ReadOnly = true;
            // 
            // POL
            // 
            this.POL.DataPropertyName = "OR_PORTOFLOADING";
            this.POL.HeaderText = "POL (Sea)";
            this.POL.Name = "POL";
            this.POL.ReadOnly = true;
            // 
            // AirPol
            // 
            this.AirPol.DataPropertyName = "OR_AirPortOfLoading";
            this.AirPol.HeaderText = "POL (Air)";
            this.AirPol.Name = "AirPol";
            this.AirPol.ReadOnly = true;
            // 
            // POD
            // 
            this.POD.DataPropertyName = "OR_PORTOFDISCHARGE";
            this.POD.HeaderText = "POD";
            this.POD.Name = "POD";
            this.POD.ReadOnly = true;
            // 
            // Address1
            // 
            this.Address1.DataPropertyName = "OR_ADDRESS1";
            this.Address1.HeaderText = "Address1";
            this.Address1.Name = "Address1";
            this.Address1.ReadOnly = true;
            // 
            // Address2
            // 
            this.Address2.DataPropertyName = "OR_ADDRESS2";
            this.Address2.HeaderText = "Address2";
            this.Address2.Name = "Address2";
            this.Address2.ReadOnly = true;
            // 
            // Suburb
            // 
            this.Suburb.DataPropertyName = "OR_SUBURB";
            this.Suburb.HeaderText = "Suburb";
            this.Suburb.Name = "Suburb";
            this.Suburb.ReadOnly = true;
            // 
            // PostCode
            // 
            this.PostCode.DataPropertyName = "OR_POSTCODE";
            this.PostCode.HeaderText = "Post Code";
            this.PostCode.Name = "PostCode";
            this.PostCode.ReadOnly = true;
            // 
            // OrgType
            // 
            this.OrgType.DataPropertyName = "OR_TYPE";
            this.OrgType.HeaderText = "Mapped From";
            this.OrgType.Name = "OrgType";
            this.OrgType.ReadOnly = true;
            // 
            // Code
            // 
            this.Code.DataPropertyName = "OR_CODE";
            this.Code.HeaderText = "Cargowise Code";
            this.Code.Name = "Code";
            this.Code.ReadOnly = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(108, 26);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // cmbOrgType
            // 
            this.cmbOrgType.FormattingEnabled = true;
            this.cmbOrgType.Items.AddRange(new object[] {
            "(none selected)",
            "Consignee",
            "Deliver To",
            "Forwarding Agent",
            "Supplier"});
            this.cmbOrgType.Location = new System.Drawing.Point(18, 88);
            this.cmbOrgType.Name = "cmbOrgType";
            this.cmbOrgType.Size = new System.Drawing.Size(223, 21);
            this.cmbOrgType.TabIndex = 2;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(27, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(27, 13);
            this.label16.TabIndex = 31;
            this.label16.Text = "Find";
            // 
            // edFind
            // 
            this.edFind.Location = new System.Drawing.Point(60, 18);
            this.edFind.Name = "edFind";
            this.edFind.Size = new System.Drawing.Size(405, 20);
            this.edFind.TabIndex = 32;
            // 
            // btnFind
            // 
            this.btnFind.Image = global::KIF_Sat.Properties.Resources.find;
            this.btnFind.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnFind.Location = new System.Drawing.Point(472, 18);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(54, 23);
            this.btnFind.TabIndex = 33;
            this.btnFind.Text = "&Find";
            this.btnFind.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // frmOrgLookup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 688);
            this.Controls.Add(this.btnFind);
            this.Controls.Add(this.edFind);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.dgAddress);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnClose);
            this.Name = "frmOrgLookup";
            this.Text = "Organisation Lookup Entry";
            this.Load += new System.EventHandler(this.frmOrgLookup_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAddress)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox edCountryCode;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox edState;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox edCity;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox edPostCode;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox edAddress2;
        private System.Windows.Forms.TextBox edAddress1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox edShortName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox edCargowiseCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox edMapValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgAddress;
        private System.Windows.Forms.TextBox edCompanyName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox edPod;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox edSeaPol;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox edAirPOO;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox edAirPOL;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox edSeaPOO;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn MapValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShortName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AirPOO;
        private System.Windows.Forms.DataGridViewTextBoxColumn SeaPOO;
        private System.Windows.Forms.DataGridViewTextBoxColumn POL;
        private System.Windows.Forms.DataGridViewTextBoxColumn AirPol;
        private System.Windows.Forms.DataGridViewTextBoxColumn POD;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Suburb;
        private System.Windows.Forms.DataGridViewTextBoxColumn PostCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrgType;
        private System.Windows.Forms.DataGridViewTextBoxColumn Code;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.Button bbNew;
        private System.Windows.Forms.ComboBox cmbOrgType;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox edFind;
        private System.Windows.Forms.Button btnFind;
    }
}