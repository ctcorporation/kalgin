﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace KIF_Sat
{
    public partial class frmSettings : Form
    {
        SqlConnection sqlConn, sqlCTCConn;
        public frmSettings()
        {
            InitializeComponent();
        }

        private void frmSettings_Load(object sender, EventArgs e)
        {
            LoadSettings();
        }

        private void LoadSettings()
        {

            try
            {
                //edCustomerLocation.Text = Globals.glProfilePath;
                edMailServer.Text = SMTPServer.Server;
                edPort.Text = SMTPServer.Port;
                edMailUsername.Text = SMTPServer.Username;
                edMailPassword.Text = SMTPServer.Password;
                edEmailAddress.Text = SMTPServer.Email;
                edServer.Text = Globals.glDbServer;
                edInstance.Text = Globals.glDbInstance;
                edUserName.Text = Globals.glDbUserName;
                edPassword.Text = Globals.glDbPassword;
                edAlertsTo.Text = Globals.glAlertsTo;
                edCTCServer.Text = Globals.glCTCDbServer;
                edCTCDatabase.Text = Globals.glCTCDbInstance;
                edCTCUsername.Text = Globals.glCTCDbUserName;
                edCTCPassword.Text = Globals.glCTCDbPassword;
                edTestLoc.Text = Globals.glTestLocation;
                Globals.Customer Cust = new Globals.Customer();
                edCustomerCode.Text = Globals.glCustCode;
                edCustomerLocation.Text = Globals.glProfilePath;
                edArchiveLocation.Text = Globals.glArcLocation;
                if (string.IsNullOrEmpty(Globals.glFailPath))
                    edFailPath.Text = Path.Combine(Globals.glProfilePath, "Failed");
                else
                    edFailPath.Text = Globals.glFailPath;
                if (string.IsNullOrEmpty(Globals.glLibPath))
                    edLibraryPath.Text = Path.Combine(Globals.glProfilePath, "Lib");
                else
                    edLibraryPath.Text = Globals.glLibPath;
                if (string.IsNullOrEmpty(Globals.glArcLocation))
                    edArchiveLocation.Text = Path.Combine(Globals.glProfilePath, "Archive");
                else
                    edArchiveLocation.Text = Globals.glArcLocation;
                edCustomerCode.Text = Globals.glCustCode;
                edPickupLocation.Text = Globals.glPickupPath;
                edOutputLocation.Text = Globals.glOutputDir;
                sqlConn = new SqlConnection();
                sqlConn.ConnectionString = Globals.connString();
                try
                {
                    if (sqlConn.State == ConnectionState.Open)
                    {
                        sqlConn.Close();
                    }
                    sqlConn.Open();
                    Cust = Globals.GetCustomer(Globals.glCustCode);
                    if (Cust == null && string.IsNullOrEmpty(edCustomerCode.Text) && string.IsNullOrEmpty(edCustomerLocation.Text))
                    {
                        MessageBox.Show("No Customer details record. Please enter Customer Location and Customer Code first.");


                    }
                    else if (Cust == null && !string.IsNullOrEmpty(edCustomerCode.Text) && !string.IsNullOrEmpty(edCustomerLocation.Text))
                    {
                        frmNewCustomer NewCustomer = new frmNewCustomer();
                        DialogResult dr = new DialogResult();
                        dr = NewCustomer.ShowDialog();
                        if (dr == DialogResult.OK)
                        {
                            LoadSettings();
                        }
                    }
                    else
                    {
                        lblCustomerName.Text = Cust.CustomerName;
                        edShortName.Text = Cust.ShortName;
                    }
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("There was an error connecting to the Database.\n Please check the Database settings \n Error was: " + ex.Message, "SQL Database Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    tcSettings.SelectedIndex = 1;
                }



            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while loading Settings. Error was: \n" + ex.Message, "Error Loading Settings", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void GetCustomer()
        {

        }

        private void CreateProfilePaths(string path)
        {
            try
            {


                if (Directory.Exists(path))
                {
                    String rootPath = Path.Combine(path);
                    // Directory.CreateDirectory(rootPath);
                    Directory.CreateDirectory(Path.Combine(rootPath, "Processing"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Held"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Archive"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Lib"));
                }
                else
                {
                    Directory.CreateDirectory(path);
                    String rootPath = Path.Combine(path);
                    Directory.CreateDirectory(Path.Combine(rootPath, "Processing"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Held"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Archive"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Lib"));
                }
            }
            catch (UnauthorizedAccessException exAuth)
            {
                MessageBox.Show("You do not have access to create a folder here.Please Check", "Folder Write Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            CreateProfilePaths(edCustomerLocation.Text);
            XmlDocument xmlConfig = new XmlDocument();
            xmlConfig.Load(Globals.glAppConfig);
            //Configuration Section Database

            XmlNodeList nodelist = xmlConfig.SelectNodes("/Satellite/Database");
            XmlNode xmlCatalog = nodelist[0].SelectSingleNode("Catalog");
            XmlNode xnode;
            if (xmlCatalog == null)
            {
                xnode = xmlConfig.CreateElement("Catalog");
                xnode.InnerText = edInstance.Text;
                Globals.glDbInstance = edInstance.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                Globals.glDbInstance = edInstance.Text;
                xmlCatalog.InnerText = edInstance.Text;
            }
            XmlNode xmlServer = nodelist[0].SelectSingleNode("Servername");
            if (xmlServer == null)
            {
                xnode = xmlConfig.CreateElement("Servername");
                xnode.InnerText = edServer.Text;
                Globals.glDbServer = edServer.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlServer.InnerText = edServer.Text;
                Globals.glDbServer = edServer.Text;
            }


            XmlNode xmlUser = nodelist[0].SelectSingleNode("Username");
            if (xmlUser == null)
            {
                xnode = xmlConfig.CreateElement("Username");
                xnode.InnerText = edUserName.Text;
                Globals.glDbUserName = edUserName.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlUser.InnerText = edUserName.Text;
                Globals.glDbUserName = edUserName.Text;
            }


            XmlNode xmlPassword = nodelist[0].SelectSingleNode("Password");
            if (xmlUser == null)
            {
                xnode = xmlConfig.CreateElement("Password");
                xnode.InnerText = edPassword.Text;
                Globals.glDbPassword = edPassword.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                Globals.glDbPassword = edPassword.Text;
                xmlPassword.InnerText = edPassword.Text;
            }
            nodelist = null;
            nodelist = xmlConfig.SelectNodes("/Satellite/CTCNode");
            if (nodelist.Count == 0)
            {
                XmlNode xRoot = xmlConfig.SelectSingleNode("/Satellite");
                xnode = xmlConfig.CreateElement("CTCNode");
                xRoot.AppendChild(xnode);
                nodelist = xmlConfig.SelectNodes("/Satellite/CTCNode");
            }
            xmlCatalog = null;
            xmlCatalog = nodelist[0].SelectSingleNode("Catalog");

            xnode = null;
            if (xmlCatalog == null)
            {
                xnode = xmlConfig.CreateElement("Catalog");
                xnode.InnerText = edCTCDatabase.Text;
                Globals.glCTCDbInstance = edCTCDatabase.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                Globals.glCTCDbInstance = edCTCDatabase.Text;
                xmlCatalog.InnerText = edCTCDatabase.Text;
            }
            xmlServer = null;
            xmlServer = nodelist[0].SelectSingleNode("Servername");
            if (xmlServer == null)
            {
                xnode = xmlConfig.CreateElement("Servername");
                xnode.InnerText = edCTCServer.Text;
                Globals.glCTCDbServer = edCTCServer.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlServer.InnerText = edCTCServer.Text;
                Globals.glCTCDbServer = edCTCServer.Text;
            }

            xmlUser = null;

            xmlUser = nodelist[0].SelectSingleNode("Username");
            if (xmlUser == null)
            {
                xnode = xmlConfig.CreateElement("Username");
                xnode.InnerText = edCTCUsername.Text;
                Globals.glCTCDbUserName = edCTCUsername.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlUser.InnerText = edCTCUsername.Text;
                Globals.glCTCDbUserName = edCTCUsername.Text;
            }

            xmlPassword = null;
            xmlPassword = nodelist[0].SelectSingleNode("Password");
            if (xmlUser == null)
            {
                xnode = xmlConfig.CreateElement("Password");
                xnode.InnerText = edCTCPassword.Text;
                Globals.glCTCDbPassword = edCTCPassword.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                Globals.glCTCDbPassword = edCTCPassword.Text;
                xmlPassword.InnerText = edCTCPassword.Text;
            }

            //configuration Section Communications
            nodelist = xmlConfig.SelectNodes("/Satellite/Communications");
            XmlNode xmlAlerts = nodelist[0].SelectSingleNode("AlertsTo");
            if (xmlAlerts == null)
            {
                xnode = xmlConfig.CreateElement("AlertsTo");
                xnode.InnerText = edAlertsTo.Text;
                Globals.glAlertsTo = edAlertsTo.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                Globals.glAlertsTo = edAlertsTo.Text;
                xmlAlerts.InnerText = edAlertsTo.Text;
            }

            XmlNode xmlSMTP = nodelist[0].SelectSingleNode("SMTPServer");
            if (xmlSMTP == null)
            {
                xnode = xmlConfig.CreateElement("SMTPServer");
                xnode.InnerText = edMailServer.Text;
                XmlAttribute xPort = xmlConfig.CreateAttribute("Port");
                xPort.InnerXml = edPort.Text;
                SMTPServer.Port = edPort.Text;
                xnode.Attributes.Append(xPort);
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlSMTP.InnerText = edMailServer.Text;
                SMTPServer.Server = edMailServer.Text;
                xmlSMTP.Attributes[0].InnerXml = edPort.Text;
                SMTPServer.Port = edPort.Text;
            }
            xmlSMTP = nodelist[0].SelectSingleNode("EmailAddress");
            if (xmlSMTP == null)
            {
                xnode = xmlConfig.CreateElement("EmailAddress");
                xnode.InnerText = edEmailAddress.Text;
                SMTPServer.Email = edEmailAddress.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlSMTP.InnerText = edEmailAddress.Text;
                SMTPServer.Email = edEmailAddress.Text;
            }
            xmlSMTP = nodelist[0].SelectSingleNode("SMTPUsername");
            if (xmlSMTP == null)
            {
                xnode = xmlConfig.CreateElement("SMTPUsername");
                xnode.InnerText = edMailUsername.Text;
                SMTPServer.Username = edMailUsername.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlSMTP.InnerText = edMailUsername.Text;
                SMTPServer.Username = edMailUsername.Text;
            }
            xmlSMTP = nodelist[0].SelectSingleNode("SMTPPassword");
            if (xmlSMTP == null)
            {
                xnode = xmlConfig.CreateElement("SMTPPassword");
                xnode.InnerText = edMailPassword.Text;
                SMTPServer.Password = edMailPassword.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlSMTP.InnerText = edMailPassword.Text;
                SMTPServer.Password = edMailPassword.Text;
            }

            //Configuration Section Customer

            nodelist = xmlConfig.SelectNodes("/Satellite/Customer");
            if (nodelist[0] == null)
            {
                xnode = xmlConfig.CreateElement("Customer");
                XmlNode xConfig = xmlConfig.SelectSingleNode("/Satellite");
                xConfig.AppendChild(xnode);
                nodelist = xmlConfig.SelectNodes("/Satellite/Customer");
            }

            XmlNode xmlCustomer = nodelist[0].SelectSingleNode("Name");
            if (xmlCustomer == null)
            {
                xnode = xmlConfig.CreateElement("Name");
                xnode.InnerText = lblCustomerName.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
                xmlCustomer.InnerText = lblCustomerName.Text;

            XmlNode xmlCustCode = nodelist[0].SelectSingleNode("Code");
            if (xmlCustCode == null)
            {
                xnode = xmlConfig.CreateElement("Code");
                xnode.InnerText = edCustomerCode.Text;
                Globals.glCustCode = edCustomerCode.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlCustCode.InnerText = edCustomerCode.Text;
                Globals.glCustCode = edCustomerCode.Text;
            }


            XmlNode xmlXMLLoc = nodelist[0].SelectSingleNode("ProfilePath");
            if (xmlXMLLoc == null)
            {
                xnode = xmlConfig.CreateElement("ProfilePath");
                xnode.InnerText = edCustomerLocation.Text;
                Globals.glProfilePath = edCustomerLocation.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                Globals.glProfilePath = edCustomerLocation.Text;
                xmlXMLLoc.InnerText = edCustomerLocation.Text;
            }


            XmlNode xmlReportLoc = nodelist[0].SelectSingleNode("OutputPath");
            if (xmlReportLoc == null)
            {
                xnode = xmlConfig.CreateElement("OutputPath");
                xnode.InnerText = edOutputLocation.Text;
                Globals.glOutputDir = edOutputLocation.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlReportLoc.InnerText = edOutputLocation.Text;
                Globals.glOutputDir = edOutputLocation.Text;
            }


            XmlNode xmlRootPath = nodelist[0].SelectSingleNode("PickupPath");
            if (xmlRootPath == null)
            {
                xnode = xmlConfig.CreateElement("PickupPath");
                xnode.InnerText = edPickupLocation.Text;
                Globals.glPickupPath = edPickupLocation.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                Globals.glPickupPath = edPickupLocation.Text;
                xmlRootPath.InnerText = edPickupLocation.Text;
            }

            XmlNode xmlTestLoc = nodelist[0].SelectSingleNode("TestingLocation");
            if (xmlTestLoc == null)
            {
                xnode = xmlConfig.CreateElement("TestingLocation");
                xnode.InnerText = edTestLoc.Text;
                Globals.glTestLocation = edTestLoc.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                Globals.glTestLocation = edTestLoc.Text;
                xmlTestLoc.InnerText = edTestLoc.Text;
            }

            XmlNode xmlLibPath = nodelist[0].SelectSingleNode("LibraryPath");
            if (xmlLibPath == null)
            {
                xnode = xmlConfig.CreateElement("LibraryPath");
                xnode.InnerText = edLibraryPath.Text;
                Globals.glLibPath = edLibraryPath.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlLibPath.InnerText = edLibraryPath.Text;
                Globals.glLibPath = edLibraryPath.Text;
            }


            XmlNode xmlFailPath = nodelist[0].SelectSingleNode("FailPath");
            if (xmlFailPath == null)
            {
                xnode = xmlConfig.CreateElement("FailPath");
                xnode.InnerText = edFailPath.Text;
                nodelist[0].AppendChild(xnode);
                Globals.glFailPath = edFailPath.Text;
            }
            else
            {
                xmlFailPath.InnerText = edFailPath.Text;
                Globals.glFailPath = edFailPath.Text;
            }


            XmlNode xmlCustomReceivePath = nodelist[0].SelectSingleNode("ArchiveLocation");
            if (xmlCustomReceivePath == null)
            {
                xnode = xmlConfig.CreateElement("ArchiveLocation");
                xnode.InnerText = edArchiveLocation.Text;
                nodelist[0].AppendChild(xnode);
                Globals.glArcLocation = edArchiveLocation.Text;
            }
            else
            {
                Globals.glArcLocation = edArchiveLocation.Text;
                xmlCustomReceivePath.InnerText = edArchiveLocation.Text;
            }



            //XmlNode xmlArcOptions = nodelist[0].SelectSingleNode("ArchiveFrequency");
            //String RbOptions = string.Empty;
            //foreach (RadioButton rb in gbArchive.Controls)
            //{
            //    if (rb.Checked)
            //    {
            //        RbOptions = rb.Text;
            //        break;
            //    }
            //}
            //if (xmlArcOptions == null)
            //{
            //    xnode = xmlConfig.CreateElement("ArchiveFrequency");
            //    xnode.InnerText = RbOptions;
            //    nodelist[0].AppendChild(xnode);
            //}
            //else
            //    xmlArcOptions.InnerText = RbOptions;

            xmlConfig.Save(Globals.glAppConfig);
            MessageBox.Show("CTC Node Settings saved", "CTC Node Configuration", MessageBoxButtons.OK, MessageBoxIcon.Information);
            LoadSettings();
        }

        private void btnLoc_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edCustomerLocation.Text;
            fd.ShowDialog();
            edCustomerLocation.Text = fd.SelectedPath;
        }

        private void bbArcLoc_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edArchiveLocation.Text;
            fd.ShowDialog();
            edArchiveLocation.Text = fd.SelectedPath;
        }

        private void bbLibLoc_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edLibraryPath.Text;
            fd.ShowDialog();
            edLibraryPath.Text = fd.SelectedPath;
        }

        private void bbPickLoc_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edPickupLocation.Text;
            fd.ShowDialog();
            edPickupLocation.Text = fd.SelectedPath;
        }

        private void bbOutLoc_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edOutputLocation.Text;
            fd.ShowDialog();
            edOutputLocation.Text = fd.SelectedPath;
        }

        private void btnCheckDB_Click(object sender, EventArgs e)
        {
            rtbDBCheck.Clear();
            rtbDBCheck.Text = "Data Source=" + edServer.Text + ";Initial Catalog=" + edInstance.Text + ";User ID = " + edUserName.Text + ";Password=" + edPassword.Text + ";" + Environment.NewLine;
            rtbDBCheck.Text += "Testing Connection" + Environment.NewLine;
            SqlConnection sqlconn = new SqlConnection();
            sqlconn.ConnectionString = "Data Source=" + edServer.Text + ";Initial Catalog=" + edInstance.Text + ";User ID = " + edUserName.Text + ";Password=" + edPassword.Text + ";";
            try
            {
                sqlconn.Open();
                rtbDBCheck.Text += " Connected Successfully." + Environment.NewLine;
            }
            catch (SqlException ex)
            {
                rtbDBCheck.Text += "Error found :" + ex.Message + Environment.NewLine;
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bbFailLoc_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edFailPath.Text;
            fd.ShowDialog();
            edFailPath.Text = fd.SelectedPath;
        }

        private void btnLoc_Click_1(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edCustomerLocation.Text;
            fd.ShowDialog();
            edCustomerLocation.Text = fd.SelectedPath;
        }

        private void btnTestLoc_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edTestLoc.Text;
            fd.ShowDialog();
            edTestLoc.Text = fd.SelectedPath;
        }

        private void btnCTCTest_Click(object sender, EventArgs e)
        {
            rtbCTCTest.Clear();
            rtbCTCTest.Text = "Data Source=" + edCTCServer.Text + ";Initial Catalog=" + edCTCDatabase.Text + ";User ID = " + edCTCUsername.Text + ";Password=" + edCTCPassword.Text + ";" + Environment.NewLine;
            rtbCTCTest.Text += "Testing Connection" + Environment.NewLine;
            SqlConnection sqlCTCConn = new SqlConnection();
            sqlCTCConn.ConnectionString = "Data Source=" + edCTCServer.Text + ";Initial Catalog=" + edCTCDatabase.Text + ";User ID = " + edCTCUsername.Text + ";Password=" + edCTCPassword.Text + ";";
            try
            {
                sqlCTCConn.Open();
                rtbCTCTest.Text += " Connected Successfully." + Environment.NewLine;
                sqlCTCConn.Close();
            }
            catch (SqlException ex)
            {
                rtbCTCTest.Text += "Error found :" + ex.Message + Environment.NewLine;
            }
        }
    }
}
