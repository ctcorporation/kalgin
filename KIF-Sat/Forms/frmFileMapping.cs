﻿using KIF_Sat.DTO;
using KIF_Sat.Models;
using KIF_Sat.Resource;
using NodeResources;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace KIF_Sat.Forms
{


    public partial class frmFileMapping : Form
    {
        private Guid _pcID = Guid.Empty;
        private Guid _mapId = Guid.Empty;
        private BindingSource bsMap = new BindingSource();
        public frmFileMapping()
        {
            InitializeComponent();
        }

        private void bbClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmFileMapping_Load(object sender, EventArgs e)
        {
            FillFileDescriptions();
            FillMapCombos("MAPPINGFIELD", cmbMappingType);
            FillMapCombos("MAPPINGOPERATION", cmbMapOperation);
            FillMapCombos("DATATYPE", cmbDataType);
            _pcID = Guid.Empty;
        }
        private void FillMapCombos(string enumValue, ComboBox cmbToFill)
        {
            var enumlist = GetFromEnum(enumValue);
            if (enumlist != null)
            {
                var listsrc = (from x in enumlist
                               select new DataItem { StringVal = x.CW_MAPVALUE.Trim(), Val = x.CW_ENUM.Trim() }).ToList();
                cmbToFill.DataSource = listsrc;
                cmbToFill.DisplayMember = "StringVal";
                cmbToFill.ValueMember = "Val";
            }
        }

        private List<Cargowise_Enums> GetFromEnum(string enumType)
        {
            using (IUnitOfWork uow = new UnitOfWork(new Models.KifEntity(Globals.ConnString.ConnString)))
            {
                var val = uow.Cargowise_Enums.Find(x => x.CW_ENUMTYPE == enumType).ToList();
                if (val.Count == 0)
                {
                    return null;
                }
                val.Insert(0, new Cargowise_Enums { CW_MAPVALUE = "(none selected)", CW_ENUM = "" });
                return val;
            }
        }

        private void FillFileDescriptions()
        {
            using (IUnitOfWork uow = new UnitOfWork(new KifEntity(Globals.ConnString.ConnString)))
            {
                var val = uow.FileDescriptions.GetAll().OrderBy(x => x.PC_FileName).ToList();
                if (val.Count >= 0)
                {
                    val.Insert(0, new FileDescription { PC_FileName = "(none selected)" });
                    cmbDescription.DataSource = val;
                    cmbDescription.DisplayMember = "PC_FileName";
                    cmbDescription.ValueMember = "PC_ID";
                }
            }
        }

        private void bbAddDescr_Click(object sender, EventArgs e)
        {
            using (IUnitOfWork uow = new UnitOfWork(new KifEntity(Globals.ConnString.ConnString)))
            {
                if (_pcID == Guid.Empty)
                {
                    _pcID = Guid.NewGuid();
                    var fd = new FileDescription
                    {
                        PC_FileName = edFileDescription.Text,
                        PC_Delimiter = cbDelimit.Checked,
                        PC_HasHeader = cbHeader.Checked,
                        PC_Quotations = cbQuotations.Checked,
                        PC_DataStartFrom = int.Parse(edDataStartFrom.Text),
                        PC_HeaderStartFrom = int.Parse(edHeaderStart.Text),
                        PC_FieldCount = int.Parse(edFieldCount.Text),
                        PC_FileExtension = edFileExtn.Text,
                        PC_FirstFieldName = edFirstField.Text,
                        PC_LastFieldName = edLastFieldName.Text,
                        PC_Id = _pcID
                    };
                    uow.FileDescriptions.Add(fd);
                }
                else
                {
                    var val = uow.FileDescriptions.Get(_pcID);
                    if (val != null)
                    {
                        val.PC_FileName = edFileDescription.Text;
                        val.PC_Delimiter = cbDelimit.Checked;
                        val.PC_HasHeader = cbHeader.Checked;
                        val.PC_Quotations = cbQuotations.Checked;
                        val.PC_DataStartFrom = int.Parse(edDataStartFrom.Text);
                        val.PC_HeaderStartFrom = int.Parse(edHeaderStart.Text);
                        val.PC_FieldCount = int.Parse(edFieldCount.Text);
                        val.PC_FileExtension = edFileExtn.Text;
                        val.PC_FirstFieldName = edFirstField.Text;
                        val.PC_LastFieldName = edLastFieldName.Text;
                    }

                }
                var success = uow.Complete();
                if (success > 0)
                {
                    MessageBox.Show("Changes saved to the File Description", "Add/Update File Description", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FillMapGrid(_pcID);
                    FillFileDescriptions();
                }
            }


        }

        private void bbAddMap_Click(object sender, EventArgs e)
        {
            using (IUnitOfWork uow = new UnitOfWork(new KifEntity(Globals.ConnString.ConnString)))
            {
                if (_mapId == Guid.Empty)
                {
                    var newMap = new MappingEnum
                    {
                        M_Description = edFileDescription.Text,
                        M_From = cmbFieldList.Text != "(none selected)" ? cmbFieldList.Text : edMapFrom.Text,
                        M_To = edMapTo.Text,
                        M_MapType = cmbMappingType.Text.Trim(),
                        M_Operation = cmbMapOperation.Text.Trim(),
                        M_PC = _pcID
                    };
                    var datItem = (DataItem)cmbDataType.SelectedItem;
                    newMap.M_FieldType = datItem.Val;
                    uow.MappingEnums.Add(newMap);
                }
                else
                {
                    var newMap = uow.MappingEnums.Get(_mapId);
                    newMap.M_Description = edFileDescription.Text;
                    newMap.M_FieldType = (string)cmbDataType.SelectedValue;
                    newMap.M_From = cmbFieldList.Text != "(none selected)" ? cmbFieldList.Text : edMapFrom.Text;
                    newMap.M_To = edMapTo.Text;
                    newMap.M_MapType = cmbMappingType.Text.Trim();
                    newMap.M_Operation = cmbMapOperation.Text.Trim();
                    newMap.M_PC = _pcID;
                }
                if (uow.Complete() > 0)
                {
                    MessageBox.Show("Mapping Added", "Add/Update Mapping", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FillMapGrid(_pcID);
                    bbAddMap.Text = "&Add";
                    _mapId = Guid.Empty;
                }

            }

        }

        private void FillMapGrid(Guid pcid)
        {
            grdMapping.AutoGenerateColumns = false;

            using (IUnitOfWork uow = new UnitOfWork(new KifEntity(Globals.ConnString.ConnString)))
            {
                var maps = uow.MappingEnums.Find(x => x.M_PC == pcid).OrderBy(x => x.M_MapType).ToList();

                bsMap.DataSource = new SortableBindingList<MappingEnum>(maps);
                grdMapping.DataSource = bsMap;


            }
        }

        private void cmbDescription_DropDownClosed(object sender, EventArgs e)
        {
            FillGrid();
        }

        private void FillGrid()
        {
            _pcID = cmbDescription.SelectedValue != null ? (Guid)cmbDescription.SelectedValue : Guid.Empty;
            GetFileDescription(_pcID);
            FillMapGrid(_pcID);

        }

        private void GetFileDescription(Guid pcID)
        {
            if (pcID == Guid.Empty)
            {
                _pcID = Guid.Empty;
                edFileDescription.Text = string.Empty;
                edFirstField.Text = string.Empty;
                edLastFieldName.Text = string.Empty;
                edFileExtn.Text = string.Empty;
                edFieldCount.Text = string.Empty;
                edHeaderStart.Text = string.Empty;
                edDataStartFrom.Text = string.Empty;
                cbDelimit.Checked = false;
                cbHeader.Checked = false;
                cbQuotations.Checked = false;
                return;
            }

            using (IUnitOfWork uow = new UnitOfWork(new KifEntity(Globals.ConnString.ConnString)))
            {
                var fd = uow.FileDescriptions.Get(pcID);
                edFileDescription.Text = fd.PC_FileName;
                edFirstField.Text = fd.PC_FirstFieldName;
                edLastFieldName.Text = fd.PC_LastFieldName;
                edFileExtn.Text = fd.PC_FileExtension;
                edFieldCount.Text = fd.PC_FieldCount.ToString();
                edHeaderStart.Text = fd.PC_HeaderStartFrom.ToString();
                edDataStartFrom.Text = fd.PC_DataStartFrom.ToString();
                cbDelimit.Checked = fd.PC_Delimiter;
                cbHeader.Checked = fd.PC_HasHeader;
                cbQuotations.Checked = fd.PC_Quotations;
            }
        }

        private void deleteMapToolStripMenuItem_Click(object sender, EventArgs e)
        {

            List<Guid> listToDelete = new List<Guid>();
            foreach (DataGridViewRow row in grdMapping.SelectedRows)
            {
                listToDelete.Add((Guid)(row.Cells["M_ID"].Value));
            }
            if (listToDelete.Count > 0)
            {
                DeleteMaps(listToDelete);
            }
        }


        private void DeleteMaps(List<Guid> maps)
        {
            using (IUnitOfWork uow = new UnitOfWork(new KifEntity(Globals.ConnString.ConnString)))
            {
                foreach (var g in maps)
                {
                    uow.MappingEnums.Remove(x => x.M_Id == g);
                }
                if (uow.Complete() > 0)
                {
                    MessageBox.Show("Record(s) deleted", "Remove Mapping Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FillMapGrid(_pcID);
                }

            }
        }

        private void grdMapping_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                GetMapData((Guid)grdMapping.Rows[e.RowIndex].Cells["M_ID"].Value);
                bbAddMap.Text = "&Update";
            }
        }

        private void GetMapData(Guid value)
        {
            using (IUnitOfWork uow = new UnitOfWork(new KifEntity(Globals.ConnString.ConnString)))
            {
                var val = uow.MappingEnums.Get(value);
                if (val == null)
                {
                    return;
                }
                _mapId = value;
                //    edMapDesc.Text = val.M_Description.Trim();
                edMapFrom.Text = val.M_From.Trim();
                edMapTo.Text = val.M_To;
                cmbDataType.SelectedValue = val.M_FieldType.Trim();
                cmbMapOperation.SelectedValue = val.M_Operation.Trim();
                cmbMappingType.SelectedValue = val.M_MapType.TrimEnd();


            }
        }

        private void bbNew_Click(object sender, EventArgs e)
        {
            _mapId = Guid.Empty;
            cmbDataType.SelectedIndex = 0;
            cmbMapOperation.SelectedIndex = 0;
            cmbMappingType.SelectedIndex = 0;
            //   edMapDesc.Text = "";
            edMapFrom.Text = "";
            edMapTo.Text = "";

        }

        private void grdMapping_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            Guid id = Guid.Empty;
            if (Guid.TryParse(e.Row.Cells[5].Value.ToString().Trim(), out id))
            {
                DialogResult dr = MessageBox.Show("Are you sure you wish to Delete this mapping?", "Field Mapping Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    using (IUnitOfWork uow = new UnitOfWork(new KifEntity(Globals.ConnString.ConnString)))
                    {
                        var map = uow.FileDescriptions.Get(id);
                        if (map != null)
                        {
                            uow.FileDescriptions.Remove(map);
                            uow.Complete();
                            FillGrid();
                        }
                    }
                }
            }


        }

        private void grdMapping_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void bbNewMapping_Click(object sender, EventArgs e)
        {
            ClearHeader();
        }

        private void ClearHeader()
        {
            edFileDescription.Text = string.Empty;
            edFileExtn.Text = string.Empty;
            edFieldCount.Text = "0";
            edFirstField.Text = "(Field Name)";
            edLastFieldName.Text = "(Field Name)";
            edHeaderStart.Text = "0";
            edDataStartFrom.Text = "0";
            cbDelimit.Checked = false;
            cbHeader.Checked = false;
            cbQuotations.Checked = false;
            cmbDescription.SelectedIndex = 0;
        }

        private void btnEvalFile_Click(object sender, EventArgs e)
        {
            EvaluateFile();
        }

        private void EvaluateFile()
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.ShowDialog();
            try
            {
                FileInfo filetoOpen = new FileInfo(fd.FileName);
                edFileExtn.Text = filetoOpen.Extension;
                switch (filetoOpen.Extension.ToUpper())
                {
                    case ".XLS":
                        EvalXLS(filetoOpen.FullName);
                        break;
                    case ".XLSX":
                        EvalXLS(filetoOpen.FullName);
                        break;
                    case ".TXT": break;
                    case ".CSV": break;
                    case ".XML": break;
                    default:
                        MessageBox.Show("Unable to determine the File Type. Please fill in details manually",
                    "Unknown File Extension", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;

                }

            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;


            }
        }
        private void EvalXLS(string fileName)
        {
            var tb = ExcelToTable.ToTable(fileName);
            if (tb != null)
            {
                edFieldCount.Text = tb.Columns.Count.ToString();
                edFirstField.Text = tb.Columns[0].ColumnName;
                edLastFieldName.Text = tb.Columns[tb.Columns.Count - 1].ColumnName;
                cbHeader.Checked = true;
            }

            var fieldList = (from x in tb.Columns.Cast<DataColumn>()
                             orderby x.ColumnName
                             select x.ColumnName).ToList();
            if (fieldList.Count > 0)
            {
                fieldList.Insert(0, "(none selected)");
            }
            BindingSource bsList = new BindingSource();
            bsList.DataSource = fieldList;
            cmbFieldList.DataSource = bsList;



        }


    }



    class DataItem
    {
        public string StringVal { get; set; }
        public string Val { get; set; }
    }
}
