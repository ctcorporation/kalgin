﻿using KIF_Sat.Models;
using NodeResources;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace KIF_Sat
{
    public partial class FrmResend : Form
    {
        public IConnectionManager connMgr;

        public FrmResend()
        {
            InitializeComponent();
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            using (KifEntity db = new KifEntity(connMgr.ConnString))
            {
                var list = (from a in db.Transactions
                            where a.T_REF1 == edFind.Text
                            select a).ToList();

                if (list.Count > 0)
                {
                    grdResend.AutoGenerateColumns = false;
                    grdResend.DataSource = list;


                }
                else
                {
                    MessageBox.Show("Order Number Not Found.", "Find Order", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private void FrmResend_Load(object sender, EventArgs e)
        {
            connMgr = new ConnectionManager();


        }

        private void cMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int iResendOrders = 0;
            int iMissingOrders = 0;
            foreach (DataGridViewRow row in grdResend.SelectedRows)
            {
                if (string.IsNullOrEmpty(row.Cells["T_ARCHIVE"].Value.ToString()))
                {
                    string strResend = Path.Combine(Globals.glFailPath, row.Cells["T_FILENAME"].Value.ToString().Trim());
                    if (File.Exists(Path.Combine(strResend)))
                    {
                        iResendOrders++;
                        NodeResource.MoveFile(strResend, Globals.glPickupPath);

                    }
                    else
                    {
                        iMissingOrders++;
                    }
                }
                else
                {
                    using (ArcOps archiveOperations = new ArcOps())
                    {
                        string strExtract = archiveOperations.ExtractFile(Path.Combine(Globals.glArcLocation, row.Cells["T_ARCHIVE"].Value.ToString().Trim()), row.Cells["T_FILENAME"].Value.ToString().Trim(), Globals.glPickupPath);
                        if (strExtract.Contains("Warning:"))
                        {
                            MessageBox.Show(strExtract);
                        }
                        else
                        {
                            iResendOrders++;
                        }
                    }

                }


            }
            MessageBox.Show(iResendOrders + " Orders Resent and " + iMissingOrders + " that were either not found or have already been sent");
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }
    }
}
