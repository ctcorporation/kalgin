﻿namespace KIF_Sat
{
    partial class FrmResend
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.edFind = new System.Windows.Forms.TextBox();
            this.btnFind = new System.Windows.Forms.Button();
            this.grdResend = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.T_REF1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.T_DATETIME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.T_REF2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.T_FILENAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.T_ARCHIVE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grdResend)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(569, 282);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Order Number";
            // 
            // edFind
            // 
            this.edFind.Location = new System.Drawing.Point(104, 16);
            this.edFind.Name = "edFind";
            this.edFind.Size = new System.Drawing.Size(111, 20);
            this.edFind.TabIndex = 2;
            // 
            // btnFind
            // 
            this.btnFind.Location = new System.Drawing.Point(239, 14);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(75, 23);
            this.btnFind.TabIndex = 3;
            this.btnFind.Text = "&Find";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // grdResend
            // 
            this.grdResend.AllowUserToAddRows = false;
            this.grdResend.AllowUserToDeleteRows = false;
            this.grdResend.AllowUserToResizeRows = false;
            this.grdResend.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdResend.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdResend.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdResend.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.T_REF1,
            this.T_DATETIME,
            this.T_REF2,
            this.T_FILENAME,
            this.T_ARCHIVE});
            this.grdResend.ContextMenuStrip = this.contextMenuStrip1;
            this.grdResend.Location = new System.Drawing.Point(15, 54);
            this.grdResend.Name = "grdResend";
            this.grdResend.ReadOnly = true;
            this.grdResend.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdResend.Size = new System.Drawing.Size(634, 215);
            this.grdResend.TabIndex = 4;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cMenuToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(123, 28);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // cMenuToolStripMenuItem
            // 
            this.cMenuToolStripMenuItem.Name = "cMenuToolStripMenuItem";
            this.cMenuToolStripMenuItem.Size = new System.Drawing.Size(192, 24);
            this.cMenuToolStripMenuItem.Text = "Resend";
            this.cMenuToolStripMenuItem.Click += new System.EventHandler(this.cMenuToolStripMenuItem_Click);
            // 
            // T_REF1
            // 
            this.T_REF1.DataPropertyName = "T_REF1";
            this.T_REF1.HeaderText = "Order Number";
            this.T_REF1.Name = "T_REF1";
            this.T_REF1.ReadOnly = true;
            // 
            // T_DATETIME
            // 
            this.T_DATETIME.DataPropertyName = "T_DATETIME";
            this.T_DATETIME.HeaderText = "Date Created";
            this.T_DATETIME.Name = "T_DATETIME";
            this.T_DATETIME.ReadOnly = true;
            // 
            // T_REF2
            // 
            this.T_REF2.DataPropertyName = "T_REF2";
            this.T_REF2.HeaderText = "Deliver To";
            this.T_REF2.Name = "T_REF2";
            this.T_REF2.ReadOnly = true;
            // 
            // T_FILENAME
            // 
            this.T_FILENAME.DataPropertyName = "T_FILENAME";
            this.T_FILENAME.HeaderText = "File Name";
            this.T_FILENAME.Name = "T_FILENAME";
            this.T_FILENAME.ReadOnly = true;
            // 
            // T_ARCHIVE
            // 
            this.T_ARCHIVE.DataPropertyName = "T_ARCHIVE";
            this.T_ARCHIVE.HeaderText = "Archive Location";
            this.T_ARCHIVE.Name = "T_ARCHIVE";
            this.T_ARCHIVE.ReadOnly = true;
            // 
            // FrmResend
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(656, 317);
            this.Controls.Add(this.grdResend);
            this.Controls.Add(this.btnFind);
            this.Controls.Add(this.edFind);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClose);
            this.Name = "FrmResend";
            this.Text = "Re-send Order ";
            this.Load += new System.EventHandler(this.FrmResend_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdResend)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox edFind;
        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.DataGridView grdResend;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cMenuToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn T_REF1;
        private System.Windows.Forms.DataGridViewTextBoxColumn T_DATETIME;
        private System.Windows.Forms.DataGridViewTextBoxColumn T_REF2;
        private System.Windows.Forms.DataGridViewTextBoxColumn T_FILENAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn T_ARCHIVE;
    }
}