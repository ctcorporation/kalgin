﻿using KIF_Sat.DTO;
using KIF_Sat.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace KIF_Sat
{
    public partial class frmEnums : Form
    {

        public Guid cw_ID;
        public frmEnums()
        {
            InitializeComponent();
        }

        private void frmEnums_Load(object sender, EventArgs e)
        {


            LoadData();
        }


        private void LoadData()
        {
            using (IUnitOfWork uow = new UnitOfWork(new KifEntity(Globals.ConnString.ConnString)))
            {
                var enumsList = uow.Cargowise_Enums.GetAll().OrderBy(x => x.CW_ENUMTYPE).ToList();
                dgEnums.DataSource = enumsList;
                dgEnums.AllowUserToDeleteRows = true;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(edCargowiseValue.Text.Trim()) || string.IsNullOrEmpty(edCustomValue.Text.Trim()) || string.IsNullOrEmpty(edDatatype.Text.Trim()))
            {
                return;
            }

            using (IUnitOfWork uow = new UnitOfWork(new KifEntity(Globals.ConnString.ConnString)))
            {
                Cargowise_Enums newEnum;
                if (cw_ID == null)
                {
                    newEnum = uow.Cargowise_Enums.Find(x => x.CW_ENUMTYPE == edDatatype.Text && x.CW_MAPVALUE == edCustomValue.Text).FirstOrDefault();
                    if (newEnum == null)
                    {
                        newEnum = new Cargowise_Enums
                        {
                            CW_ENUM = edCargowiseValue.Text,
                            CW_ENUMTYPE = edDatatype.Text,
                            CW_MAPVALUE = edCustomValue.Text
                        };
                    }
                }
                else
                {
                    newEnum = uow.Cargowise_Enums.Get(cw_ID);
                    if (newEnum != null)
                    {
                        newEnum.CW_ENUM = edCargowiseValue.Text;
                        newEnum.CW_ENUMTYPE = edDatatype.Text;
                        newEnum.CW_MAPVALUE = edCustomValue.Text;
                    }
                }
                if (uow.Complete() > 0)
                {
                    LoadData();
                }

            }
        }

        private void dgEnums_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                cw_ID = (Guid)dgEnums[0, e.RowIndex].Value;
                edCargowiseValue.Text = dgEnums["CWValue", e.RowIndex].Value.ToString();
                edCustomValue.Text = dgEnums["Custom", e.RowIndex].Value.ToString();
                edDatatype.Text = dgEnums["Type", e.RowIndex].Value.ToString();
            }

        }

        private void bbNew_Click(object sender, EventArgs e)
        {
            edDatatype.Text = "";
            edCustomValue.Text = "";
            edCargowiseValue.Text = "";
            cw_ID = Guid.Empty;
            edDatatype.Focus();
        }

        private void edDatatype_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                if (this.ActiveControl != null)
                {
                    this.SelectNextControl(this.ActiveControl, true, true, true, true);
                }
                e.Handled = true;
            }
        }

        private void dgEnums_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {


        }

        private void deleteRowsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<Guid> listToDelete = new List<Guid>();
            foreach (DataGridViewRow row in dgEnums.SelectedRows)
            {
                listToDelete.Add((Guid)(row.Cells["ID"].Value));
            }
            if (listToDelete.Count > 0)
            {
                DeleteEnums(listToDelete);
            }
        }

        private void DeleteEnums(List<Guid> listToDelete)
        {
            using (IUnitOfWork uow = new UnitOfWork(new KifEntity(Globals.ConnString.ConnString)))
            {
                foreach (var g in listToDelete)
                {
                    uow.Cargowise_Enums.Remove(x => x.CW_ID == g);
                }
                if (uow.Complete() > 0)
                {
                    MessageBox.Show("Record(s) deleted", "Remove Mapping Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LoadData();
                }

            }
        }

        private void dgEnums_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            using (IUnitOfWork uow = new UnitOfWork(new KifEntity(Globals.ConnString.ConnString)))
            {
                uow.Cargowise_Enums.Remove(x => x.CW_ID == (Guid)e.Row.Cells[0].Value);
                if (uow.Complete() > 0)
                {
                    LoadData();
                }
            }
        }


    }
}
