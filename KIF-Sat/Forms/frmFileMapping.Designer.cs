﻿namespace KIF_Sat.Forms
{
    partial class frmFileMapping
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bbClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.edFileDescription = new System.Windows.Forms.TextBox();
            this.cbHeader = new System.Windows.Forms.CheckBox();
            this.cbDelimit = new System.Windows.Forms.CheckBox();
            this.edFieldCount = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbQuotations = new System.Windows.Forms.CheckBox();
            this.edHeaderStart = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.edDataStartFrom = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.edFirstField = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.edLastFieldName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.edFileExtn = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.bbAddDescr = new System.Windows.Forms.Button();
            this.cmbDescription = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.bbNew = new System.Windows.Forms.Button();
            this.cmbDataType = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.bbAddMap = new System.Windows.Forms.Button();
            this.cmbMapOperation = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.edMapTo = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.edMapFrom = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cmbMappingType = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.grdMapping = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.M_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cxMapping = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bbNewMapping = new System.Windows.Forms.Button();
            this.btnEvalFile = new System.Windows.Forms.Button();
            this.cmbFieldList = new Syncfusion.Windows.Forms.Tools.ComboBoxAdv();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMapping)).BeginInit();
            this.cxMapping.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbFieldList)).BeginInit();
            this.SuspendLayout();
            // 
            // bbClose
            // 
            this.bbClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bbClose.Location = new System.Drawing.Point(713, 438);
            this.bbClose.Name = "bbClose";
            this.bbClose.Size = new System.Drawing.Size(75, 23);
            this.bbClose.TabIndex = 0;
            this.bbClose.Text = "&Close";
            this.bbClose.UseVisualStyleBackColor = true;
            this.bbClose.Click += new System.EventHandler(this.bbClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "File Name/Description";
            // 
            // edFileDescription
            // 
            this.edFileDescription.Location = new System.Drawing.Point(146, 51);
            this.edFileDescription.Name = "edFileDescription";
            this.edFileDescription.Size = new System.Drawing.Size(420, 20);
            this.edFileDescription.TabIndex = 2;
            // 
            // cbHeader
            // 
            this.cbHeader.AutoSize = true;
            this.cbHeader.Location = new System.Drawing.Point(31, 85);
            this.cbHeader.Name = "cbHeader";
            this.cbHeader.Size = new System.Drawing.Size(127, 17);
            this.cbHeader.TabIndex = 3;
            this.cbHeader.Text = "File Has Header Row";
            this.cbHeader.UseVisualStyleBackColor = true;
            // 
            // cbDelimit
            // 
            this.cbDelimit.AutoSize = true;
            this.cbDelimit.Location = new System.Drawing.Point(31, 108);
            this.cbDelimit.Name = "cbDelimit";
            this.cbDelimit.Size = new System.Drawing.Size(96, 17);
            this.cbDelimit.TabIndex = 4;
            this.cbDelimit.Text = "File is delimited";
            this.cbDelimit.UseVisualStyleBackColor = true;
            // 
            // edFieldCount
            // 
            this.edFieldCount.Location = new System.Drawing.Point(244, 85);
            this.edFieldCount.Name = "edFieldCount";
            this.edFieldCount.Size = new System.Drawing.Size(29, 20);
            this.edFieldCount.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(178, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Field Count";
            // 
            // cbQuotations
            // 
            this.cbQuotations.AutoSize = true;
            this.cbQuotations.Location = new System.Drawing.Point(31, 131);
            this.cbQuotations.Name = "cbQuotations";
            this.cbQuotations.Size = new System.Drawing.Size(121, 17);
            this.cbQuotations.TabIndex = 7;
            this.cbQuotations.Text = "Quotations are used";
            this.cbQuotations.UseVisualStyleBackColor = true;
            // 
            // edHeaderStart
            // 
            this.edHeaderStart.Location = new System.Drawing.Point(302, 109);
            this.edHeaderStart.Name = "edHeaderStart";
            this.edHeaderStart.Size = new System.Drawing.Size(29, 20);
            this.edHeaderStart.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(178, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Header Starts from Line";
            // 
            // edDataStartFrom
            // 
            this.edDataStartFrom.Location = new System.Drawing.Point(302, 132);
            this.edDataStartFrom.Name = "edDataStartFrom";
            this.edDataStartFrom.Size = new System.Drawing.Size(29, 20);
            this.edDataStartFrom.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(178, 135);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Data starts from Line";
            // 
            // edFirstField
            // 
            this.edFirstField.Location = new System.Drawing.Point(443, 83);
            this.edFirstField.Name = "edFirstField";
            this.edFirstField.Size = new System.Drawing.Size(274, 20);
            this.edFirstField.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(355, 86);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "First Field Name";
            // 
            // edLastFieldName
            // 
            this.edLastFieldName.Location = new System.Drawing.Point(443, 109);
            this.edLastFieldName.Name = "edLastFieldName";
            this.edLastFieldName.Size = new System.Drawing.Size(274, 20);
            this.edLastFieldName.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(355, 112);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Last Field Name";
            // 
            // edFileExtn
            // 
            this.edFileExtn.Location = new System.Drawing.Point(443, 136);
            this.edFileExtn.Name = "edFileExtn";
            this.edFileExtn.Size = new System.Drawing.Size(77, 20);
            this.edFileExtn.TabIndex = 17;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(355, 139);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "File Extension";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(31, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(109, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "File Desciption/Name";
            // 
            // bbAddDescr
            // 
            this.bbAddDescr.Location = new System.Drawing.Point(678, 144);
            this.bbAddDescr.Name = "bbAddDescr";
            this.bbAddDescr.Size = new System.Drawing.Size(107, 23);
            this.bbAddDescr.TabIndex = 19;
            this.bbAddDescr.Text = "Save Mapping";
            this.bbAddDescr.UseVisualStyleBackColor = true;
            this.bbAddDescr.Click += new System.EventHandler(this.bbAddDescr_Click);
            // 
            // cmbDescription
            // 
            this.cmbDescription.FormattingEnabled = true;
            this.cmbDescription.Location = new System.Drawing.Point(172, 18);
            this.cmbDescription.Name = "cmbDescription";
            this.cmbDescription.Size = new System.Drawing.Size(266, 21);
            this.cmbDescription.TabIndex = 20;
            this.cmbDescription.DropDownClosed += new System.EventHandler(this.cmbDescription_DropDownClosed);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.cmbFieldList);
            this.panel1.Controls.Add(this.bbNew);
            this.panel1.Controls.Add(this.cmbDataType);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.bbAddMap);
            this.panel1.Controls.Add(this.cmbMapOperation);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.edMapTo);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.edMapFrom);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.cmbMappingType);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.grdMapping);
            this.panel1.Location = new System.Drawing.Point(12, 173);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(776, 259);
            this.panel1.TabIndex = 21;
            // 
            // bbNew
            // 
            this.bbNew.Location = new System.Drawing.Point(605, 222);
            this.bbNew.Name = "bbNew";
            this.bbNew.Size = new System.Drawing.Size(75, 23);
            this.bbNew.TabIndex = 27;
            this.bbNew.Text = "&New";
            this.bbNew.UseVisualStyleBackColor = true;
            this.bbNew.Click += new System.EventHandler(this.bbNew_Click);
            // 
            // cmbDataType
            // 
            this.cmbDataType.FormattingEnabled = true;
            this.cmbDataType.Location = new System.Drawing.Point(639, 171);
            this.cmbDataType.Name = "cmbDataType";
            this.cmbDataType.Size = new System.Drawing.Size(95, 21);
            this.cmbDataType.TabIndex = 26;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(576, 175);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(57, 13);
            this.label14.TabIndex = 25;
            this.label14.Text = "Data Type";
            // 
            // bbAddMap
            // 
            this.bbAddMap.Location = new System.Drawing.Point(686, 222);
            this.bbAddMap.Name = "bbAddMap";
            this.bbAddMap.Size = new System.Drawing.Size(75, 23);
            this.bbAddMap.TabIndex = 24;
            this.bbAddMap.Text = "&Add";
            this.bbAddMap.UseVisualStyleBackColor = true;
            this.bbAddMap.Click += new System.EventHandler(this.bbAddMap_Click);
            // 
            // cmbMapOperation
            // 
            this.cmbMapOperation.FormattingEnabled = true;
            this.cmbMapOperation.Location = new System.Drawing.Point(124, 198);
            this.cmbMapOperation.Name = "cmbMapOperation";
            this.cmbMapOperation.Size = new System.Drawing.Size(121, 21);
            this.cmbMapOperation.TabIndex = 8;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(10, 201);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 13);
            this.label12.TabIndex = 7;
            this.label12.Text = "Map Operation";
            // 
            // edMapTo
            // 
            this.edMapTo.Location = new System.Drawing.Point(346, 225);
            this.edMapTo.Name = "edMapTo";
            this.edMapTo.Size = new System.Drawing.Size(222, 20);
            this.edMapTo.TabIndex = 6;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(253, 228);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "Map Field TO";
            // 
            // edMapFrom
            // 
            this.edMapFrom.Location = new System.Drawing.Point(346, 172);
            this.edMapFrom.Name = "edMapFrom";
            this.edMapFrom.Size = new System.Drawing.Size(222, 20);
            this.edMapFrom.TabIndex = 4;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(253, 175);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "Map Field FROM";
            // 
            // cmbMappingType
            // 
            this.cmbMappingType.FormattingEnabled = true;
            this.cmbMappingType.Location = new System.Drawing.Point(124, 171);
            this.cmbMappingType.Name = "cmbMappingType";
            this.cmbMappingType.Size = new System.Drawing.Size(121, 21);
            this.cmbMappingType.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 174);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(108, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Select Mapping Type";
            // 
            // grdMapping
            // 
            this.grdMapping.AllowUserToAddRows = false;
            this.grdMapping.AllowUserToResizeRows = false;
            this.grdMapping.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdMapping.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.M_ID});
            this.grdMapping.ContextMenuStrip = this.cxMapping;
            this.grdMapping.Dock = System.Windows.Forms.DockStyle.Top;
            this.grdMapping.Location = new System.Drawing.Point(0, 0);
            this.grdMapping.Name = "grdMapping";
            this.grdMapping.ReadOnly = true;
            this.grdMapping.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdMapping.Size = new System.Drawing.Size(776, 166);
            this.grdMapping.TabIndex = 0;
            this.grdMapping.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdMapping_CellDoubleClick);
            this.grdMapping.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.grdMapping_ColumnHeaderMouseClick);
            this.grdMapping.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.grdMapping_UserDeletingRow);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "M_MapType";
            this.Column1.HeaderText = "Map type";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 200;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "M_From";
            this.Column2.HeaderText = "Map From";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 200;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "M_To";
            this.Column3.HeaderText = "Map To";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 200;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "M_Operation";
            this.Column4.HeaderText = "Mapping Operation";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 200;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "M_FieldType";
            this.Column5.HeaderText = "Field Data Type";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // M_ID
            // 
            this.M_ID.DataPropertyName = "M_ID";
            this.M_ID.HeaderText = "M_ID";
            this.M_ID.Name = "M_ID";
            this.M_ID.ReadOnly = true;
            this.M_ID.Visible = false;
            // 
            // cxMapping
            // 
            this.cxMapping.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.cxMapping.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteMapToolStripMenuItem,
            this.copyMapToolStripMenuItem});
            this.cxMapping.Name = "cxMapping";
            this.cxMapping.Size = new System.Drawing.Size(135, 48);
            // 
            // deleteMapToolStripMenuItem
            // 
            this.deleteMapToolStripMenuItem.Name = "deleteMapToolStripMenuItem";
            this.deleteMapToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.deleteMapToolStripMenuItem.Text = "&Delete Map";
            this.deleteMapToolStripMenuItem.Click += new System.EventHandler(this.deleteMapToolStripMenuItem_Click);
            // 
            // copyMapToolStripMenuItem
            // 
            this.copyMapToolStripMenuItem.Name = "copyMapToolStripMenuItem";
            this.copyMapToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.copyMapToolStripMenuItem.Text = "&Copy Map";
            // 
            // bbNewMapping
            // 
            this.bbNewMapping.Location = new System.Drawing.Point(445, 16);
            this.bbNewMapping.Name = "bbNewMapping";
            this.bbNewMapping.Size = new System.Drawing.Size(108, 23);
            this.bbNewMapping.TabIndex = 22;
            this.bbNewMapping.Text = "&New File Mapping";
            this.bbNewMapping.UseVisualStyleBackColor = true;
            this.bbNewMapping.Click += new System.EventHandler(this.bbNewMapping_Click);
            // 
            // btnEvalFile
            // 
            this.btnEvalFile.Location = new System.Drawing.Point(587, 49);
            this.btnEvalFile.Name = "btnEvalFile";
            this.btnEvalFile.Size = new System.Drawing.Size(75, 23);
            this.btnEvalFile.TabIndex = 23;
            this.btnEvalFile.Text = "&Eval File";
            this.btnEvalFile.UseVisualStyleBackColor = true;
            this.btnEvalFile.Click += new System.EventHandler(this.btnEvalFile_Click);
            // 
            // cmbFieldList
            // 
            this.cmbFieldList.BeforeTouchSize = new System.Drawing.Size(222, 21);
            this.cmbFieldList.Location = new System.Drawing.Point(346, 198);
            this.cmbFieldList.Name = "cmbFieldList";
            this.cmbFieldList.Size = new System.Drawing.Size(222, 21);
            this.cmbFieldList.TabIndex = 28;
            this.cmbFieldList.Text = "(none selected)";
            // 
            // frmFileMapping
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 473);
            this.Controls.Add(this.btnEvalFile);
            this.Controls.Add(this.bbNewMapping);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.cmbDescription);
            this.Controls.Add(this.bbAddDescr);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.edFileExtn);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.edLastFieldName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.edFirstField);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.edDataStartFrom);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.edHeaderStart);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbQuotations);
            this.Controls.Add(this.edFieldCount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbDelimit);
            this.Controls.Add(this.cbHeader);
            this.Controls.Add(this.edFileDescription);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bbClose);
            this.Name = "frmFileMapping";
            this.Text = "File Mapping Templates";
            this.Load += new System.EventHandler(this.frmFileMapping_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMapping)).EndInit();
            this.cxMapping.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbFieldList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bbClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox edFileDescription;
        private System.Windows.Forms.CheckBox cbHeader;
        private System.Windows.Forms.CheckBox cbDelimit;
        private System.Windows.Forms.TextBox edFieldCount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox cbQuotations;
        private System.Windows.Forms.TextBox edHeaderStart;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox edDataStartFrom;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox edFirstField;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox edLastFieldName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox edFileExtn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button bbAddDescr;
        private System.Windows.Forms.ComboBox cmbDescription;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView grdMapping;
        private System.Windows.Forms.TextBox edMapTo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox edMapFrom;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbMappingType;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button bbAddMap;
        private System.Windows.Forms.ComboBox cmbMapOperation;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cmbDataType;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ContextMenuStrip cxMapping;
        private System.Windows.Forms.ToolStripMenuItem deleteMapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyMapToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn M_ID;
        private System.Windows.Forms.Button bbNew;
        private System.Windows.Forms.Button bbNewMapping;
        private System.Windows.Forms.Button btnEvalFile;
        private Syncfusion.Windows.Forms.Tools.ComboBoxAdv cmbFieldList;
    }
}