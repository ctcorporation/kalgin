﻿using NodeResources;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace KIF_Sat
{
    public partial class frmProfiles : Form
    {
        public SqlConnection sqlConn;
        public Guid r_id;
        public Guid gP_id;
        public SqlDataAdapter daProfile;

        public frmProfiles()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DeliveryChecked(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            String rbt = rb.Tag.ToString();
            switch (rbt)
            {
                case "pickup":
                    lblReceivePassword.Visible = false;
                    edReceiveUsername.Visible = false;
                    edReceivePassword.Visible = false;
                    lblReceiveUsername.Visible = false;
                    lblServerAddress.Visible = false;
                    edServerAddress.Visible = false;
                    lblFTPReceivePort.Visible = false;
                    edFTPReceivePort.Visible = false;
                    edFTPReceivePath.Visible = false;
                    lblFTPReceivePath.Visible = false;


                    break;
                case "eadapter":
                    lblReceivePassword.Visible = true;
                    edReceiveUsername.Visible = true;
                    edReceivePassword.Visible = true;
                    lblReceiveUsername.Visible = true;
                    lblServerAddress.Visible = true;
                    lblServerAddress.Text = "eAdapter URL";
                    edServerAddress.Visible = true;
                    lblFTPReceivePort.Visible = false;
                    edFTPReceivePort.Visible = false;
                    edFTPReceivePath.Visible = false;
                    lblFTPReceivePath.Visible = false;
                    break;
                case "ftp":
                    lblReceivePassword.Visible = true;
                    edReceiveUsername.Visible = true;
                    edReceivePassword.Visible = true;
                    lblReceiveUsername.Visible = true;
                    lblServerAddress.Visible = true;
                    lblServerAddress.Text = "Server Address";
                    edServerAddress.Visible = true;
                    lblFTPReceivePort.Visible = true;
                    edFTPReceivePort.Visible = true;
                    edFTPReceivePath.Visible = true;
                    lblFTPReceivePath.Visible = true;

                    break;
                case "email":
                    lblReceivePassword.Visible = false;
                    edReceiveUsername.Visible = false;
                    edReceivePassword.Visible = false;
                    lblReceiveUsername.Visible = false;
                    lblServerAddress.Visible = true;
                    edServerAddress.Visible = true;
                    lblServerAddress.Text = "Email Address";
                    lblFTPReceivePort.Visible = false;
                    edFTPReceivePort.Visible = false;
                    edFTPReceivePath.Visible = false;
                    lblFTPReceivePath.Visible = false;
                    break;
                default:
                    lblReceivePassword.Visible = true;
                    edReceiveUsername.Visible = true;
                    edReceivePassword.Visible = true;
                    lblReceiveUsername.Visible = true;
                    lblServerAddress.Visible = true;
                    lblServerAddress.Text = "Server Address";
                    edServerAddress.Visible = true;
                    lblFTPReceivePort.Visible = true;
                    edFTPReceivePort.Visible = true;
                    edFTPReceivePath.Visible = false;
                    lblFTPReceivePath.Visible = false;
                    break;
            }
        }


        private void frmProfiles_Load(object sender, EventArgs e)
        {

            sqlConn = new SqlConnection { ConnectionString = Globals.connString() };
            try
            {
                sqlConn.Open();
                sqlConn.Close();
            }
            catch (SqlException ex)
            {
                sqlConn.ConnectionString = Globals.connString();

            }
            r_id = Globals.gl_CustId;
            if (r_id == Guid.Empty)
            {
                frmNewCustomer NewCustomer = new frmNewCustomer();
                if (NewCustomer.ShowDialog(this) == DialogResult.OK)
                {
                    frmProfiles_Load(sender, e);
                }
            }
            else
            {
                FillGrid(r_id);
                ddlDTSType.DisplayMember = "Text";
                ddlDTSType.ValueMember = "Value";

                var items = new[] {
            new { Text = "Replace", Value = "R" },
            new { Text = "Update", Value = "U" },
            new { Text = "Delete", Value = "D" },
            new { Text = "Modify", Value = "M" }};
                ddlDTSType.DataSource = items;
            }



        }

        private void FillGrid(Guid C_id)
        {
            if (C_id != Guid.Empty)
            {
                SqlDataAdapter daProfile = new SqlDataAdapter("SELECT P_ID, P_C, P_REASONCODE, C_ID,P_RECIPIENTID, P_SENDERID, P_DESCRIPTION, P_DIRECTION, P_ACTIVE from VW_CustomerProfile where C_ID = @C_ID", sqlConn);
                // daProfile.SelectCommand.CommandText = "SELECT C_NAME, C_CODE, C_IS_ACTIVE, C_ON_HOLD, C_PATH, C_FTP_CLIENT, P_ID, P_C, P_REASONCODE, P_SERVER, P_USERNAME, P_PASSWORD, P_RECEIVEONLY, C_ID,P_RECIPIENTID, P_DESCRIPTION, P_PORT";
                // daProfile.SelectCommand.Connection = sqlConn;
                daProfile.SelectCommand.Parameters.AddWithValue("@C_ID", r_id);
                DataSet dsProfile = new DataSet();
                daProfile.Fill(dsProfile, "CUSTOMERPROFILE");
                dgProfile.AutoGenerateColumns = false;
                dgProfile.DataSource = dsProfile;
                dgProfile.DataMember = "CUSTOMERPROFILE";
            }
        }

        private void rbCustomer_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSend.Checked)
            {
                gbReceiveOptions.Enabled = false;
                gbSendOptions.Enabled = true;
            }
            else
            {
                gbReceiveOptions.Enabled = true;
                gbSendOptions.Enabled = false;
            }
        }

        private void rbVendor_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSend.Checked)
            {
                gbReceiveOptions.Enabled = false;
                gbSendOptions.Enabled = true;
            }
            else
            {
                gbReceiveOptions.Enabled = true;
                gbSendOptions.Enabled = false;
            }
        }

        private void ReadOnlycontrols(Boolean toggle)
        {
            if (!toggle)
            {

                cbDTS.Enabled = true;

            }
            else
            {

                cbDTS.Enabled = false;
            }
        }

        void SetupReceive()
        {
            String strReceive = String.Empty;
            foreach (RadioButton rb in gbReceiveMethod.Controls)
            {
                if (rb.Checked)
                {
                    switch (rb.Name)
                    {


                        case "rbPickup":
                            lblServerAddress.Visible = false;
                            edServerAddress.Visible = false;
                            lblFTPReceivePort.Visible = false;
                            edFTPReceivePort.Visible = false;
                            lblFTPReceivePath.Visible = false;
                            edFTPReceivePath.Visible = false;
                            lblReceiveUsername.Visible = false;
                            edReceiveUsername.Visible = false;
                            lblReceivePassword.Visible = false;
                            edReceivePassword.Visible = false;
                            lblReceiveEmailAddress.Visible = false;
                            edReceiveEmailAddress.Visible = false;
                            btnReceiveFolder.Visible = false;
                            edReceiveCustomerName.Visible = true;
                            lblReceiveCustomerName.Visible = true;
                            cbSSL.Visible = false;

                            break;
                        case "rbReceiveFTP":
                            lblServerAddress.Visible = true;
                            edServerAddress.Visible = true;
                            lblFTPReceivePort.Visible = true;
                            edFTPReceivePort.Visible = true;
                            lblFTPReceivePath.Visible = true;
                            edFTPReceivePath.Visible = true;
                            lblReceiveUsername.Visible = true;
                            edReceiveUsername.Visible = true;
                            lblReceivePassword.Visible = true;
                            edReceivePassword.Visible = true;
                            lblReceiveEmailAddress.Visible = false;
                            edReceiveEmailAddress.Visible = false;
                            btnReceiveFolder.Visible = false;
                            edReceiveCustomerName.Visible = true;
                            lblReceiveCustomerName.Visible = true;
                            cbSSL.Visible = false;
                            break;

                        case "rbEmail":
                            lblServerAddress.Visible = true;
                            edServerAddress.Visible = true;
                            lblFTPReceivePort.Visible = true;
                            edFTPReceivePort.Visible = true;
                            lblFTPReceivePath.Visible = false;
                            edFTPReceivePath.Visible = false;
                            lblReceiveUsername.Visible = true;
                            edReceiveUsername.Visible = true;
                            lblReceivePassword.Visible = true;
                            edReceivePassword.Visible = true;
                            btnReceiveFolder.Visible = false;
                            lblReceiveEmailAddress.Visible = true;
                            edReceiveEmailAddress.Visible = true;
                            lblReceiveCustomerName.Visible = true;
                            edReceiveCustomerName.Visible = true;

                            cbSSL.Visible = true;
                            break;
                        case "rbReceivePickup":
                            lblServerAddress.Visible = false;
                            edServerAddress.Visible = false;
                            lblFTPReceivePort.Visible = false;
                            edFTPReceivePort.Visible = false;
                            lblFTPReceivePath.Visible = true;
                            edFTPReceivePath.Visible = true;
                            btnReceiveFolder.Visible = true;
                            lblReceiveUsername.Visible = false;
                            edReceiveUsername.Visible = false;
                            lblReceivePassword.Visible = false;
                            edReceivePassword.Visible = false;
                            lblReceiveEmailAddress.Visible = false;
                            edReceiveEmailAddress.Visible = false;
                            edReceiveCustomerName.Visible = true;
                            lblReceiveCustomerName.Visible = true;
                            cbSSL.Visible = false;
                            break;

                    }
                }
            }
        }

        void SetupSend()
        {
            String strSend = String.Empty;

            foreach (RadioButton rb in gbSendMethod.Controls)
            {

                if (rb.Checked == true)
                {

                    switch (rb.Name)
                    {
                        case "rbPickupSend":
                            lblSendServer.Visible = true;
                            lblSendServer.Text = "Pickup Folder";
                            edSendCTCUrl.Visible = true;
                            lblSendUsername.Visible = false;
                            edSendUsername.Visible = false;
                            lblSendPassword.Visible = false;
                            edSendPassword.Visible = false;
                            lblSendPath.Visible = false;
                            edSendFolder.Visible = false;
                            btnPickupFolder.Visible = true;
                            lblTestResults.Visible = false;
                            edSendeAdapterResults.Visible = false;
                            btnTestSendeAdapter.Visible = false;
                            lblSendEmailAddress.Visible = false;
                            edSendEmailAddress.Visible = false;
                            edSendFtpPort.Visible = false;
                            lblSendFTPPort.Visible = false;
                            lblSendSubject.Visible = false;
                            edSendSubject.Visible = false;
                            break;
                        case "rbCTCSend":
                            lblSendServer.Visible = true;
                            lblSendServer.Text = "Server Address";
                            edSendCTCUrl.Visible = true;
                            lblSendUsername.Visible = true;
                            edSendUsername.Visible = true;
                            lblSendPassword.Visible = true;
                            edSendPassword.Visible = true;
                            lblSendPath.Visible = false;
                            edSendFolder.Visible = false;
                            lblTestResults.Visible = true;
                            btnPickupFolder.Visible = false;
                            edSendeAdapterResults.Visible = true;
                            btnTestSendeAdapter.Visible = true;
                            lblSendEmailAddress.Visible = false;
                            edSendEmailAddress.Visible = false;
                            lblSendSubject.Visible = false;
                            edSendSubject.Visible = false;
                            edSendFtpPort.Visible = false;
                            lblSendFTPPort.Visible = false;
                            break;
                        case "rbSoapSend":
                            lblSendServer.Visible = true;
                            edSendCTCUrl.Visible = true;
                            lblSendServer.Text = "Server Address";
                            lblSendUsername.Visible = true;
                            edSendUsername.Visible = true;
                            lblSendPassword.Visible = true;
                            edSendPassword.Visible = true;
                            lblSendPath.Visible = false;
                            edSendFolder.Visible = false;
                            lblTestResults.Visible = true;
                            btnPickupFolder.Visible = false;
                            edSendeAdapterResults.Visible = true;
                            btnTestSendeAdapter.Visible = true;
                            lblSendEmailAddress.Visible = false;
                            edSendEmailAddress.Visible = false;
                            lblSendSubject.Visible = true;
                            lblSendSubject.Text = "Endpoint";
                            edSendSubject.Visible = false;
                            edSendFtpPort.Visible = false;
                            lblSendFTPPort.Visible = false;
                            break;
                        case "rbFTPSend":
                            lblSendServer.Visible = true;
                            edSendCTCUrl.Visible = true;
                            lblSendServer.Text = "Server Address";
                            lblSendUsername.Visible = true;
                            edSendUsername.Visible = true;
                            lblSendPassword.Visible = true;
                            edSendPassword.Visible = true;
                            lblSendPath.Visible = true;
                            edSendFolder.Visible = true;
                            btnPickupFolder.Visible = false;
                            lblTestResults.Visible = true;
                            edSendeAdapterResults.Visible = true;
                            btnTestSendeAdapter.Visible = true;
                            lblSendEmailAddress.Visible = false;
                            edSendEmailAddress.Visible = false;
                            lblSendSubject.Visible = false;
                            edSendSubject.Visible = false;
                            lblSendFTPPort.Visible = true;
                            break;
                        case "rbEmailSend":
                            lblSendServer.Visible = false;
                            edSendCTCUrl.Visible = false;
                            lblSendServer.Text = "Server Address";
                            lblSendUsername.Visible = false;
                            edSendUsername.Visible = false;
                            lblSendPassword.Visible = false;
                            edSendPassword.Visible = false;
                            lblSendPath.Visible = false;
                            edSendFolder.Visible = false;
                            btnPickupFolder.Visible = false;
                            lblTestResults.Visible = false;
                            edSendeAdapterResults.Visible = false;
                            btnTestSendeAdapter.Visible = false;
                            lblSendEmailAddress.Visible = true;
                            edSendEmailAddress.Visible = true;
                            lblSendSubject.Visible = true;
                            edSendSubject.Visible = true;
                            lblSendSubject.Text = "Subject";
                            edSendFtpPort.Visible = false;
                            lblSendFTPPort.Visible = false;
                            break;
                        case "rbFTPSendDirect":
                            lblSendServer.Visible = true;
                            edSendCTCUrl.Visible = true;
                            lblSendServer.Text = "Server Address";
                            lblSendUsername.Visible = true;
                            edSendUsername.Visible = true;
                            lblSendPassword.Visible = true;
                            edSendPassword.Visible = true;
                            lblSendPath.Visible = true;
                            edSendFolder.Visible = true;
                            btnPickupFolder.Visible = false;
                            lblTestResults.Visible = true;
                            edSendeAdapterResults.Visible = true;
                            btnTestSendeAdapter.Visible = true;
                            lblSendEmailAddress.Visible = false;
                            edSendEmailAddress.Visible = false;
                            lblSendSubject.Visible = false;
                            edSendSubject.Visible = false;
                            edSendFtpPort.Visible = true;
                            lblSendFTPPort.Visible = true;
                            break;
                    }
                }

            }
        }

        private void FillDTSGrid(Guid pid)
        {
            try
            {
                SqlDataAdapter daDTS = new SqlDataAdapter("SELECT D_ID, D_INDEX, D_C, D_P, D_DTS, D_DTSTYPE,D_FILETYPE, D_SEARCHPATTERN, D_NEWVALUE, D_CURRENTVALUE, D_QUALIFIER, D_TARGET FROM DTS where D_P = @D_P ORDER BY D_INDEX", sqlConn);
                daDTS.SelectCommand.Parameters.AddWithValue("@D_P", pid);
                dgDts.AutoGenerateColumns = false;
                DataSet dsDTS = new DataSet();
                daDTS.Fill(dsDTS, "DTS");
                dgDts.DataSource = dsDTS;
                dgDts.DataMember = "DTS";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Found Accessing DB. Error was: " + ex.Message);
            }
        }

        void FillCWContext()
        {
            try
            {
                SqlDataAdapter daContext = new SqlDataAdapter("SELECT CC_ID, CC_Context from CargowiseContext order by CC_Context", sqlConn);
                DataSet dsContext = new DataSet();
                daContext.Fill(dsContext, "CONTEXT");
                DataRow dr = dsContext.Tables["CONTEXT"].NewRow();
                dr["DC_NAME"] = "(none selected)";
                dsContext.Tables["CONTEXT"].Rows.InsertAt(dr, 0);
                ddlCWContext.DataSource = dsContext.Tables["Context"];
                ddlCWContext.DisplayMember = "CC_Context";
                ddlCWContext.ValueMember = "CC_ID";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Found Accessing DB. Error was: " + ex.Message);
            }
        }

        private void ClearControls(bool allControls)
        {
            if (allControls)
            {

            }
            tvwXSD.ResetText();
            edReceiveCustomerName.Text = "";
            edDescription.Text = "";
            edFTPReceivePort.Text = "";
            edReasonCode.Text = "";
            edRecipientID.Text = "";
            edReceivePassword.Text = "";
            edReceiveUsername.Text = "";
            edServerAddress.Text = "";
            btnAdd.Text = "&Add >>";
            edDTSCurrentValue.Text = "";
            rbReceive.Checked = false;
            rbSend.Checked = false;
            edReasonCode.Text = "";
            edEventCode.Text = "";
            cmbMsgType.SelectedIndex = -1;
            cbChargeable.Checked = false;
            cbGroupCharges.Checked = false;
            edServerAddress.Text = "";
            edFTPReceivePort.Text = "";
            edFTPReceivePath.Text = "";
            btnXsdPath.Text = "";
            edReceiveSubject.Text = "";
            edReceiveEmailAddress.Text = "";
            edReceiveSender.Text = "";
            edReceiveSubject.Text = "";
            edXSDPath.Text = "";
            edCustomMethod.Text = "";
            lbParameters.Items.Clear();
            edLibraryName.Text = "";
            edParameter.Text = "";
        }

        public void ConvertXMLNodeToTreeNode(XmlNode xmlNode, TreeNodeCollection treeNodes)
        {
            TreeNode newTreeNode = treeNodes.Add(xmlNode.Name);
            switch (xmlNode.NodeType)
            {
                case XmlNodeType.ProcessingInstruction:
                case XmlNodeType.XmlDeclaration:
                    newTreeNode.Text = "<?" + xmlNode.Name + " " +
                      xmlNode.Value + "?>";
                    break;
                case XmlNodeType.Element:
                    newTreeNode.Text = "<" + xmlNode.Name + ">";
                    break;
                case XmlNodeType.Attribute:
                    newTreeNode.Text = "atrribute: " + xmlNode.Name;
                    break;
                case XmlNodeType.Text:
                case XmlNodeType.CDATA:
                    newTreeNode.Text = xmlNode.Value;
                    break;
                case XmlNodeType.Comment:
                    newTreeNode.Text = "<!--" + xmlNode.Value + "-->";
                    break;

            }
            if (xmlNode.Attributes != null)
            {
                foreach (XmlAttribute attrib in xmlNode.Attributes)
                {
                    ConvertXMLNodeToTreeNode(attrib, newTreeNode.Nodes);
                }
            }
            if (xmlNode.HasChildNodes)
            {
                foreach (XmlNode childNode in xmlNode.ChildNodes)
                {
                    ConvertXMLNodeToTreeNode(childNode, newTreeNode.Nodes);
                }
            }


        }

        private void ClearProfile()
        {
            ClearControls(false);
        }

        public void loadXSDTree()
        {
            tvwXSD.Nodes.Clear();
            XmlDocument doc = new XmlDocument();
            string xsdPath = Path.Combine(Globals.glLibPath, edXSDPath.Text);
            try
            {
                doc.Load(xsdPath);

            }
            catch (Exception err)
            {

            }
            ConvertXMLNodeToTreeNode(doc, tvwXSD.Nodes);
            tvwXSD.Nodes[0].ExpandAll();
        }

        private void rbPickup_CheckedChanged(object sender, EventArgs e)
        {
            SetupReceive();
        }

        private void rbPickupSend_CheckedChanged(object sender, EventArgs e)
        {
            SetupSend();
        }

        private void btnXsdPath_Click(object sender, EventArgs e)
        {
            OpenFileDialog fs = new OpenFileDialog();
            try
            {
                fs.DefaultExt = "*.XSD";
                fs.FileName = edXSDPath.Text;
            }
            catch (Exception ex)
            { }
            fs.ShowDialog();
            edXSDPath.Text = fs.FileName;
        }

        private void btnLoadXSD_Click(object sender, EventArgs e)
        {
            loadXSDTree();
        }

        private void bbAddDTS_Click(object sender, EventArgs e)
        {
            SqlCommand execAdd = new SqlCommand("Add_DTS", sqlConn);
            execAdd.CommandType = CommandType.StoredProcedure;
            try
            {
                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();
                }
                sqlConn.Open();
                execAdd.Parameters.AddWithValue("@D_P", gP_id);
                execAdd.Parameters.AddWithValue("@D_INDEX", int.Parse(edDTSIndex.Text));
                execAdd.Parameters.AddWithValue("@D_C", r_id);
                execAdd.Parameters.AddWithValue("@D_FILETYPE", ddlDTSFileType.Text);
                execAdd.Parameters.AddWithValue("@D_DTSTYPE", ddlDTSType.SelectedValue);
                if (rbDTSRecord.Checked)
                    execAdd.Parameters.AddWithValue("@D_DTS", "R");
                if (rbDTSStructure.Checked)
                    execAdd.Parameters.AddWithValue("@D_DTS", "S");

                execAdd.Parameters.AddWithValue("@D_SEARCHPATTERN", edDTSSearch.Text);
                execAdd.Parameters.AddWithValue("@D_NEWVALUE", edDTSValue.Text);
                execAdd.Parameters.AddWithValue("@D_QUALIFIER", edDTSQual.Text);
                execAdd.Parameters.AddWithValue("@D_TARGET", edDTSTarget.Text);
                execAdd.Parameters.AddWithValue("@D_CURRENTVALUE", edDTSCurrentValue.Text);
                execAdd.ExecuteNonQuery();
                sqlConn.Close();
                FillDTSGrid(gP_id);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Saving Record:" + ex.Message);
            }
        }

        private void btnPickupFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fdPickup = new FolderBrowserDialog();
            fdPickup.SelectedPath = edSendCTCUrl.Text;
            fdPickup.ShowDialog();
            edSendCTCUrl.Text = fdPickup.SelectedPath;
        }

        private void btnReceiveFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edFTPReceivePath.Text;
            fd.ShowDialog();
            edFTPReceivePath.Text = fd.SelectedPath;
        }

        private void bbTestRx_Click(object sender, EventArgs e)
        {
            Boolean ssl;
            if (cbSSL.Checked)
                ssl = true;
            else
                ssl = false;
            txtTestResults.Text = "";
            using (MailModule mail = new MailModule(Globals.glMailServerSettings, "csr@ctcorp.com.au", Globals.glTestLocation))
            {
                txtTestResults.Text = mail.CheckMail();
            }

        }

        private void btnNewProfile_Click(object sender, EventArgs e)
        {
            ClearProfile();
            btnAdd.Text = "Add  >>";
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            SqlCommand execAdd = new SqlCommand();
            execAdd.Connection = sqlConn;
            if (btnAdd.Text == "&Update >>")
            {
                execAdd.CommandText = "Edit_Profile";
                SqlParameter PID = execAdd.Parameters.Add("@P_ID", SqlDbType.UniqueIdentifier);
                PID.Value = gP_id;
            }
            else
            {
                execAdd.CommandText = "Add_Profile";
            }
            execAdd.CommandType = CommandType.StoredProcedure;
            SqlParameter PC = execAdd.Parameters.Add("@P_C", SqlDbType.UniqueIdentifier);
            PC.Value = this.r_id;
            SqlParameter PReasonCode = execAdd.Parameters.Add("@P_REASONCODE", SqlDbType.Char, 3);
            PReasonCode.Value = edReasonCode.Text.ToUpper();
            SqlParameter PEventCode = execAdd.Parameters.Add("@P_EVENTCODE", SqlDbType.Char, 3);
            PEventCode.Value = edEventCode.Text.ToUpper();
            SqlParameter PDTS = execAdd.Parameters.Add("@P_DTS", SqlDbType.Char, 1);
            if (cbDTS.Checked)
            {
                PDTS.Value = "Y";

            }
            else
            {
                PDTS.Value = "N";
            }

            SqlParameter PDirection = execAdd.Parameters.Add("@P_DIRECTION", SqlDbType.Char, 1);
            string sDelivery = String.Empty;
            SqlParameter customerCompanyName = execAdd.Parameters.Add("@P_CUSTOMERCOMPANYNAME", SqlDbType.VarChar, 50);
            if (rbSend.Checked)
            {
                PDirection.Value = 'S';
                SqlParameter PServer = execAdd.Parameters.Add("@P_SERVER", SqlDbType.VarChar, 200);
                PServer.Value = edSendCTCUrl.Text;
                SqlParameter PUsername = execAdd.Parameters.Add("@P_USERNAME", SqlDbType.VarChar, 50);
                PUsername.Value = edSendUsername.Text;
                SqlParameter PPassword = execAdd.Parameters.Add("@P_PASSWORD", SqlDbType.VarChar, 50);
                PPassword.Value = edSendPassword.Text;
                SqlParameter sendSubject = execAdd.Parameters.Add("@P_SUBJECT", SqlDbType.VarChar, 100);
                sendSubject.Value = edSendSubject.Text;
                SqlParameter sendtoemailAddress = execAdd.Parameters.Add("@P_EMAILADDRESS", SqlDbType.VarChar, 100);
                sendtoemailAddress.Value = edSendEmailAddress.Text;

                foreach (RadioButton rb in gbSendMethod.Controls)
                {
                    if (rb.Checked)
                    {
                        switch (rb.Name)
                        {

                            case "rbPickupSend":
                                {
                                    sDelivery = "R";
                                    break;
                                }
                            case "rbCTCSend":
                                {
                                    sDelivery = "C";
                                    break;
                                }
                            case "rbFTPSend":
                                {
                                    sDelivery = "F";
                                    break;
                                }
                            case "rbFTPSendDirect":
                                {
                                    sDelivery = "D";
                                    break;
                                }
                            case "rbEmailSend":
                                {
                                    sDelivery = "E";
                                    break;
                                }
                            case "rbSoapSend":
                                {
                                    sDelivery = "S";
                                    break;
                                }
                        }
                    }
                }
            }
            else
            {
                PDirection.Value = 'R';
                SqlParameter PServer = execAdd.Parameters.Add("@P_SERVER", SqlDbType.VarChar, 50);
                PServer.Value = edServerAddress.Text;
                SqlParameter PUsername = execAdd.Parameters.Add("@P_USERNAME", SqlDbType.VarChar, 50);
                PUsername.Value = edReceiveUsername.Text;
                SqlParameter PPath = execAdd.Parameters.Add("@P_PATH", SqlDbType.VarChar, 100);
                PPath.Value = edFTPReceivePath.Text;
                SqlParameter PPassword = execAdd.Parameters.Add("@P_PASSWORD", SqlDbType.VarChar, 50);
                PPassword.Value = edReceivePassword.Text;
                foreach (RadioButton rb in gbReceiveMethod.Controls)
                {
                    if (rb.Checked)
                    {
                        switch (rb.Name)
                        {

                            case "rbPickup":
                                {
                                    sDelivery = "R";

                                    break;
                                }
                            case "rbSendCTC":
                                {
                                    sDelivery = "C";
                                    break;
                                }
                            case "rbSendFTP":
                                {
                                    sDelivery = "F";
                                    break;
                                }
                            case "rbEmail":
                                {
                                    sDelivery = "E";
                                    break;
                                }
                            case "rbReceivePickup":
                                {
                                    sDelivery = "P";
                                    break;
                                }
                        }
                    }
                }
                SqlParameter emailAddress = execAdd.Parameters.Add("@P_EMAILADDRESS", SqlDbType.VarChar, 100);
                emailAddress.Value = edReceiveEmailAddress.Text;
                SqlParameter receiveSubject = execAdd.Parameters.Add("@P_SUBJECT", SqlDbType.VarChar, 100);
                receiveSubject.Value = edReceiveSubject.Text;
                customerCompanyName.Value = edReceiveCustomerName.Text;
            }
            SqlParameter pDelivery = execAdd.Parameters.Add("@P_DELIVERY", SqlDbType.Char, 1);
            pDelivery.Value = sDelivery;
            SqlParameter PPort = execAdd.Parameters.Add("@P_PORT", SqlDbType.Char, 10);
            PPort.Value = edFTPReceivePort.Text;
            SqlParameter PDescription = execAdd.Parameters.Add("@P_DESCRIPTION", SqlDbType.VarChar, 100);
            PDescription.Value = edDescription.Text;
            SqlParameter PRecipientId = execAdd.Parameters.Add("@P_RECIPIENTID", SqlDbType.VarChar, 10);
            PRecipientId.Value = edRecipientID.Text;
            SqlParameter Senderid = execAdd.Parameters.Add("@P_SENDERID", SqlDbType.VarChar, 15);
            Senderid.Value = edSenderID.Text;
            SqlParameter p_id = execAdd.Parameters.Add("@R_ID", SqlDbType.UniqueIdentifier);
            p_id.Direction = ParameterDirection.Output;
            SqlParameter r_Action = execAdd.Parameters.Add("@R_ACTION", SqlDbType.Char, 1);
            r_Action.Direction = ParameterDirection.Output;
            SqlParameter p_Chargeable = execAdd.Parameters.Add("@CHARGEABLE", SqlDbType.Char, 1);
            if (cbChargeable.Checked)
                p_Chargeable.Value = "Y";
            else
                p_Chargeable.Value = "N";
            SqlParameter p_Billto = execAdd.Parameters.Add("@BILLTO", SqlDbType.Char, 15);
            if (rbCustomer.Checked)
            {
                p_Billto.Value = Globals.glCustCode;
            }
            if (rbVendor.Checked)
            {
                p_Billto.Value = edRecipientID.Text;
            }
            SqlParameter Msg_type = execAdd.Parameters.Add("@MSGTYPE", SqlDbType.Char, 20);
            Msg_type.Value = cmbMsgType.Text;

            SqlParameter fileType = execAdd.Parameters.Add("@P_FILETYPE", SqlDbType.Char, 3);
            fileType.Value = cmbFileType.Text;
            SqlParameter ssl = execAdd.Parameters.Add("@P_SSL", SqlDbType.Char, 1);
            if (cbSSL.Checked)
                ssl.Value = "Y";
            else
                ssl.Value = "N";
            SqlParameter customMethod = execAdd.Parameters.Add("@P_METHOD", SqlDbType.VarChar, 50);
            customMethod.Value = edCustomMethod.Text;



            SqlParameter groupCharges = execAdd.Parameters.Add("@P_GROUPCHARGES", SqlDbType.Char, 1);
            if (cbGroupCharges.Checked)
            {
                groupCharges.Value = 'Y';
            }
            else
                groupCharges.Value = 'N';

            SqlParameter rxSenderEmail = execAdd.Parameters.Add("@P_SENDEREMAIL", SqlDbType.VarChar, 100);
            rxSenderEmail.Value = edReceiveSender.Text;
            SqlParameter xsdFile = execAdd.Parameters.Add("@P_XSD", SqlDbType.VarChar, 50);
            if (!String.IsNullOrEmpty(edXSDPath.Text.Trim()))
            {
                bool filecopied = false;
                string fileName = Path.GetFileNameWithoutExtension(edXSDPath.Text);
                int i = 1;
                while (!filecopied)
                    try
                    {

                        {
                            if (File.Exists(edXSDPath.Text))
                            {
                                File.Copy(edXSDPath.Text, Path.Combine(Globals.glProfilePath, "Lib", fileName + Path.GetExtension(edXSDPath.Text)));
                                filecopied = true;
                                xsdFile.Value = fileName + Path.GetExtension(edXSDPath.Text);
                            }
                            else
                            {
                                filecopied = true;
                                xsdFile.Value = edXSDPath.Text;
                            }
                        }

                    }
                    catch (IOException ex)
                    {
                        i++;
                        fileName = fileName + i;

                    }



            }
            SqlParameter libName = execAdd.Parameters.Add("@P_LIBNAME", SqlDbType.VarChar, 50);
            if (!String.IsNullOrEmpty(edLibraryName.Text.Trim()))
            {
                libName.Value = edLibraryName.Text;
            }

            SqlParameter profileactive = execAdd.Parameters.Add("@P_ACTIVE", SqlDbType.Char, 1);
            if (cbProfileActive.Checked)
                profileactive.Value = "Y";
            else
                profileactive.Value = "N";

            try
            {
                sqlConn.Open();
                execAdd.ExecuteNonQuery();
                sqlConn.Close();

                FillGrid(this.r_id);
                if (rbPickup.Checked)
                {
                    //CreateProfPath(Path.Combine(edRootPath.Text + "\\Processing"), edRecipientID.Text);
                }
                if (execAdd.Parameters["@R_ACTION"].Value.ToString() == "U")
                {
                    MessageBox.Show("Customer Profile Updated", "Customer Profiles", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                else
                {
                    MessageBox.Show("Customer Profile Updated. Rule Created", "Customer Profiles", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Saving Record : " + ex.Message);
            }
            edDescription.Text = "";
            edReasonCode.Text = "";
            edRecipientID.Text = "";
            rbPickup.Checked = true;
            edFTPReceivePort.Text = "";
            edServerAddress.Text = "";
            edReceivePassword.Text = "";
            edReceivePassword.Text = "";
            btnAdd.Text = "&Add  >>";
            try
            {
                sqlConn.Close();
            }
            catch (Exception ex)
            {

            }
        }



        private void btnAddParam_Click(object sender, EventArgs e)
        {
            lbParameters.Items.Add(edParameter.Text);
        }

        private void dgProfile_CellContextMenuStripNeeded(object sender, DataGridViewCellContextMenuStripNeededEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            if (e.RowIndex == -1 || e.ColumnIndex == -1)
                return;
            bool isActive = true;

            foreach (DataGridViewRow row in dgv.SelectedRows)
            {
                if ((string)row.Cells["Active"].Value == "Y")
                    isActive = true;
                if ((string)row.Cells["Active"].Value == "N")
                    isActive = false;
            }
            if (isActive)
                markInactiveToolStripMenuItem.Text = "Mark InActive";
            else
                markInactiveToolStripMenuItem.Text = "Mark Active";
        }

        public void GetCustomerProfile(int dgRow)
        {
            SqlCommand sqlProfile = new SqlCommand("SELECT P_C, P_REASONCODE, P_SERVER, P_USERNAME, P_PASSWORD, P_DELIVERY, P_PATH,  P_PORT, P_XSD, P_LIBNAME, P_DESCRIPTION, P_RECIPIENTID, P_MSGTYPE,  " +
                                                    "P_DIRECTION, P_SENDERID, P_DTS, P_ACTIVE, P_FILETYPE, P_EMAILADDRESS, P_SSL, P_SENDEREMAIL, P_SUBJECT, P_EVENTCODE, P_GROUPCHARGES, P_METHOD, P_CUSTOMERCOMPANYNAME from PROFILE where P_ID = @P_ID", sqlConn);
            String gid = string.Empty;
            gid = dgProfile[0, dgRow].Value.ToString();
            if (!String.IsNullOrEmpty(gid))
            {
                try
                {
                    gP_id = new Guid(dgProfile[0, dgRow].Value.ToString());
                    sqlProfile.Parameters.AddWithValue("@P_ID", gP_id);
                    sqlConn.Open();
                    SqlDataReader dr = sqlProfile.ExecuteReader(CommandBehavior.CloseConnection);
                    if (dr.HasRows)
                    {
                        dr.Read();
                        edDescription.Text = dr["P_DESCRIPTION"].ToString();
                        edReasonCode.Text = dr["P_REASONCODE"].ToString();
                        edEventCode.Text = dr["P_EVENTCODE"].ToString();
                        edRecipientID.Text = dr["P_RECIPIENTID"].ToString();
                        edServerAddress.Text = dr["P_SERVER"].ToString();
                        edFTPReceivePort.Text = dr["P_PORT"].ToString();
                        edSenderID.Text = dr["P_SENDERID"].ToString();

                        //TODO Repair XSD Load. 
                        //rtbXSD.Text = dr["P_XSD"].ToString();
                        edXSDPath.Text = dr["P_XSD"].ToString();
                        if (!string.IsNullOrEmpty(dr["P_XSD"].ToString()))
                        {
                            loadXSDTree();
                        }
                        try
                        {

                        }
                        catch (Exception ex)
                        {

                        }
                        edLibraryName.Text = dr["P_LIBNAME"].ToString();
                        edCustomMethod.Text = dr["P_METHOD"].ToString();
                        edRecipientID.Text = dr["P_RECIPIENTID"].ToString();
                        cmbMsgType.Text = dr["P_MSGTYPE"].ToString();
                        if (dr["P_ACTIVE"].ToString() == "Y")
                            cbProfileActive.Checked = true;
                        else
                            cbProfileActive.Checked = false;
                        if (dr["P_GROUPCHARGES"].ToString() == "Y")
                        {
                            cbGroupCharges.Checked = true;
                        }
                        else
                        {
                            cbGroupCharges.Checked = false;
                        }
                        if (dr["P_DTS"].ToString() == "N")
                            cbDTS.Checked = false;
                        else
                        {
                            cbDTS.Checked = true;
                        }
                        if (dr["P_DIRECTION"].ToString() == "R")
                        {
                            rbReceive.Checked = true;
                            rbSend.Checked = false;
                            edServerAddress.Text = dr["P_SERVER"].ToString();
                            edFTPReceivePath.Text = dr["P_PATH"].ToString();
                            edReceiveUsername.Text = dr["P_USERNAME"].ToString();
                            edReceivePassword.Text = dr["P_PASSWORD"].ToString();
                            edReceiveCustomerName.Text = dr["P_CUSTOMERCOMPANYNAME"].ToString();
                            edReceiveEmailAddress.Text = dr["P_EMAILADDRESS"].ToString();
                            if (dr["P_SSL"].ToString() == "Y")
                                cbSSL.Checked = true;
                            else
                                cbSSL.Checked = false;
                            edReceiveSubject.Text = dr["P_SUBJECT"].ToString();
                            edReceiveSender.Text = dr["P_SENDEREMAIL"].ToString();
                            cmbFileType.Text = dr["P_FILETYPE"].ToString();
                            switch (dr["P_DELIVERY"].ToString())
                            {
                                case "R":
                                    {
                                        rbPickup.Checked = true;
                                        break;
                                    }
                                case "F":
                                    {
                                        rbReceiveFTP.Checked = true;
                                        break;
                                    }
                                case "E":
                                    {
                                        rbEmail.Checked = true;
                                        break;
                                    }
                                case "P":
                                    {
                                        rbReceivePickup.Checked = true;
                                        break;
                                    }
                                default:
                                    {
                                        rbPickup.Checked = true;
                                        break;
                                    }
                            }
                            SetupReceive();
                        }
                        else
                        {
                            rbReceive.Checked = false;
                            rbSend.Checked = true;
                            edSendCTCUrl.Text = dr["P_SERVER"].ToString();
                            edSendFolder.Text = dr["P_PATH"].ToString();
                            edSendUsername.Text = dr["P_USERNAME"].ToString();
                            edSendPassword.Text = dr["P_PASSWORD"].ToString();
                            edSendSubject.Text = dr["P_SUBJECT"].ToString();
                            edSendEmailAddress.Text = dr["P_EMAILADDRESS"].ToString();

                            switch (dr["P_DELIVERY"].ToString())
                            {
                                case "R":
                                    {
                                        rbPickupSend.Checked = true;
                                        break;
                                    }
                                case "F":
                                    {
                                        rbFTPSend.Checked = true;
                                        break;
                                    }
                                case "D":
                                    {
                                        rbFTPSendDirect.Checked = true;
                                        break;
                                    }
                                case "E":
                                    {
                                        rbEmailSend.Checked = true;
                                        break;
                                    }
                                case "C":
                                    {
                                        rbCTCSend.Checked = true;
                                        break;
                                    }
                                default:
                                    {
                                        rbPickupSend.Checked = true;
                                        break;
                                    }
                            }
                            SetupSend();
                        }
                        btnAdd.Text = "&Update >>";
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error Saving Record : " + ex.Message);
                }

                sqlConn.Close();
                FillDTSGrid(gP_id);
            }
        }

        private void dgProfile_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            GetCustomerProfile(e.RowIndex);
        }

        private void dgProfile_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dgProfile.Rows[e.RowIndex].Cells["Direction"].Value.ToString() == "R")
            {
                dgProfile.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LawnGreen;
            }
            if (dgProfile.Rows[e.RowIndex].Cells["Direction"].Value.ToString() == "S")
            {
                dgProfile.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightSkyBlue;
            }
            if (dgProfile.Rows[e.RowIndex].Cells["Active"].Value.ToString() == "N")
            {
                dgProfile.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.LightGray;
            }
        }

        private void dgProfile_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex != -1 && e.RowIndex != -1 && e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                DataGridViewCell c = (sender as DataGridView)[e.ColumnIndex, e.RowIndex];
                if (!c.Selected)
                {
                    c.DataGridView.ClearSelection();
                    c.DataGridView.CurrentCell = c;
                    c.Selected = true;
                }
            }
        }

        private void markInactiveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int rowIndex = dgProfile.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            DataGridViewRow row = dgProfile.Rows[rowIndex];
            String strActiveToggle = "";
            if (markInactiveToolStripMenuItem.Text == "Mark InActive")
                strActiveToggle = "N";
            if (markInactiveToolStripMenuItem.Text == "Mark Active")
                strActiveToggle = "Y";

            {
                SqlCommand execDelete = new SqlCommand("Update PROFILE Set P_ACTIVE = @Active where P_ID = @P_ID", sqlConn);
                Guid p_id = new Guid(dgProfile[0, rowIndex].Value.ToString());
                execDelete.Parameters.Add("@P_ID", SqlDbType.UniqueIdentifier).Value = p_id;
                execDelete.Parameters.Add("@Active", SqlDbType.Char, 1).Value = strActiveToggle;
                try
                {
                    sqlConn.Open();
                    execDelete.ExecuteNonQuery();
                    sqlConn.Close();
                    FillGrid(this.r_id);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("There was an error :" + ex.Message, "Error Found", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }
        }

        private void duplicateProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int rowIndex = dgProfile.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            DataGridViewRow row = dgProfile.Rows[rowIndex];

            SqlCommand execDuplicate = new SqlCommand("INSERT INTO PROFILE (P_C, P_REASONCODE, P_SERVER, P_USERNAME, P_PASSWORD, P_DELIVERY, P_PORT, P_DESCRIPTION, P_PATH, P_DIRECTION, P_XSD, P_LIBNAME, " +
                                                      "P_MESSAGETYPE, P_RECIPIENTID, P_MSGTYPE, P_CHARGEABLE, P_BILLTO, P_SENDERID, P_DTS, P_EMAILADDRESS, P_FILETYPE, P_ACTIVE, P_SSL) " +
                                                      "SELECT P_C, P_REASONCODE, P_SERVER, P_USERNAME, P_PASSWORD, P_DELIVERY, P_PORT, P_DESCRIPTION +' - Duplicate' , P_PATH, P_DIRECTION, P_XSD, P_LIBNAME," +
                                                      "P_MESSAGETYPE, P_RECIPIENTID, P_MSGTYPE, P_CHARGEABLE, P_BILLTO, P_SENDERID, P_DTS, P_EMAILADDRESS, P_FILETYPE, P_ACTIVE, P_SSL " +
                                                      "FROM PROFILE AS TOCOPY WHERE (P_ID = @P_ID)", sqlConn);
            Guid p_id = new Guid(dgProfile[0, rowIndex].Value.ToString());
            execDuplicate.Parameters.Add("@P_ID", SqlDbType.UniqueIdentifier).Value = p_id;
            try
            {
                sqlConn.Open();
                execDuplicate.ExecuteNonQuery();
                sqlConn.Close();
                FillGrid(this.r_id);
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was an error :" + ex.Message, "Error Found", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        private void deleteProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int rowIndex = dgProfile.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            DataGridViewRow row = dgProfile.Rows[rowIndex];
            DialogResult drDelete = MessageBox.Show("Are you sure you wish to delete this rule?", "Delete from Customer Profile", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (drDelete == DialogResult.Yes)
            {
                SqlCommand execDelete = new SqlCommand("Delete from PROFILE where P_ID = @P_ID", sqlConn);
                Guid p_id = new Guid(dgProfile[0, rowIndex].Value.ToString());

                execDelete.Parameters.Add("@P_ID", SqlDbType.UniqueIdentifier).Value = p_id;
                try
                {
                    sqlConn.Open();
                    execDelete.ExecuteNonQuery();
                    sqlConn.Close();
                    FillGrid(this.r_id);
                    MessageBox.Show("Client rule deleted", "Customer profile updated", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                catch (Exception ex)
                {
                    MessageBox.Show("There was an error :" + ex.Message, "Error Found", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }
        }

        private void dgProfile_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            DialogResult drDelete = MessageBox.Show("Are you sure you wish to delete this rule?", "Delete from Customer Profile", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (drDelete == DialogResult.Yes)
            {
                SqlCommand execDelete = new SqlCommand("Delete from PROFILE where P_ID = @P_ID", sqlConn);
                Guid p_id = new Guid(dgProfile[0, e.Row.Index].Value.ToString());

                execDelete.Parameters.Add("@P_ID", SqlDbType.UniqueIdentifier).Value = p_id;
                try
                {
                    sqlConn.Open();
                    execDelete.ExecuteNonQuery();
                    sqlConn.Close();
                    FillGrid(this.r_id);
                    MessageBox.Show("Client rule deleted", "Customer profile updated", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                catch (Exception ex)
                {
                    MessageBox.Show("There was an error :" + ex.Message, "Error Found", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }
        }
    }
}
