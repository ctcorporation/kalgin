﻿using KIF_Sat.Repository;
using System;

namespace KIF_Sat.DTO
{
    public interface IUnitOfWork : IDisposable
    {
        ICustomerProfileRepository CustomerProfiles { get; }
        IProfileRepository Profiles { get; }
        IMapOperationRepository MapOperations { get; }
        IOrganisationRepository Organisations { get; }
        ICustomerRepository Customers { get; }
        IFileDescriptionRepository FileDescriptions { get; }
        IHeartBeatRepository HeartBeats { get; }
        IMappingEnumRepository MappingEnums { get; }
        ICargowise_EnumsRepository Cargowise_Enums { get; }
        IProcessingErrorRepository ProcessingErrors { get; }

        void AddRepos();
        int Complete();
        void Dispose();
        void Dispose(bool disposing);
    }
}