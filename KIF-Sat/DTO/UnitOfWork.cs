﻿using KIF_Sat.Models;
using KIF_Sat.Repository;
using System;
using System.Data.Entity.Validation;

namespace KIF_Sat.DTO
{
    public class UnitOfWork : IUnitOfWork
    {
        #region members

        private readonly KifEntity _context;
        private bool disposed = false;
        private string _errorMessage = string.Empty;
        #endregion

        #region properties
        public string ErrorLog
        {
            get
            { return _errorMessage; }
            set
            {
                _errorMessage = value;

            }
        }


        public ICustomerProfileRepository CustomerProfiles
        {
            get;
            private set;
        }

        public IProcessingErrorRepository ProcessingErrors
        {
            get;
            private set;
        }

        public IProfileRepository Profiles
        {
            get;
            private set;
        }

        public IMapOperationRepository MapOperations
        {
            get;
            private set;
        }

        public IOrganisationRepository Organisations
        {
            get;
            private set;
        }
        public ICustomerRepository Customers
        {
            get;
            private set;
        }
        public IFileDescriptionRepository FileDescriptions
        {
            get;
            private set;
        }
        public IMappingEnumRepository MappingEnums
        {
            get;
            private set;
        }

        public IHeartBeatRepository HeartBeats
        {
            get;
            private set;
        }

        public ICargowise_EnumsRepository Cargowise_Enums
        {
            get;
            private set;
        }

        #endregion

        #region constructors

        public UnitOfWork(KifEntity context)
        {
            _context = context;
            AddRepos();
        }

        #endregion

        #region methods
        public void AddRepos()
        {
            FileDescriptions = new FileDescriptionRepository(_context);
            MappingEnums = new MappingEnumRepository(_context);
            HeartBeats = new HeartBeatRepository(_context);
            Cargowise_Enums = new Cargowise_EnumsRepository(_context);
            Customers = new CustomerRepository(_context);
            Organisations = new OrganisationRepository(_context);
            MapOperations = new MapOperationRepository(_context);
            Profiles = new ProfileRepository(_context);
            ProcessingErrors = new ProcessingErrorRepository(_context);
            CustomerProfiles = new CustomerProfileRepository(_context);
        }

        public int Complete()
        {
            try
            {
                return _context.SaveChanges();
            }
            catch (InvalidOperationException ex)
            {
                _errorMessage += ex.GetType().Name + "Found: Error was: " + ex.Message + ":" + ex.InnerException.Message + Environment.NewLine;
            }
            catch (DbEntityValidationException ex)
            {

                foreach (DbEntityValidationResult validationResult in ex.EntityValidationErrors)
                {
                    string entityName = validationResult.Entry.Entity.GetType().Name;
                    foreach (DbValidationError error in validationResult.ValidationErrors)
                    {
                        _errorMessage += entityName + "." + error.PropertyName + ": " + error.ErrorMessage + Environment.NewLine;

                    }
                }
            }
            return -1;
        }
        #endregion

        #region helpers
        public virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }

        public void Dispose()
        {
            if (!disposed)
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
        }
        #endregion
    }
}
