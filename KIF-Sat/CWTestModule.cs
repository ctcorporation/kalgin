﻿using System;
using System.Windows.Forms;

namespace KIF_Sat
{
    public partial class CWTestModule : Form
    {
        public CWTestModule()
        {
            InitializeComponent();
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            edSendeAdapterResults.Clear();
            if (!validateSender()) return;
            //if (!validateRecipient()) return;
            if (!validatePassword()) return;
            if (!validateServer()) return;
        }

        private bool validateSender()
        {
            var code = edSendUsername.Text.Trim();
            if (String.IsNullOrEmpty(code))
            {
                edSendeAdapterResults.Text += ("Sender ID cannot be Blank\r\n");
                return false;
            }

            edSendeAdapterResults.Text += "Sender ID is Ok\r\n";
            return true;
        }


        private void bbSend_Click(object sender, EventArgs e)
        {
            //       String ctcMessage = CTCAdapter.SendMessage(edSendCTCUrl.Text, edSendFile.Text, edRecipientID.Text, edSendUsername.Text, edSendPassword.Text);
            //       NodeResource.AddText(edSendeAdapterResults, ctcMessage);
        }

        private void bbClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bbDir_Click(object sender, EventArgs e)
        {
            fdSendFile.ShowDialog();
            edSendFile.Text = fdSendFile.FileName;
        }

        private bool validatePassword()
        {
            var code = edSendPassword.Text.Trim();
            if (String.IsNullOrEmpty(code))
            {
                edSendeAdapterResults.Text += "Password should not be Blank\r\n";
                return false;
            }
            edSendeAdapterResults.Text += "Password is ok\r\n";
            return true;

        }
        private bool validateServer()
        {
            var code = edSendCTCUrl.Text.Trim();
            if (String.IsNullOrEmpty(code))
            {
                edSendeAdapterResults.Text += "Service URL cannot be Blank\r\n";
                return false;
            }
            edSendeAdapterResults.Text += "Service URL is OK.\r\n";
            try
            {
                //if (CTCAdapter.Ping(edSendCTCUrl.Text, edSendUsername.Text, edSendPassword.Text))
                //{
                //    edSendeAdapterResults.Text += "Ping Service URL is Successful\r\n";
                //    return true;
                //}
                //else
                //{
                //    edSendeAdapterResults.Text += "Ping Service URL is Un-Successful. Please check username and password or URL\r\n";
                //    return false;
                //}
                return false;

            }
            catch (Exception ex)
            {
                edSendeAdapterResults.Text += "Error Message: " + ex.Message + "\r\n";
                return false;
            }
        }
    }
}
