﻿using System;

namespace KIF_Sat
{
    class SatelliteErrors
    {
        public enum NodeError
        {
            //Missing Profile Error
            e100,
            //File not Found Error
            e101,
            //File in use Error
            e102,
            //FTP Receive Error
            e103,
            //FTP Send Error
            e104,
            //SMTP Send Error
            e105,
            //Database Connection Error
            e106,
            //Missing Cust ID Error
            e107,
            //eAdapter send error
            e108,
            //File IO Error
            e109,
            //XML Node Error
            e110,
            // Unhandled Error
            e111,
            // DataSource Collection not found in UDM XML
            e112
            // Undefined error in XUE XML

        }

        public class ProcessResult
        {
            private string folderloc;
            private bool processed;

            public string FolderLoc
            {
                get
                {
                    return folderloc;
                }
                set
                {
                    folderloc = value;
                }
            }
            public bool Processed
            {
                get
                {
                    return processed;
                }
                set
                {
                    processed = value;
                }
            }
        }
        public class CTCErrorCode
        {
            private NodeError code;
            private string description;
            private string severity;

            public NodeError Code
            {
                get
                {
                    return code;
                }
                set
                {
                    code = value;
                }
            }
            public string Description
            {
                get
                {
                    return description;
                }
                set
                {
                    description = value;
                }
            }
            public string Severity
            {
                get
                {
                    return severity;
                }
                set
                {
                    severity = value;
                }
            }
        }
        public class ProcessingErrors
        {
            private Guid procid;

            private string senderid;

            private string recipientid;

            private CTCErrorCode errorCode;

            private string filename;

            private DateTime procdate;

            public Guid ProcId
            {
                get
                { return procid; }
                set
                { procid = value; }
            }

            public string SenderId
            {
                get
                {
                    return senderid;
                }
                set
                {
                    senderid = value;
                }
            }
            public string RecipientId
            {
                get
                {
                    return recipientid;
                }
                set
                {
                    recipientid = value;
                }
            }

            public CTCErrorCode ErrorCode
            {
                get
                {
                    return errorCode;

                }
                set
                {
                    errorCode = value;
                }
            }
            public string FileName
            {
                get
                { return filename; }
                set
                { filename = value; }
            }

        }
    }
}
