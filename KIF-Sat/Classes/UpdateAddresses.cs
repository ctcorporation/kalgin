﻿
using KIF_Sat.DTO;
using KIF_Sat.Models;
using NodeResources;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace KIF_Sat.Classes
{
    public class UpdateAddresses
    {
        static readonly AddressMessage MessageStruct = new AddressMessage
        {
            CompanyType = "Company Type:",
            CompanyName = "Company Name:",
            OrderNo = "Order No:",
            ShortName = "Short Name:",
            Address = "Address:",
            City = "City:",
            State = "State:",
            PostCode = "Post Code:",
            CountryCode = "Country Code:",
            Discharge = "Port Of Discharge Code:",
            POOS = "(Sea) Port of Loading:",
            POLS = "(Sea) Port of Loading:",
            POOA = "(Air) Port of Origin:",
            POLA = "(Air) Port of Loading:",
            CWCode = "Paste Cargowise Code here:"
        };
        #region members
        string _text;
        private IMailServerSettings _mailbox;

        #endregion

        #region Properties
        public Stream Message { get; set; }
        public IMailServerSettings SystemMail { get; set; }
        public Profile Profile { get; set; }

        public IConnectionManager ConnString { get; set; }

        #endregion

        #region Constructors
        public UpdateAddresses(Profile profile)
        {
            Profile = profile;
        }
        #endregion

        #region Methods
        public List<AddressMessage> ScanMailBox()
        {
            var _addresses = new List<AddressMessage>();
            int port = 0;
            if (Profile != null)
            {
                if (Profile.P_DELIVERY == "E")
                {

                    IMailServerSettings mailbox = new MailServerSettings
                    {
                        Email = Profile.P_EMAILADDRESS,
                        Server = Profile.P_SERVER,
                        Port = int.TryParse(Profile.P_PORT, out port) ? port : 0,
                        UserName = Profile.P_USERNAME,
                        Password = Profile.P_PASSWORD,
                        IsSecure = Profile.P_SSL == "Y" ? true : false
                    };
                    using (MailModule mail = new MailModule(mailbox, "csr@ctcorp.com.au", Globals.glTestLocation))
                    {
                        List<IMsgBody> msgSummary = mail.GetMailBody().ToList();
                        foreach (var msg in msgSummary)
                        {
                            if (msg.MsgSubject != null)
                            {
                                if (msg.MsgSubject.Contains(Profile.P_SUBJECT))
                                {
                                    byte[] byteArray = Encoding.ASCII.GetBytes(msg.MsgText);
                                    MemoryStream stream = new MemoryStream(byteArray);
                                    var address = ConvertMessage(stream);
                                    if (address != null)
                                    {
                                        _addresses.Add(address);
                                        SendResponse(msg, address);
                                    }
                                }
                            }

                        }

                    }
                    return _addresses;

                }


            }
            return null;

        }

        public void AddAddress(AddressMessage address)
        {
            using (IUnitOfWork uow = new UnitOfWork(new KifEntity(ConnString.ConnString)))
            {
                var ad = (uow.Organisations.Find(x => x.OR_ShortCode == address.ShortName)).FirstOrDefault();
                bool newAdd = false;
                if (ad == null)
                {
                    ad = new Organisation();
                    newAdd = true;
                }
                string addressPart = "Company Name";
                try
                {
                    addressPart = "Company Name";
                    ad.OR_Name = string.IsNullOrEmpty(address.CompanyName) ? string.Empty : address.CompanyName.Trim().Truncate(100);
                    addressPart = "Short Code";
                    ad.OR_ShortCode = string.IsNullOrEmpty(address.ShortName) ? string.Empty : address.ShortName.Trim().Truncate(50);
                    addressPart = "Address";
                    ad.OR_Address1 = string.IsNullOrEmpty(address.Address) ? string.Empty : address.Address.Trim().Truncate(100);
                    addressPart = "Cargowise Code";
                    ad.OR_CODE = string.IsNullOrEmpty(address.CWCode) ? string.Empty : address.CWCode.Trim().Truncate(20);
                    addressPart = "State";
                    ad.OR_State = string.IsNullOrEmpty(address.State) ? string.Empty : address.State.Trim().Truncate(10);
                    addressPart = "City";
                    ad.OR_Suburb = string.IsNullOrEmpty(address.City) ? string.Empty : address.City.Trim().Truncate(50);
                    addressPart = "PostCode";
                    ad.OR_PostCode = string.IsNullOrEmpty(address.PostCode) ? string.Empty : address.PostCode.Trim().Truncate(10);
                    addressPart = "Company Type";
                    ad.OR_Type = string.IsNullOrEmpty(address.CompanyType) ? string.Empty : address.CompanyType.Trim().Truncate(50);
                    addressPart = "Air POL";
                    ad.OR_AirPortOfLoading = string.IsNullOrEmpty(address.POLA) ? string.Empty : address.POLA.Trim().Truncate(5);
                    addressPart = "Sea POO";
                    ad.OR_SeaPortOfOrigin = string.IsNullOrEmpty(address.POOS) ? string.Empty : address.POOS.Trim().Truncate(5);
                    addressPart = "Air POO";
                    ad.OR_AirPortOfOrigin = string.IsNullOrEmpty(address.POOA) ? string.Empty : address.POOA.Trim().Truncate(5);
                    addressPart = "POD";
                    ad.OR_PortOfDischarge = string.IsNullOrEmpty(address.Discharge) ? string.Empty : address.Discharge.Trim().Truncate(5);
                    addressPart = "Sea POL";
                    ad.OR_PortOfLoading = string.IsNullOrEmpty(address.POLS) ? string.Empty : address.POLS.Trim().Truncate(5);
                    addressPart = "Full Company Name";
                    ad.OR_MapValue = address.CompanyName.Truncate(200);
                    if (newAdd)
                    {
                        uow.Organisations.Add(ad);
                    }
                    uow.Complete();
                }
                catch (NullReferenceException ex)
                {
                    using (MailModule mail = new MailModule(SystemMail))
                    {
                        mail.SendMsg("", Profile.P_NOTIFY, "Address Update Failed:", "Address Email Message was missing " + addressPart); ;
                    }
                }

            }
        }
        private void SendResponse(IMsgBody msg, AddressMessage address)
        {
            using (MailModule mail = new MailModule(SystemMail))
            {
                mail.SendMsg("", msg.MsgFrom, "Re: " + msg.MsgSubject, "Address for Order Number: " + address.OrderNo + " updated.");
            }
        }

        public AddressMessage ConvertMessage(Stream stream)
        {
            AddressMessage newMessage = new AddressMessage();
            using (StreamReader sr = new StreamReader(stream))
            {
                bool validMsg = false;
                var ln = sr.ReadLine();
                try
                {
                    while (!sr.EndOfStream)
                    {
                        while (FoundText(MessageStruct.CompanyType, ln) == 999)
                        {
                            ln = sr.ReadLine();
                        }
                        newMessage.CompanyType = _text;
                        validMsg = true;
                        while (FoundText(MessageStruct.OrderNo, ln) == 999)
                        {
                            ln = sr.ReadLine();
                        }
                        newMessage.OrderNo = string.IsNullOrEmpty(_text) ? string.Empty : _text.Trim();
                        while (FoundText(MessageStruct.CompanyName, ln) == 999)
                        {
                            ln = sr.ReadLine();
                        }
                        newMessage.CompanyName = string.IsNullOrEmpty(_text) ? string.Empty : _text.Trim();
                        string compaddress = "";
                        if (newMessage.CompanyName.Contains(MessageStruct.ShortName))
                        {
                            FoundText(MessageStruct.ShortName, newMessage.CompanyName);
                            newMessage.ShortName = _text.Trim();
                            newMessage.CompanyName = newMessage.CompanyName.Substring(0, newMessage.CompanyName.IndexOf(MessageStruct.ShortName) - 1).Trim();

                            while (FoundText(MessageStruct.ShortName, ln) == 999)
                            {
                                ln = sr.ReadLine();
                                compaddress += ln.StartsWith(MessageStruct.ShortName) ? string.Empty : ln;
                            }


                        }
                        else
                        {
                            while (FoundText(MessageStruct.ShortName, ln) == 999)
                            {
                                ln = sr.ReadLine();
                                compaddress += ln.StartsWith(MessageStruct.ShortName) ? string.Empty : ln.Trim();

                            }
                            newMessage.ShortName = string.IsNullOrEmpty(_text) ? string.Empty : _text.Trim();

                        }
                        newMessage.CompanyName += compaddress.RemoveChars('\n');
                        newMessage.MapValue = newMessage.CompanyName.Truncate(200);

                        while (FoundText(MessageStruct.Address, ln) == 999)
                        {
                            ln = sr.ReadLine();
                        }
                        newMessage.Address = string.IsNullOrEmpty(_text)?string.Empty:_text.Trim();
                        while (FoundText(MessageStruct.City, ln) == 999)
                        {
                            ln = sr.ReadLine();
                        }
                        newMessage.City = string.IsNullOrEmpty(_text) ? string.Empty : _text.Trim();
                        while (FoundText(MessageStruct.State, ln) == 999)
                        {
                            ln = sr.ReadLine();
                        }
                        newMessage.State = string.IsNullOrEmpty(_text) ? string.Empty : _text.Trim();
                        while (FoundText(MessageStruct.PostCode, ln) == 999)
                        {
                            ln = sr.ReadLine();
                        }
                        newMessage.PostCode = string.IsNullOrEmpty(_text) ? string.Empty : _text.Trim();
                        while (FoundText(MessageStruct.CountryCode, ln) == 999)
                        {
                            ln = sr.ReadLine();
                        }
                        newMessage.CountryCode = string.IsNullOrEmpty(_text) ? string.Empty : _text.Trim();
                        while (FoundText(MessageStruct.Discharge, ln) == 999)
                        {
                            ln = sr.ReadLine();
                        }
                        newMessage.Discharge = string.IsNullOrEmpty(_text) ? string.Empty : _text.Trim();
                        while (FoundText(MessageStruct.POOS, ln) == 999)
                        {
                            ln = sr.ReadLine();
                        }
                        newMessage.POOS = string.IsNullOrEmpty(_text) ? string.Empty : _text.Trim();
                        while (FoundText(MessageStruct.POLS, ln) == 999)
                        {
                            ln = sr.ReadLine();
                        }
                        newMessage.POLS = string.IsNullOrEmpty(_text) ? string.Empty : _text.Trim();
                        while (FoundText(MessageStruct.POOA, ln) == 999)
                        {
                            ln = sr.ReadLine();
                        }
                        newMessage.POOA = string.IsNullOrEmpty(_text) ? string.Empty : _text.Trim();
                        while (FoundText(MessageStruct.POLA, ln) == 999)
                        {
                            ln = sr.ReadLine();
                        }
                        newMessage.POLS = string.IsNullOrEmpty(_text) ? string.Empty : _text.Trim();
                        while (FoundText(MessageStruct.CWCode, ln) == 999)
                        {
                            ln = sr.ReadLine();
                        }
                        newMessage.CWCode = string.IsNullOrEmpty(_text) ? string.Empty : _text.Trim();
                        if (validMsg)
                        {
                            return newMessage;
                        }
                    }
                }
                catch (NullReferenceException ex)
                {

                }

            }
            return null;
        }


        #endregion
        #region Helpers
        private int FoundText(string toFind, string searchIn)
        {
            _text = "";
            if (string.IsNullOrEmpty(toFind))
            {
                return 999;
            }
            if (searchIn.Contains(toFind))
            {
                var i = searchIn.IndexOf(toFind);
                var x = i + toFind.Length;
                _text = searchIn.Substring(x, (searchIn.Length - x));
                return searchIn.IndexOf(toFind);
            }
            return 999;
        }
        #endregion

    }


}
