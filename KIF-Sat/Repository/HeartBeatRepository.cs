﻿using KIF_Sat.Models;

namespace KIF_Sat.Repository
{
    public class HeartBeatRepository : GenericRepository<HeartBeat>, IHeartBeatRepository
    {
        public HeartBeatRepository(KifEntity context)
            : base(context)
        {

        }

        public KifEntity KifEntity
        {
            get { return Context as KifEntity; }
        }
    }
}
