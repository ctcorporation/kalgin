﻿using KIF_Sat.Models;

namespace KIF_Sat.Repository
{
    public interface IMapOperationRepository : IGenericRepository<MapOperation>
    {
        KifEntity KifEntity { get; }
    }
}