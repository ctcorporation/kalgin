﻿using KIF_Sat.Models;

namespace KIF_Sat.Repository
{
    public class FileDescriptionRepository : GenericRepository<FileDescription>, IFileDescriptionRepository
    {
        public FileDescriptionRepository(KifEntity context)
                    : base(context)
        {

        }

        public KifEntity KifEntity
        {
            get { return Context as KifEntity; }
        }
    }
}
