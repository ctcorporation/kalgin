﻿using KIF_Sat.Models;

namespace KIF_Sat.Repository
{
    public interface IFileDescriptionRepository : IGenericRepository<FileDescription>
    {
        KifEntity KifEntity { get; }
    }
}