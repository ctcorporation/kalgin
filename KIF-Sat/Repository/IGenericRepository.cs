﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace KIF_Sat.Repository
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        void Add(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
        TEntity Get(Guid id);
        IEnumerable<TEntity> GetAll();
        void Remove(TEntity entity);
        void Remove(Expression<Func<TEntity, bool>> predicate);
        void RemoveRange(IEnumerable<TEntity> entities);
    }
}