﻿using KIF_Sat.Models;

namespace KIF_Sat.Repository
{
    public interface IProfileRepository : IGenericRepository<Profile>
    {
        KifEntity KifEntity { get; }
    }
}