﻿using KIF_Sat.Models;

namespace KIF_Sat.Repository
{
    public class MapOperationRepository : GenericRepository<MapOperation>, IMapOperationRepository
    {
        public MapOperationRepository(KifEntity context)
            : base(context)
        {

        }

        public KifEntity KifEntity => Context as KifEntity;
    }
}
