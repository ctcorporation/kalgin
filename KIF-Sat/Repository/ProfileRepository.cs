﻿using KIF_Sat.Models;

namespace KIF_Sat.Repository
{
    public class ProfileRepository : GenericRepository<Profile>, IProfileRepository
    {
        public ProfileRepository(KifEntity context)
            : base(context)
        {

        }

        public KifEntity KifEntity
        {
            get { return Context as KifEntity; }
        }
    }
}
