﻿using KIF_Sat.Models;

namespace KIF_Sat.Repository
{
    public class ProcessingErrorRepository : GenericRepository<Processing_Error>, IProcessingErrorRepository
    {
        public ProcessingErrorRepository(KifEntity context)
            : base(context)
        {

        }

        public KifEntity KifEntity
        {
            get
            {
                return Context as KifEntity;
            }
        }
    }
}
