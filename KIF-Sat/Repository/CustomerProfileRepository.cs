﻿using KIF_Sat.Models;

namespace KIF_Sat.Repository
{
    public class CustomerProfileRepository : GenericRepository<CustomerProfile>, ICustomerProfileRepository
    {
        public CustomerProfileRepository(KifEntity connString)
            : base(connString)
        {

        }

        public KifEntity KifEntity
        {
            get
            {
                return Context as KifEntity;
            }
        }

    }
}
