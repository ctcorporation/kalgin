﻿using KIF_Sat.Models;

namespace KIF_Sat.Repository
{
    public interface ICustomerProfileRepository : IGenericRepository<CustomerProfile>
    {
        KifEntity KifEntity { get; }
    }
}