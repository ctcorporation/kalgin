﻿using KIF_Sat.Models;

namespace KIF_Sat.Repository
{
    public interface ICustomerRepository : IGenericRepository<Customer>
    {
        KifEntity KifEntity { get; }
    }
}