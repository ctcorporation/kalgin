﻿using KIF_Sat.Models;

namespace KIF_Sat.Repository
{
    public class Cargowise_EnumsRepository : GenericRepository<Cargowise_Enums>, ICargowise_EnumsRepository
    {
        public Cargowise_EnumsRepository(KifEntity context)
            : base(context)
        {

        }
        public KifEntity KifEntity
        {
            get
            { return Context as KifEntity; }

        }
    }
}
