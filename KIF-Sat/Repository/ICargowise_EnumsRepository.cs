﻿using KIF_Sat.Models;

namespace KIF_Sat.Repository
{
    public interface ICargowise_EnumsRepository : IGenericRepository<Cargowise_Enums>
    {
        KifEntity KifEntity { get; }
    }
}