﻿using KIF_Sat.Models;

namespace KIF_Sat.Repository
{
    public interface IHeartBeatRepository : IGenericRepository<HeartBeat>
    {
        KifEntity KifEntity { get; }
    }
}