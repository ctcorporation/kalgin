﻿using KIF_Sat.Models;

namespace KIF_Sat.Repository
{
    public class MappingEnumRepository : GenericRepository<MappingEnum>, IMappingEnumRepository
    {
        public MappingEnumRepository(KifEntity context)
            : base(context)
        {

        }

        public KifEntity KifEntity
        {
            get { return Context as KifEntity; }
        }

    }
}
