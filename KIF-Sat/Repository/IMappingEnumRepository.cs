﻿using KIF_Sat.Models;

namespace KIF_Sat.Repository
{
    public interface IMappingEnumRepository : IGenericRepository<MappingEnum>
    {
        KifEntity KifEntity { get; }
    }
}