﻿using KIF_Sat.Models;

namespace KIF_Sat.Repository
{
    public interface IProcessingErrorRepository : IGenericRepository<Processing_Error>
    {
        KifEntity KifEntity { get; }
    }
}