﻿using KIF_Sat.Models;

namespace KIF_Sat.Repository
{
    public class OrganisationRepository : GenericRepository<Organisation>, IOrganisationRepository
    {
        public OrganisationRepository(KifEntity context)
            : base(context)
        {

        }

        public KifEntity KifEntity
        {
            get
            {
                return Context as KifEntity;
            }
        }
    }
}
