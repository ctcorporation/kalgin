﻿using KIF_Sat.Models;

namespace KIF_Sat.Repository
{
    public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(KifEntity context)
            : base(context)
        {

        }

        public KifEntity KifEntity
        {
            get
            {
                return Context as KifEntity;
            }
        }
    }
}
