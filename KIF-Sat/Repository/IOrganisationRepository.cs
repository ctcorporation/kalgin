﻿using KIF_Sat.Models;

namespace KIF_Sat.Repository
{
    public interface IOrganisationRepository : IGenericRepository<Organisation>
    {
        KifEntity KifEntity { get; }
    }
}