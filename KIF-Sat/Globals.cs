﻿using Ionic.Zip;
using NodeResources;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Schema;

namespace KIF_Sat
{


    public class TransReference
    {
        private string reference1;
        private string reference2;
        private string reference3;
        private RefType ref1type;
        private RefType ref2type;
        private RefType ref3type;
        public string Reference1
        {
            get { return this.reference1; }
            set { this.reference1 = value; }
        }
        public string Reference2
        {
            get { return this.reference2; }
            set { this.reference2 = value; }
        }
        public string Reference3
        {
            get { return this.reference3; }
            set { this.reference3 = value; }
        }
        public RefType Ref1Type
        {
            get { return this.ref1type; }
            set { this.ref1type = value; }
        }
        public RefType Ref2Type
        {
            get { return this.ref2type; }
            set { this.ref2type = value; }
        }
        public RefType Ref3Type
        {
            get { return this.ref3type; }
            set { this.ref3type = value; }
        }

        public enum RefType
        {
            Master,

            Housebill,

            Order,

            Consol,

            Shipment,

            Container,

            Invoice,

            Brokerage,

            Transaction,

            Operation,

            WHSPick,

            WHSOrder,

            Product,

            Custom,

            Response





        }


    }

    public class CustomValidateHandler
    {
        private static Boolean isValid;
        private static IList<string> myValList = new List<string>();

        public IList<string> ValidationList
        {
            get { return myValList; }
            set { myValList = value; }
        }

        public Boolean IsValid
        {
            get { return isValid; }
            set { isValid = value; }
        }

        public static void HandlerErrors(Object sender, ValidationEventArgs e)
        {
            if (e.Severity == XmlSeverityType.Error)
            {
                isValid = false;
                myValList.Add(
                    String.Format(
                    Environment.NewLine + "Line: {0}, Position {1}: \"{2}\"",
                    e.Exception.LineNumber,
                    e.Exception.LinePosition,
                    e.Exception.Message));
            }

        }
    }



    public class SMTPServer
    {
        private static String server;
        private static String port;
        private static String username;
        private static String email;
        private static String password;

        public static String Server
        {
            get { return server; }
            set { server = value; }
        }
        public static String Port
        {
            get { return port; }
            set { port = value; }
        }
        public static String Email
        {
            get { return email; }
            set { email = value; }
        }
        public static String Username
        {
            get { return username; }
            set { username = value; }
        }
        public static String Password
        {
            get { return password; }
            set { password = value; }
        }
    }
    public class Globals
    {

        private static IMailServerSettings _mailServerSettings
        { get; set; }


        public static IMailServerSettings glMailServerSettings
        {
            get { return _mailServerSettings; }
            set { _mailServerSettings = value; }
        }
        public static string glProfilePath { get; set; }
        public static string glFailPath { get; set; }

        public static string glDbServer { get; set; }

        public static string glDbInstance { get; set; }

        public static string glDbUserName { get; set; }

        public static string glDbPassword { get; set; }

        public static string glCTCDbServer { get; set; }

        public static string glCTCDbInstance { get; set; }

        public static string glCTCDbUserName { get; set; }

        public static string glCTCDbPassword { get; set; }

        public static string glArchiveFreq { get; set; }

        public static string glAlertsTo { get; set; }

        public static string glCustCode { get; set; }

        public static string glCustAlertsto { get; set; }

        public static string glArcLocation { get; set; }

        public static string glTestLocation { get; set; }

        public static string glLibPath { get; set; }

        public static Guid gl_CustId { get; set; }

        private static IConnectionManager _connString { get; set; }


        public static IConnectionManager ConnString
        {
            get { return _connString; }
            set { _connString = value; }
        }


        public static string connString()
        {
            string sConnection = "Data Source=" + glDbServer + ";Initial Catalog=" + glDbInstance + ";User ID = " + glDbUserName + ";Password=" + glDbPassword + ";";
            return sConnection;
        }

        public static IConnectionManager CTCconnString
        {
            get;
            set;
        }
        public static Customer GetCustomer(string custCode)
        {
            Customer result = new Customer();
            SqlConnection sqlConn = new SqlConnection { ConnectionString = Globals.connString() };
            SqlCommand sqlCust = new SqlCommand("Select  C_ID, C_CODE, C_NAME, C_SHORTNAME, C_PATH, C_IS_ACTIVE, C_ON_HOLD, C_FTP_CLIENT  FROM CUSTOMER WHERE C_CODE = @C_CODE", sqlConn);
            sqlCust.Parameters.AddWithValue("@C_CODE", custCode);

            try
            {
                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();
                }

                sqlConn.Open();
                SqlDataReader dr = sqlCust.ExecuteReader(CommandBehavior.CloseConnection);
                if (dr.HasRows)
                {
                    dr.Read();
                    result.CustomerName = dr["C_NAME"].ToString();
                    result.ShortName = dr["C_SHORTNAME"].ToString();
                    result.Id = (Guid)dr["C_ID"];
                    result.Code = dr["C_CODE"].ToString();
                    dr.Close();

                }
                else
                {
                    result = null;
                }

                // btnAdd.Text="E&dit";   
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was an error :" + ex.Message, "Error Locating Records", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return result;

        }

        public static string glAppConfig { get; set; }

        public static string glPickupPath { get; set; }
        public static string glOutputDir { get; set; }


        static int GetWeekNumberOfMonth(DateTime date)
        {
            date = date.Date;
            DateTime firstMonthDay = new DateTime(date.Year, date.Month, 1);
            DateTime firstMonthMonday = firstMonthDay.AddDays((DayOfWeek.Monday + 7 - firstMonthDay.DayOfWeek) % 7);
            if (firstMonthMonday > date)
            {
                firstMonthDay = firstMonthDay.AddMonths(-1);
                firstMonthMonday = firstMonthDay.AddDays((DayOfWeek.Monday + 7 - firstMonthDay.DayOfWeek) % 7);
            }
            return (date - firstMonthMonday).Days / 7 + 1;
        }

        public static String ArchiveFile(string arcPath, string fileName)
        {
            String archive = "";
            String archiveName = Globals.glCustCode + DateTime.Now.ToString("MMM-yyyy");
            try
            {

                if (!File.Exists(Path.Combine(arcPath, archiveName + ".ZIP")))
                {
                    using (ZipFile ZipNew = new ZipFile())
                    {
                        String newArchive = Path.Combine(arcPath, archiveName + ".ZIP");
                        ZipNew.AddFile(fileName, "");
                        ZipNew.Save(Path.Combine(newArchive));
                        archive = newArchive;
                        ZipNew.Dispose();
                    }
                }
                else
                {
                    var zipFile = ZipFile.Read(@Path.Combine(arcPath, archiveName + ".ZIP"));
                    var result = zipFile.Any(entry => entry.FileName.EndsWith(Path.GetFileName(fileName)));
                    zipFile.Dispose();
                    if (!result)
                    {
                        using (ZipFile Zip1 = ZipFile.Read(@Path.Combine(arcPath, archiveName + ".ZIP")))
                        {

                            try
                            {
                                Zip1.AddFile(fileName, "");
                                Zip1.Save(@Path.Combine(arcPath, archiveName + ".ZIP"));
                                Zip1.Save();

                            }
                            catch (Exception)
                            {

                            }
                            Zip1.Dispose();
                        }
                    }
                    else
                    {
                        archive = Path.Combine(arcPath, archiveName + ".ZIP");
                    }
                }
                //   File.Delete(fileName);
            }
            catch (Exception)
            {
            }

            return Path.GetFileName(archive);
        }


        public static void CreateProfPath(String path, String profile)
        {
            try
            {
                String profilePath = Path.Combine(path, profile);
                Directory.CreateDirectory(profilePath);
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("You do not have access to create a folder here.Please Check", "Folder Write Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (IOException exIO)
            {
                MessageBox.Show("Error Creating Directory: " + exIO.Message, "Folder Write Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public class Customer
        {
            public string CustomerName { get; set; }
            public string Code { get; set; }
            public Guid Id { get; set; }
            public string ShortName { get; set; }

        }

        public static void CreateRootPath(String path)
        {
            try
            {


                if (Directory.Exists(path))
                {
                    String rootPath = Path.Combine(path);
                    Directory.CreateDirectory(rootPath);
                    Directory.CreateDirectory(Path.Combine(rootPath, "Processing"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Held"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Archive"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Lib"));
                }
                else
                {
                    Directory.CreateDirectory(path);
                    String rootPath = Path.Combine(path);
                    Directory.CreateDirectory(Path.Combine(rootPath, "Processing"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Held"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Archive"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Lib"));
                }
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("You do not have access to create a folder here.Please Check", "Folder Write Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (IOException exIO)
            {
                MessageBox.Show("Error Creating Directory: " + exIO.Message, "Folder Write Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
