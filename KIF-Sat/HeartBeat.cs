﻿using KIF_Sat.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace KIF_Sat
{

    public static class Heartbeat
    {
        private static List<Assembly> GetListOfEntryAssemblyWithReferences()
        {
            List<Assembly> listOfAssemblies = new List<Assembly>();
            var mainAsm = Assembly.GetEntryAssembly();
            listOfAssemblies.Add(mainAsm);

            foreach (var refAsmName in mainAsm.GetReferencedAssemblies())
            {
                listOfAssemblies.Add(Assembly.Load(refAsmName));
            }
            return listOfAssemblies;
        }
        public static void RegisterHeartBeat(string custCode, string operation, ParameterInfo[] parameters, string pathParam)
        {
            Process currentProcess = Process.GetCurrentProcess();
            var appParam = Assembly.GetEntryAssembly().GetName().Name;
            var custID = GetCustId(custCode);
            DateTime? checkinParam = null;
            DateTime? openParam = null;
            int pid = currentProcess.Id;
            if (operation == "Starting")
            {
                checkinParam = DateTime.Now;
                openParam = DateTime.Now;

            }
            if (operation == "Stopping")
            {
                pid = 0;
                openParam = null;
                checkinParam = null;


            }
            string pList = string.Empty;
            if (parameters != null)
            {
                for (int i = 0; i < parameters.Length; i++)
                {
                    pList += parameters[i].Name + ": " + parameters[i].ToString();
                }
            }
            try
            {
                using (CNodeEntity db = new CNodeEntity(Globals.CTCconnString.ConnString))
                {
                    var hb = (from h in db.HeartBeat
                              where h.HB_NAME == appParam
                              select h).FirstOrDefault();
                    if (hb == null)
                    {

                        Models.HeartBeat newHeartBeart = new Models.HeartBeat
                        {
                            HB_C = custID,
                            HB_PATH = pathParam,
                            HB_PID = pid,
                            HB_LASTOPERATIONPARAMS = pList,
                            HB_LASTCHECKIN = checkinParam,
                            HB_LASTOPERATION = operation,
                            HB_NAME = appParam,
                            HB_OPENED = openParam,
                            HB_ISMONITORED = "Y"
                        };
                        db.HeartBeat.Add(newHeartBeart);
                    }
                    else
                    {
                        hb.HB_LASTCHECKIN = DateTime.Now;
                        hb.HB_LASTOPERATION = operation;
                        hb.HB_LASTOPERATIONPARAMS = pList;
                        hb.HB_PID = pid;
                    }
                    db.SaveChanges();

                }

            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;

            }

        }

        public static Guid GetCustId(string custcode)
        {
            using (CNodeEntity db = new CNodeEntity(Globals.CTCconnString.ConnString))
            {
                var cust = (from c in db.Customers
                            where c.C_CODE == custcode
                            select c).FirstOrDefault();
                if (cust != null)
                {
                    return cust.C_ID;

                }
                else
                {
                    return Guid.Empty;
                }
            }
        }
    }

}
