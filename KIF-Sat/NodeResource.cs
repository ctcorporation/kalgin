﻿using KIF_Sat.Models;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using static KIF_Sat.SatelliteErrors;

namespace KIF_Sat
{
    class NodeResource
    {


        public static Guid GetCustomer(string custCode)
        {
            using (KifEntity db = new KifEntity(Globals.ConnString.ConnString))
            {
                var cus = (from c in db.Customers
                           where c.C_CODE == custCode
                           select c.C_ID).FirstOrDefault();
                return cus;
            }
        }


        public static DateTime ConvertExcelDate(int serialDate)
        {
            if (serialDate > 59)
            {
                serialDate -= 1;
            }

            return new DateTime(1899, 12, 31).AddDays(serialDate);
        }
        public static DataTable ConvertCSVtoDataTable(string strFilePath)
        {
            StreamReader sr = new StreamReader(strFilePath);
            string[] headers = sr.ReadLine().Split(',');
            DataTable dt = new DataTable();
            foreach (string header in headers)
            {
                dt.Columns.Add(header);
            }
            while (!sr.EndOfStream)
            {
                string[] rows = Regex.Split(sr.ReadLine(), ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                if (headers.Length == rows.Length)
                {
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = rows[i];
                    }
                    dt.Rows.Add(dr);
                }
            }
            sr.Close();

            return dt;
        }

        public static void AddLabel(Label lb, string value)
        {
            if (lb.InvokeRequired)
            {
                lb.BeginInvoke(new System.Action(delegate { AddLabel(lb, value); }));
                return;
            }
            lb.Text = value;
        }



        public static void AddText(RichTextBox rtb, string value)
        {
            if (rtb.InvokeRequired)
            {
                rtb.BeginInvoke(new System.Action(delegate { AddText(rtb, value); }));
                return;
            }

            rtb.AppendText((value) + Environment.NewLine);
            rtb.ScrollToCaret();

        }
        public static void AddText(RichTextBox rtb, string value, Color color)
        {
            if (rtb.InvokeRequired)
            {
                rtb.BeginInvoke(new System.Action(delegate { AddText(rtb, value, color); }));
                return;
            }
            string[] str = value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            rtb.DeselectAll();
            rtb.SelectionColor = color;
            rtb.AppendText(value + Environment.NewLine);
            rtb.SelectionFont = new Font(rtb.SelectionFont, FontStyle.Regular);
            rtb.SelectionColor = Color.Black;
            rtb.ScrollToCaret();

        }

        public static XDocument DocumentToXDocumentReader(XmlDocument doc)
        {
            return XDocument.Load(new XmlNodeReader(doc));
        }

        public bool LockedFile(FileInfo file)
        {
            try
            {
                string filePath = file.FullName;
                FileStream fs = File.OpenWrite(filePath);
                fs.Close();
                return false;
            }
            catch (Exception) { return true; }
        }

        public static void AddRTBText(RichTextBox rtb, string value)
        {
            if (rtb.InvokeRequired)
            {
                rtb.BeginInvoke(new Action(delegate { AddRTBText(rtb, value); }));
                return;
            }

            rtb.AppendText((value) + Environment.NewLine);
            rtb.ScrollToCaret();

        }
        public static void AddRTBText(RichTextBox rtb, string value, Color color)
        {
            if (rtb.InvokeRequired)
            {
                rtb.BeginInvoke(new Action(delegate { AddRTBText(rtb, value, color); }));
                return;
            }
            string[] str = value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            rtb.DeselectAll();
            rtb.SelectionColor = color;
            rtb.AppendText(value + Environment.NewLine);
            rtb.SelectionFont = new Font(rtb.SelectionFont, FontStyle.Regular);
            rtb.SelectionColor = Color.Black;
            rtb.ScrollToCaret();

        }

        public static void AddProcessingError(ProcessingErrors procError)
        {
            SqlConnection sqlConn = new SqlConnection();
            sqlConn.ConnectionString = Globals.connString();
            SqlCommand sqlProcErrors = new SqlCommand("Add_ProcessingError", sqlConn);
            sqlProcErrors.CommandType = CommandType.StoredProcedure;
            if (sqlConn.State != ConnectionState.Closed)
            {
                sqlConn.Close();
            }
            sqlConn.Open();
            SqlParameter senderID = sqlProcErrors.Parameters.Add("@SENDERID", SqlDbType.VarChar, 15);
            SqlParameter recipientID = sqlProcErrors.Parameters.Add("@RECIPIENTID", SqlDbType.VarChar, 15);
            SqlParameter filename = sqlProcErrors.Parameters.Add("@FILENAME", SqlDbType.VarChar, -1);
            SqlParameter procDate = sqlProcErrors.Parameters.Add("@ProcDate", SqlDbType.DateTime);
            SqlParameter errorCode = sqlProcErrors.Parameters.Add("@ERRORCODE", SqlDbType.NChar, 10);
            SqlParameter errorDesc = sqlProcErrors.Parameters.Add("@ERRORDESC", SqlDbType.VarChar, 200);
            SqlParameter procId = sqlProcErrors.Parameters.Add("@PROCID", SqlDbType.UniqueIdentifier);
            senderID.Value = procError.SenderId;
            recipientID.Value = procError.RecipientId;
            filename.Value = Path.GetFileName(procError.FileName);
            errorCode.Value = procError.ErrorCode.Code;
            errorDesc.Value = procError.ErrorCode.Description;
            procDate.Value = DateTime.Now;
            procId.Value = procError.ProcId;
            sqlProcErrors.ExecuteNonQuery();

        }

        public static void AddTransaction(Guid custid, Guid profile, string fileName, string direction, bool original, TransReference trRefs, string archive)
        {
            using (KifEntity db = new KifEntity(Globals.ConnString.ConnString))
            {
                var tr = (from t in db.Transactions
                          where t.T_FILENAME == fileName
                          && t.T_MSGTYPE == (original ? "Original" : "Duplicate")
                          select t).FirstOrDefault();
                if (tr == null)
                {
                    Transaction newTr = new Transaction
                    {
                        T_C = custid,
                        T_P = profile,
                        T_FILENAME = fileName,
                        T_DATETIME = DateTime.Now,
                        T_TRIAL = "N",
                        T_DIRECTION = direction,
                        T_REF1 = trRefs.Reference1,
                        T_REF2 = trRefs.Reference2,
                        T_REF3 = trRefs.Reference3,
                        T_REF1TYPE = trRefs.Ref1Type.ToString(),
                        T_REF2TYPE = trRefs.Ref2Type.ToString(),
                        T_REF3TYPE = trRefs.Ref3Type.ToString(),
                        T_ARCHIVE = Path.GetFileName(archive)
                    };
                    db.Transactions.Add(newTr);

                }
                else
                {
                    tr.T_DATETIME = DateTime.Now;
                    tr.T_DIRECTION = direction;
                    tr.T_REF1 = trRefs.Reference1;
                    tr.T_REF2 = trRefs.Reference2;
                    tr.T_REF3 = trRefs.Reference3;
                    tr.T_REF1TYPE = trRefs.Ref1Type.ToString();
                    tr.T_REF2TYPE = trRefs.Ref2Type.ToString();
                    tr.T_REF3TYPE = trRefs.Ref3Type.ToString();
                    tr.T_ARCHIVE = Path.GetFileName(archive);
                }
                db.SaveChanges();
            }
        }

        public static void AddTransactionLog(Guid p_id, String senderid, String filename, String lastresult, DateTime lastdate, String success, String cust, String recipientid)
        {
            using (KifEntity db = new KifEntity(Globals.ConnString.ConnString))
            {
                Transaction_Log tl = new Transaction_Log
                {
                    X_P = p_id,
                    X_FILENAME = Path.GetFileName(filename),
                    X_DATE = lastdate,
                    X_LASTRESULT = lastresult,
                    X_SUCCESS = success,
                    X_C = GetCustomer(cust)
                };
                db.Transaction_Logs.Add(tl);
                db.SaveChanges();
            }
        }

        public static string GetEnum(string enumType, string mapValue)
        {
            using (KifEntity db = new KifEntity(Globals.ConnString.ConnString))
            {
                var ceEnum = (from e in db.Cargowise_Enums
                              where e.CW_ENUMTYPE == enumType
                              && e.CW_MAPVALUE == mapValue
                              select e.CW_ENUM).FirstOrDefault();
                if (ceEnum != null)
                {
                    return ceEnum.Trim();
                }
                return null;
            }
        }

        public static string MoveFile(string fileToMove, string folder)
        {
            int icount = 1;

            string filename = Path.GetFileNameWithoutExtension(fileToMove);
            string ext = Path.GetExtension(fileToMove);
            string newfile = Path.Combine(folder, Path.GetFileName(fileToMove));
            while (File.Exists(newfile))
            {
                newfile = Path.Combine(folder, filename + icount + ext);
                icount++;

            }


            try
            {
                File.Move(fileToMove, Path.Combine(folder, newfile));
            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;
                newfile = "Warning: " + strEx + " exception found while moving " + filename;
            }
            return newfile;
        }

    }


}
