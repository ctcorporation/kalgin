﻿using KIF_Sat.DTO;
using KIF_Sat.Models;
using NodeResources;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using XMLLocker;
using XMLLocker.CTC;
using XUS;

namespace KIF_Sat
{
    public class ADMImport
    {
        #region Public Properties
        public string ErrorString
        {
            get
            { return _errString; }
            set
            { _errString = value; }
        }

        public string OutputPath
        {
            get; set;
        }
        public Transaction TrLog
        {
            get
            { return _tr; }
            set
            { _tr = value; }
        }
        #endregion

        #region Private Members
        private Transaction _tr;
        private string _importFile;

        private string _errString;
        private string _connstring;
        private List<MapOperation> _map;
        #endregion

        #region Constructors
        public ADMImport(string fileToImport, string connString)
        {
            _importFile = fileToImport;
            _connstring = connString;
        }
        #endregion

        #region Methods

        private List<CompanyElement> GetCompaniesFromRow(DataRow dataRow)
        {
            var order = dataRow[ReturnStringFromList(_map, "Order", "OrderNo")].ToString();
            var agentField = ReturnStringFromList(_map, "Order", "ForwardingAgent");
            var supplierField = ReturnStringFromList(_map, "Order", "Supplier");
            var deliverToField = ReturnStringFromList(_map, "Order", "DeliverTo");
            List<CompanyElement> companies = new List<CompanyElement>();
            bool errorsfound = false;
            var consigneeField = ReturnStringFromList(_map, "Order", "Consignee");

            if (string.IsNullOrEmpty(dataRow[consigneeField].ToString()))
            {
                _errString += order + "|Critical|Consignee| Consignee is Blank" + Environment.NewLine;
                return null;
            }
            var company = GetCompany(_map, "Order", "Consignee", dataRow);
            if (company == null)
            {
                //Report Missing Company
                errorsfound = true;
                _errString += order + "|Critical|Consignee|" + dataRow[consigneeField].ToString() + Environment.NewLine;
            }
            else
            {
                companies.Add(company);
            }
            company = GetCompany(_map, "Order", "ForwardingAgent", dataRow);
            if (company == null)
            {
                _errString += order + "|Critical|ForwardingAgent|" + dataRow[agentField].ToString() + Environment.NewLine;
                //Report Missing Company
                errorsfound = true;
            }
            else
            {
                companies.Add(company);
            }
            company = GetCompany(_map, "Order", "Supplier", dataRow);
            if (company == null)
            {
                _errString += order + "|Critical|Supplier|" + dataRow[supplierField].ToString() + Environment.NewLine;
                //Report Missing Company
                errorsfound = true;
            }
            else
            {
                companies.Add(company);
            }
            company = GetCompany(_map, "Order", "DeliverTo", dataRow);
            if (company == null)
            {
                _errString += order + "|Critical|DeliverTo|" + dataRow[deliverToField].ToString() + Environment.NewLine;
                //Report Missing Company
                errorsfound = true;
            }
            else
            {
                companies.Add(company);
            }
            if (errorsfound)
            {
                return null;
            }
            return companies;
        }



        private CompanyElement GetCompany(List<MapOperation> map, string operation, string companyType, DataRow dataRow)
        {
            var companyTypeField = ReturnStringFromList(map, operation, companyType);

            CompanyElementCompanyType compType = new CompanyElementCompanyType();
            switch (companyType)
            {
                case "Consignee":
                    compType = CompanyElementCompanyType.Consignee;
                    break;
                case "Supplier":
                    compType = CompanyElementCompanyType.Supplier;
                    break;
                case "ForwardingAgent":
                    compType = CompanyElementCompanyType.ForwardingAgent;
                    break;
                case "DeliverTo":
                    compType = CompanyElementCompanyType.DeliveryAddress;
                    break;
            }
            if (string.IsNullOrEmpty(dataRow[companyTypeField].ToString()))
            {
                return null;
            }
            var companyName = dataRow[companyTypeField].ToString();
            using (IUnitOfWork uow = new UnitOfWork(new KifEntity(_connstring)))
            {
                var comp = uow.Organisations.Find(x => x.OR_MapValue == companyName).FirstOrDefault();
                if (comp == null)
                {
                    return null;
                }
                CompanyElement company = new CompanyElement
                {
                    CompanyOrgCode = comp.OR_CODE,
                    CompanyName = comp.OR_Name,
                    Address1 = comp.OR_Address1,
                    Address2 = comp.OR_Address2,
                    Address3 = comp.OR_Address3,
                    City = comp.OR_Suburb,
                    State = comp.OR_State,
                    CompanyCode = comp.OR_ShortCode,
                    PostCode = comp.OR_PostCode,
                    CompanyType = compType
                };
                return company;
            }
        }

        private List<DateElement> GetDatesFromRow(DataRow dataRow)
        {
            List<DateElement> Dates = new List<DateElement>();
            var orderDate = ReturnStringFromList(_map, "Order", "OrderDate");
            var requiredDate = ReturnStringFromList(_map, "Order", "DeliverBy");
            var etaDate = ReturnStringFromList(_map, "Order", "ETA");
            var date = new DateElement
            {
                DateType = DateElementDateType.Ordered,
                ActualDate = dataRow[orderDate].ToString()
            };
            if (!string.IsNullOrEmpty(date.ActualDate))
            {
                Dates.Add(date);
            }
            date = new DateElement
            {
                DateType = DateElementDateType.DeliverBy,
                EstimateDate = dataRow[orderDate].ToString()
            };
            if (!string.IsNullOrEmpty(date.EstimateDate))
            {
                Dates.Add(date);
            }
            date = new DateElement
            {
                DateType = DateElementDateType.Arrival,
                EstimateDate = dataRow[orderDate].ToString()
            };
            if (!string.IsNullOrEmpty(date.EstimateDate))
            {
                Dates.Add(date);
            }
            return Dates;


        }

        public bool CreateOrder()
        {
            var tb = ExcelToDB(_importFile);
            _map = BuildMap(tb);
            if (_map.Count == 0)
            {
                return false;
            }
            CreateCommonfromTable(tb);
            if (_tr == null)
            {
                _tr = new Transaction();
                _tr.T_FILENAME = _importFile;
            }
            _tr.T_DATETIME = DateTime.Now;
            _tr.T_ID = Guid.NewGuid();
            return true;
        }
        private void CreateCommonfromTable(DataTable tbl)
        {
            string prevOrderNo = "";
            NodeFile nodeFile = new NodeFile();
            int iLineNo = 0;
            List<OrderLineElement> orderLines = new List<OrderLineElement>();
            OrderLineElement orderLine = new OrderLineElement();
            List<NodeFileTrackingOrder> TrackingOrders = new List<NodeFileTrackingOrder>();
            var orderLineNoField = ReturnStringFromList(_map, "Order", "LineNo");
            var agentField = ReturnStringFromList(_map, "Order", "ForwardingAgent");
            var agent = GetEnum("AGENT").Trim();
            var orderNoField = ReturnStringFromList(_map, "Order", "OrderNo");
            var transportModeField = ReturnStringFromList(_map, "Order", "Mode");
            var supplierOrdernoField = ReturnStringFromList(_map, "Order", "SupplierOrderNo");
            for (int i = 0; i < tbl.Rows.Count; i++)
            {
                if (i == 84)
                {
                    i = i;
                }
                if (tbl.Rows[i][agentField].ToString() != agent)
                {
                    continue;
                }
                CompanyElement cust = new CompanyElement();

                var orderNumber = tbl.Rows[i][orderNoField].ToString();
                //Meaning that the Orders are the same and therefore to continue the line count. 
                if (prevOrderNo == orderNumber)
                {
                    iLineNo++;
                }
                else
                {
                    //Checks to see if the Line no is blank. If not we will use that number. 
                    if (!string.IsNullOrEmpty(tbl.Rows[i][orderLineNoField].ToString()))
                    {
                        int iTest;
                        if (int.TryParse(tbl.Rows[i][orderLineNoField].ToString(), out iTest))
                        {
                            iLineNo = iTest;
                        }
                    }
                    else
                    {
                        // Prev Order and This Order are different and now need to reset the Line Number. 
                        iLineNo = 1;
                        orderLines = new List<OrderLineElement>();
                    }

                }
                // If ordernumber is a new order number OR Line No is greater than 0 and the Order and Previous are the same
                orderLine = new OrderLineElement();
                if ((orderNumber != prevOrderNo) || (iLineNo > 0 && orderNumber == prevOrderNo))
                {
                    //Set the previous order to current order. Won't check this till next iteration
                    prevOrderNo = orderNumber;
                    NodeFileTrackingOrder order = new NodeFileTrackingOrder();
                    order.IncoTerms = "EXW";

                    if (!string.IsNullOrEmpty(transportModeField))
                    {
                        if (!string.IsNullOrEmpty(tbl.Rows[i][transportModeField].ToString()))
                        {
                            if (tbl.Rows[i][transportModeField].ToString() == "AIRFREIGHT")
                            {
                                order.TransportMode = "AIR";
                                order.ContainerMode = "LSE";
                            }
                            else
                            {
                                order.TransportMode = "SEA";
                                if (tbl.Rows[i][transportModeField].ToString().Contains("LCL"))
                                {
                                    order.ContainerMode = "LCL";
                                }
                                else
                                {
                                    order.ContainerMode = "FCL";
                                }

                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(supplierOrdernoField))
                    {
                        order.SupplierOrderNo = tbl.Rows[i][supplierOrdernoField].ToString();
                    }

                    orderLine = GetOrderLineFromRow(tbl.Rows[i]);
                    orderLine.LineNo = iLineNo.ToString();
                    //Create a new Order file.
                    order.OrderNo = orderNumber;
                    //Add Dates from the Current Row.
                    order.Dates = GetDatesFromRow(tbl.Rows[i]).ToArray();
                    //add the Companies from the Current Row. 
                    var companies = GetCompaniesFromRow(tbl.Rows[i]);
                    if (companies == null)
                    {
                        continue;
                    }
                    order.Companies = companies.ToArray();
                    //Create a new List of lines and add the current Order Line to it. 
                    orderLines = new List<OrderLineElement>();
                    //Set the Identity header of the new Order 
                    nodeFile.IdendtityMatrix = new NodeFileIdendtityMatrix
                    {
                        SenderId = Globals.glCustCode,
                        CustomerId = "ADMAUS_AU",
                        FileDateTime = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"),
                        PurposeCode = "CTC",
                        DocumentType = "ForwardingOrder",
                        EventCode = "DIM"
                    };
                    //Write the Order File out. 
                }
                orderLine.LineNo = iLineNo.ToString();
                orderLines.Add(orderLine);
                //Look Ahead to see if the next line is the same as the current number. 
                //If the number is different need to close the OrderFile.
                string nextOrderNo = string.Empty;
                try
                {
                    nextOrderNo = tbl.Rows[i + 1][orderNoField].ToString();
                }
                catch (NullReferenceException)
                {
                    nextOrderNo = string.Empty;
                }
                catch (IndexOutOfRangeException)
                {
                    nextOrderNo = string.Empty;
                }
                if (nextOrderNo != prevOrderNo)
                {
                    WriteCommonXML(nodeFile);
                }
            }
        }

        private void WriteCommonXML(NodeFile nodeFile)
        {
            CommonToCw ccw = new CommonToCw();
            XUS.UniversalInterchange xus = ccw.CreateOrderFile(nodeFile, OutputPath);
            if (xus == null)
            {
                return;
            }

            if (CreateXUSFile(xus))
            {

            }


        }

        private bool CreateXUSFile(UniversalInterchange xus)
        {
            try
            {
                String cwXML = Path.Combine(OutputPath, GetDataContext(xus) + ".xml");
                int iFileCount = 0;
                while (File.Exists(cwXML))
                {
                    iFileCount++;
                    cwXML = Path.Combine(OutputPath, GetDataContext(xus) + iFileCount + ".xml");

                }
                Stream outputCW = File.Open(cwXML, FileMode.Create);
                using (StringWriter writer = new StringWriter())
                {
                    XmlSerializer xSer = new XmlSerializer(typeof(UniversalInterchange));
                    var cwNSUniversal = new XmlSerializerNamespaces();
                    cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
                    xSer.Serialize(outputCW, xus, cwNSUniversal);
                    outputCW.Flush();
                    outputCW.Close();
                }
            }
            catch (Exception ex)
            {
                _errString += ex.GetType().Name + " Error found:" + ex.Message;
                return false;
            }
            return true;

        }

        private string GetDataContext(UniversalInterchange xus)
        {
            var context = (from x in xus.Body.BodyField.Shipment.DataContext.DataTargetCollection
                           select x.Key).FirstOrDefault();
            if (context == null)
            {
                return null;
            }
            else
            {
                return context;
            }
        }

        private OrderLineElement GetOrderLineFromRow(DataRow dataRow)
        {
            OrderLineElement orderLine = new OrderLineElement();
            var unitpriceField = ReturnStringFromList(_map, "Order", "UnitPrice");
            if (!string.IsNullOrEmpty(unitpriceField))
            {
                decimal d;
                if (decimal.TryParse(dataRow[unitpriceField].ToString(), out d))
                {
                    orderLine.UnitPrice = d;
                    orderLine.UnitPriceSpecified = true;
                }

            }
            //Line Order Qty
            var ordQtyField = ReturnStringFromList(_map, "Order", "OrdQty");
            if (!string.IsNullOrEmpty(ordQtyField))
            {
                decimal d;
                if (decimal.TryParse(dataRow[ordQtyField].ToString(), out d))
                {
                    orderLine.OrderQty = d;
                    orderLine.OrderQtySpecified = true;
                }
            }
            //Unit of Measure
            var uomField = ReturnStringFromList(_map, "Order", "UOM");
            if (!string.IsNullOrEmpty(uomField))
            {
                orderLine.UnitOfMeasure = dataRow[uomField].ToString();
            }
            return orderLine;

        }

        private DataTable ExcelToDB(string filetoOpen)
        {
            var tb = ExcelToTable.ToTable(filetoOpen);

            return tb;
        }

        private List<MapOperation> BuildMap(DataTable tb)
        {
            using (IUnitOfWork uow = new UnitOfWork(new KifEntity(_connstring)))
            {
                var fldcount = tb.Columns.Count;
                var colName = tb.Columns[0].ColumnName;

                var fd = uow.FileDescriptions.Find(x => x.PC_FieldCount == fldcount
                        && x.PC_FirstFieldName.Trim() == colName).FirstOrDefault();
                // FileDescription has been found.
                if (fd != null)
                {
                    var me = uow.MappingEnums.Find(x => x.M_PC == fd.PC_Id);
                    if (me != null)
                    {
                        List<MapOperation> mapOperation = new List<MapOperation>();
                        foreach (var m in me)
                        {

                            MapOperation map = new MapOperation
                            {
                                MD_DataType = m.M_FieldType,
                                MD_Operation = m.M_Operation,
                                MD_MapDescription = m.M_Description,
                                MD_FromField = m.M_From,
                                MD_ToField = m.M_To,
                                MD_Type = m.M_MapType
                            };
                            mapOperation.Add(map);
                        }
                        return mapOperation;
                    }
                }

            }
            return null;

        }
        #endregion

        #region Helpers
        private static string ReturnStringFromList(List<MapOperation> map, string operation, string stringToFind)
        {
            var value = (from x in map
                         where x.MD_Operation == operation
                         && x.MD_ToField == stringToFind
                         select x.MD_FromField).FirstOrDefault();
            return value;
        }

        private string GetEnum(string enumToFind)
        {
            using (IUnitOfWork uow = new UnitOfWork(new KifEntity(_connstring)))
            {
                var val = uow.Cargowise_Enums.Find(x => x.CW_ENUM == enumToFind).FirstOrDefault();
                if (val != null)
                {
                    return val.CW_MAPVALUE;
                }
            }
            return null;
        }




        #endregion
    }
}
